#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
PowerFlow with NDE
Vitor Batista
vitordsbatista@gmail.com
Python 2.7
"""

#import numpy as np
import os
import pickle as pk
import math
import csv
import sys
from scipy.io import loadmat
from itertools import compress
import copy
import timeit
from timeit import default_timer as timer

def dfs(graph, start):
    """
        Realiza uma busca em profundidade num grafo

        Parâmetros:
            graph   -- dicionário com o grafo
            start   -- nó de início da busca
        Saída:
            visited -- nós visitados pela busca
    """
    visited, stack = [], [[start, 1]]
    while stack:
        vertex = stack.pop()
        if vertex not in visited:
            visited.append(vertex)
            if vertex[0] in graph.keys():
                tmp = set([i[0] for i in visited]).intersection(graph[vertex[0]]).symmetric_difference(graph[vertex[0]]),
                for i in tmp[0]:
                    stack.append([i, vertex[1]+1])
    visited = map(list, zip(*visited))              #Transposição da rnp
    return visited

def dp2rnp(case):
    """
        Gera a floresta com todas as rnp's da rede

        Parâmetros:
            case     -- dicionário com os dados da rede elétrica
        Saída:
            floresta -- rnps num dicionário
       
    """
    status = case['line']['status']
    dePara = list(compress(case['line']['from_to'], status))
    paraDe = [(j, i) for i, j in dePara]
    #print dePara
    #print paraDe
    roots = case['gen']['bus']
    #Transforma o De-Para num grafo
    dp = dict()
    for i in dePara:     
        if dp.has_key(i[0]):
            dp[i[0]].append(i[1])
        else:
            dp[i[0]] = [i[1]]
    for i in paraDe:     
        if dp.has_key(i[0]):
            dp[i[0]].append(i[1])
        else:
            dp[i[0]] = [i[1]]
    floresta = dict()
    for n, i in enumerate(roots): #For com todos os nós raizes para gerar todas as rnps
        tmp = dfs(dp, i)
        floresta[str(n)] = tmp
    return floresta

def load_mpc(path, sub_file=None):
    """
        Lê um caso do matpower

        Parâmetros:
            path -- caminho para o caso do matpower (.mat)
        Saída:
            case -- dicionário com os dados da rede
       
    """
    if sub_file:
        sub = {}
        feeders = []
        #Carrega um arquivo com os dados das subestações
        with open(sub_file, 'r') as f:
            reader = csv.reader(f)
            for i in reader:
                key = int(i[0])
                S = [float(i[1])]
                feed = map(int, i[2:])
                feeders += feed
                sub[key] = S+feed
    #Carrega o arquivo .mat
    mat = loadmat(path)
    #Potência base
    Sb = mat['baseMVA'][0][0]
    #Tensão base
    Vb = mat['mpc'][0][0][2][0][9]
    #Separa ele em dicionários
    bus = {
        'bus': [], 'type': [],
        'P': [], 'Q': [], 'S': [], 
        'G': [], 'B': [], 'Ysh': [],
        'Vm': [], 'Va': []
    }
    gen = {
        'bus': []
    }
    line = {
        'from_to': [],
        'r': [], 'x': [], 'Z': [],
        'max_i': [], 'status': [],
        'p_loss': [], 'q_loss': [],
        'I': [], 'loading': []
    }
    #Barras
    for i in mat['mpc'][0][0][2]:
        bus['bus'].append(int(i[0]))
        bus['type'].append(int(i[1]))
        bus['P'].append(i[2])
        bus['Q'].append(i[3])
        bus['S'].append(complex(i[2], i[3]))
        bus['G'].append(i[4])
        bus['B'].append(i[5])
        bus['Ysh'].append(complex(i[4], i[5]))
        # Saída de dados do fluxo de carga
        bus['Vm'].append(1)
        bus['Va'].append(0)
    #Alimentadores
    if sub_file:
        gen['bus'] = feeders
        gen['sub'] = sub
    else:
        for i in mat['mpc'][0][0][3]:
            gen['bus'].append(int(i[0]))
            gen['sub'] = None
    #Linhas
    for i in mat['mpc'][0][0][4]:
        line['from_to'].append((int(i[0]), int(i[1])))
        line['r'].append(i[2])
        line['x'].append(i[3])
        line['Z'].append(complex(i[2], i[3]))
        line['max_i'].append((i[5]*Sb/Vb)/math.sqrt(3))
        line['status'].append(int(i[10]))
        # Saída de dados do fluxo de carga
        line['p_loss'].append(0)
        line['q_loss'].append(0)
        line['I'].append(0)
        line['loading'].append(0)

    #Adiciona num dict
    case = {'bus': bus, 'gen': gen, 'line': line, 'Sb': Sb, 'Vb': Vb}
    #Cria os resultados do fluxo de carga
    case['output'] = {
        'sum_p_loss': 0,                 
        'sum_q_loss': 0,                 
        'max_line_loading': 0,
        'max_sub_loading': 0,
        'min_voltage': [],
        'max_voltage': []
    }
    return case

def rnp2dp(rnp):
    """
        Transforma uma rnp num de-para

        Parâmetros:
            rnp     -- representação nó profundidade
        Saída:
            dePara  -- de-para
    """
    dePara = []
    #Inverte a RNP
    rnp = [rnp[0][::-1], rnp[1][::-1]]          #Inverte a rnp
    for n, (i, j) in enumerate(zip(*rnp)):
        for m, (k, l) in enumerate(zip(*rnp)[n+1:]):
            if l == j-1:
                dePara.append((k, i))
                break
    return dePara

def run_pf(case, rnp, verbose=0):
    """
        Executa o fluxo de carga para cada uma rnps

        Parâmetros:
            case    -- dicionário com os dados da rede elétrica
            rnp     -- floresta com as rnps estruturadas
            verbose -- 1 para saída visual de dados
        Saída:
            case    -- dicionário com os dados da rede elétrica e
                        os resultados do fluxo de carga e dos dados
                        formatados
            
    """
    t0 = timer()
    bus_order = case['bus']['bus']
    line_order = case['line']['from_to']
    it = []
    for i in rnp:
        case, tmp = bfsa(case, rnp[i])
        it.append(tmp)
    tn = timer() - t0
    #====Saída de dados para o algoritmo====#
    #Perdas
    case['output']['sum_p_loss'] = sum(case['line']['p_loss'])
    case['output']['sum_q_loss'] = sum(case['line']['q_loss'])
    #Tensão máxima
    vol_max = max(case['bus']['Vm'])
    id_max = [n for n, i in enumerate(case['bus']['Vm']) if i == vol_max]   #Encontra as barras
    bus_max = [case['bus']['bus'][i] for i in id_max]
    case['output']['max_voltage'] = [bus_max, vol_max]
    #Tensão mínima
    vol_min = min(case['bus']['Vm'])
    id_min = [n for n, i in enumerate(case['bus']['Vm']) if i == vol_min]   #Encontra as barras
    bus_min = [case['bus']['bus'][i] for i in id_min]
    case['output']['min_voltage'] = [bus_min, vol_min]
    #Máximo carregamento das linhas
    loading = [i/j*100 if j else 0.0 for i, j in zip(case['line']['I'], case['line']['max_i'])]
    loading = []
    for i, j in zip(case['line']['from_to'], case['line']['I']):
        ix = case['line']['from_to'].index(i)
        max_i = case['line']['max_i'][ix]
        if max_i:
            loading.append(j/max_i*100)
        else:
            loading.append(0.)
    case['line']['loading'] = loading
    max_loading_line = max(loading)
    case['output']['max_loading'] = max_loading_line
    #Máximo carregamento das subestações
    #Calcula as cargas de cada alimentador
    loads = {}
    for i in rnp:
        soma = 0.
        for j in rnp[i][0]:
            ix = case['bus']['bus'].index(j)
            soma += abs(case['bus']['S'][ix])
        loads[rnp[i][0][0]] = soma
    if case['gen']['sub']:
        #Concatena as cargas dos alimentadores em cada subestação
        for i in case['gen']['sub']:
            feed =  case['gen']['sub'][i][1:]
            loading = 0
            #Calcula o carregamento de cada subestação
            for j in feed:
                loading += loads[j]
            ratio = loading/case['gen']['sub'][i][0]
            case['gen']['sub'][i].insert(1, ratio)
            case['gen']['sub'][i].insert(1, loading)
        #Seleciona o maior carregamento das subestações
        max_loading_sub = max([case['gen']['sub'][i][2] for i in case['gen']['sub']]) * 100
        case['output']['max_loading_sub'] = max_loading_sub
    else:
        max_loading_sub = -1
        case['output']['max_loading_sub'] = -1
    #Tempo de execução
    case['output']['time'] = tn
    #====Saída de dados para o usuário====#
    if verbose:
        #-Dados da barra
        print '  {:=^22}'.format('Bus Data')
        print '{:^9}{:^8}{:^8}'.format('Bus', 'Vm', 'Va')
        print '  {:->22}'.format('')
        for b, m, a in zip(case['bus']['bus'], case['bus']['Vm'], case['bus']['Va']):
            print '{:^9}{:^8.3f}{:^8.3f}'.format(b, m, a)
        #-Dados da linha
        print '\n  {:=^55}'.format("Line Data")
        print '{:^8}{:^6}{:^10}{:^9}{:^7}{:^9}{:^9}'.format('From', 'To', 'p_loss', 'q_loss', 'I', 'Max_I', 'Loading')
        print '  {:->55}'.format('')
        for ft, p, q, i, m, l in zip(case['line']['from_to'], case['line']['p_loss'], case['line']['q_loss'], case['line']['I'], case['line']['max_i'], case['line']['loading']):
            f, t = ft
            i = i.real
            print '{:^9}{:^5}{:^11.3f}{:^7.3f}{:^9.3f}{:^7.3f}{:^11.2f}'.format(f, t, p, q, i, m, l)
        #DADOS GERAIS
        print '\n  {:=^65}'.format("General Data")
        print '  {:^8}{:^6}{:^9}{:^9}{:^7}{:^14}{:^9}{:^9}'.format('Time', 'Iter', 'p_loss', 'q_loss', 'V_max', 'V_min', 'max_line', 'max_sub')
        print '  {:->65}'.format('')
        print '  {:^8.4f}{:^6.2f}{:^9.4f}{:^8.4f}{:^8.4f}{:^8.4f}@ {}{:^11.2f}{:^8.2f}'.format(
           tn, sum(it)/float(len(it)), case['output']['sum_p_loss'], case['output']['sum_q_loss'], vol_max, vol_min, bus_min, max_loading_line, max_loading_sub
        )
    return case

def bfsa(case, rnp):
    """
        Fluxo de carga de varredura direta e inversa

        Parâmetros:
            case    -- dicionário com os dados da rede elétrica
            rnp     -- floresta com as rnps estruturadas
        Saída:
            case    -- dicionário com os dados da rede elétrica e
                        os resultados do fluxo de carga e dos dados
                        formatados
    """
    #Gera uma rnp invertida
    rnp_rev = [rnp[0][::-1], rnp[1][::-1]]
    #Leitura de dados
    bus = rnp[0]                            #Barras da rnp
    dp_all = case['line']['from_to']        #Todos as linhas possiveis
    dp = rnp2dp(rnp)                       #Transforma a rnp em dp
    for n, i in enumerate(dp):              #Validação do de-para
        if not i in dp_all:
            dp[n] = (i[1], i[0])
    #==============================================================#
    #Estrutura de dados
    #Será um dicionário, onde o índice é uma tupla (no caso de linhas), 
    #e os outros valores serão números complexos da rede
    #0 - V (Tensões)
    #1 - S (Potência)
    #2 - I (Corrente)
    #0 - Z (impedãncia)
    #1 - I_line (corrente na linha)
    #line_data = dict.fromkeys(dp2)
    bus_data = dict()#.fromkeys(bus)
    line_data = dict.fromkeys(dp)
    #==============================================================#
    rnp_len = len(rnp[0])
    #Cálculo da tensão inicial
    tmp = complex(1, 0)
    busd = [case['bus']['bus'].index(i) for i in bus]
    for i, j in zip(bus, busd):
        bus_data[i] = [
            tmp, case['bus']['S'][j], 
            0, case['bus']['Ysh'][j]
        ]
    #Cálculo da impedância
    line_id = [case['line']['from_to'].index(i) for i in dp]
    for i, j in zip(dp, line_id):
        line_data[i] = [case['line']['Z'][j], 0]
    #==============================================================#
    #Loop
    v_err = 1
    #=========Passo 1=========#
    V_ant = dict()
    for i in bus_data:
        V = bus_data[i][0]
        Y = bus_data[i][3]
        S = bus_data[i][1]
        #S = bus_data[i][1] + Y.conjugate()*V**2
        bus_data[i][2] = (S/V).conjugate() + Y*V        #Calcula as correntes
        V_ant[i] = V
    max_it = 100
    it = 1
    while v_err > 0.00000001:
        """
        t0 = timer()
        """
        it += 1
        if it > max_it:
            raise Exception('Power Flow did not converge after %d iterations!' %max_it)
        #=========Passo 2=========#
        for ix, (node, depth) in enumerate(zip(*rnp_rev)[:-1]):
            for node_bef, depth_bef in zip(*rnp_rev)[ix+1:]:
                #Se for o nó pai
                if depth-1 == depth_bef:
                    #Percorre a rnp e identifica os nós que saem do node
                    adj_nodes = []
                    for node_aft in zip(*rnp)[rnp_len-ix:]:
                        if node_aft[1] <= depth:
                            break
                        if node_aft[1] == depth+1:
                            adj_nodes.append(node_aft[0])
                    #Somatório das correntes que saem
                    sum_I_line = 0
                    #Soma as correntes deles
                    for adj in adj_nodes:
                        tup = (node, adj)
                        if not tup in dp:
                            tup = (adj, node)
                        sum_I_line += line_data[tup][1]
                    #Caso no de-para os nós estejam ao contrário
                    tup = (node_bef, node)
                    if not tup in dp:
                        tup = (node, node_bef)
                    #Cálculo da corrente de linha
                    line_data[tup][1] = bus_data[node][2] + sum_I_line
                    break
        """
        t = timer() - t0
        t2.append(t)
        print 'passo 2', t
        t0 = timer()
        """
        #=========Passo 3=========#
        #Obrigado a fazer a pesquisa direta
        rs = len(rnp[0])    #Tamanho da rnp
        for ix, (node, depth) in enumerate(zip(*rnp)[1:]):
            for i in zip(*rnp_rev)[rs-ix-1:]:
                if depth == i[1]+1:
                    before_node = i[0]
                    tup = (before_node, node)
                    if not tup in dp:
                        tup = (node, before_node)
                    bus_data[node][0] = bus_data[before_node][0] - line_data[tup][0] * line_data[tup][1]
                    break
        """
        t = timer() - t0
        t3.append(t)
        print 'passo 3', t
        t0 = timer()
        """
        # ========= Passo 1 e erro ========= #
        p_err = []
        q_err = []
        vm_err = []
        va_err = []
        v_err_a = []
        for i in bus_data:
            #Passo 1
            V = bus_data[i][0]
            Y = bus_data[i][3]
            S = bus_data[i][1]
            I = (S/V).conjugate() + Y*V
            bus_data[i][2] = I      #Adiciona o I no bus_data
            #Erro
            V_err = V - V_ant[i]
            v_err_a.append(abs(V - V_ant[i]))
            vm_err.append(abs(V_err.real))
            va_err.append(abs(V_err.imag))
            V_ant[i] = V
        vm_err = max(vm_err)
        va_err = max(va_err)
        v_err = max(v_err_a)
    """
        t = timer() - t0
        t1.append(t)
        print 'passo 1 e erro', t
    tf = timer() - ti
    print '-----'
    print 'tempo total:', tf
    print 'Tempo total'
    print 'passo 1', sum(t1)
    print 'passo 2', sum(t2)
    print 'passo 3', sum(t3)
    print 'Percentual'
    print 'passo 1', sum(t1)/tf*100
    print 'passo 2', sum(t2)/tf*100
    print 'passo 3', sum(t3)/tf*100
    """
    # ===== Saída de dados ===== #
    root = rnp[0][0]
    bus = [[root, 1, 0]]
    res_branch = [[-1, -1, -1, -1]]
    #Barras
    for i in rnp[0][1:]:
        V = bus_data[i][0]
        real = V.real
        imag = V.imag
        #Vm = np.hypot(real, imag)
        Vm = math.hypot(real, imag)
        #Va = np.arctan(imag/real)
        Va = math.atan(imag/real)
        #Va = np.rad2deg(Va)
        Va = math.degrees(Va)
        ix = case['bus']['bus'].index(i)
        case['bus']['Vm'][ix] = Vm
        case['bus']['Va'][ix] = Va
    #Linhas
    #Cálculo das perdas
    for n, i in enumerate(line_data):
        I = line_data[i][1]
        Z = line_data[i][0].conjugate()
        z1 = Z.real
        z2 = Z.imag
        t1 = z1*abs(I)**2
        t2 = z2*abs(I)**2
        p_loss = t1
        q_loss = t2
        ix = case['line']['from_to'].index(i)
        case['line']['p_loss'][ix] = p_loss
        case['line']['q_loss'][ix] = q_loss
        I = abs(I)*(case['Sb']/case['Vb'])/math.sqrt(3)
        case['line']['I'][ix] = I
    return case, it

def save_case(case, name):
    with open(name, 'w') as csvfile:
        writer = csv.writer(csvfile)
        #Dados das base (potência e tensão)
        writer.writerow(["@BASE"])
        writer.writerow(['S_base (MVA)', 'V_base (kVA)'])
        writer.writerow([case['Sb'], case['Vb']])
        #Dados das barras
        writer.writerow(["@BUS"])
        writer.writerow(['bus', 'type', 'P', 'Q', 'G', 'B', 
                            'V_mag', 'V_ang'])
        for i in zip(case['bus']['bus'],
                     case['bus']['type'],
                     case['bus']['P'],
                     case['bus']['Q'],
                     case['bus']['G'],
                     case['bus']['B'],
                     case['bus']['Vm'],
                     case['bus']['Va']):
            writer.writerow(i)
        #Dados das linhas
        writer.writerow(["@LINE"])
        writer.writerow(['from', 'to', 'status', 'r', 'x', 'P_loss',
                         'Q_loss', 'I', 'I_max', 'loading'])
        for i in zip(case['line']['from_to'],
                     case['line']['status'],
                     case['line']['r'],
                     case['line']['x'],
                     case['line']['p_loss'],
                     case['line']['q_loss'],
                     case['line']['I'],
                     case['line']['max_i'],
                     case['line']['loading']):
            i = list(i)
            tmp = i.pop(0)
            writer.writerow(list(tmp) + i)
        #Dados do gerador
        writer.writerow(["@GEN"])
        writer.writerow(['sub', 'St', 'Sl', 'loading', 'feeders'])
        if case['gen']['sub']:
            for i in zip(case['gen']['sub']):
                writer.writerow([i[0]]+case['gen']['sub'][i[0]])
"""
#TESTES
path = 'cases/case84new.mat'
case = load_mpc(path)
rnp = dp2rnp(case)
run_pf(case, rnp, verbose=1)
rnp['10'][0].pop()
rnp['10'][1].pop()
rnp['2'][0].append(94)
rnp['2'][1].append(8)
run_pf(case, rnp, verbose=1)
#TESTES
#"""
"""
Pickle
#import pickle as pk
#with open('test.p', 'r') as pfile:
#case = pk.load(pfile)

case = 'case84.mat'
path = 'cases/'+case
#subprocess.call("convert-cases ./cases/"+case)

#os.system("cd cases; convert-cases "+case[:-2])
case = load_mpc(path, 'pn_84_2.txt')
rnp = dp2rnp(case)
case = run_pf(case, rnp, verbose=0)
#save_case(case, 'case84.csv')

with open('case84.p', 'w') as pfile:
    pk.dump(case, pfile)

case = load_mpc('cases/case84.mat', 'pn_84_2.txt')
save_case(case, 'case84.csv')

with open('case84.p', 'w') as pfile:
    pk.dump(case, pfile)
"""

#TODO: ler o caso a partir de um csv
def load_case(case_name):
    """ Carrega um caso do csv
        Parâmetros:
            path -- caminho para o caso do matpower (.mat)
        Saída:
            case -- dicionário com os dados da rede
    """
    with open(case_name, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        base = []
        bus = []
        line = []
        gen = []
        tmp = []
        for i in reader:
            if i[0] == '@BASE':
                pass
            elif i[0] == '@BUS':
                #TODO: colocar a partir do 2
                base = tmp[1:]
                tmp = []
            elif i[0] == '@LINE':
                bus = tmp[2:]
                tmp = []
            elif i[0] == '@GEN':
                line = tmp[1:]
                tmp = []
            tmp.append(i)
        gen = tmp[1:]
    #Geração do dicionário
    bus = zip(*bus)
    bus_dict = {
        'bus': list(bus[0]),
        'type': list(bus[1]),
        'P': list(bus[2]),
        'Q': list(bus[3]),
        'G': list(bus[4]),
        'B': list(bus[5]),
        'Vm': list(bus[6]),
        'Va': list(bus[7]),
        #'S': list(bus[0]),
        #'Ysh': list(bus[0])
    }
    #gen = zip(*gen)
    print gen
    print zip(*gen)
    gen_dict = {
        'bus': [], 'sub': []
    }
    line = {
        'from_to': [],
        'r': [], 'x': [], 'Z': [],
        'max_i': [], 'status': [],
        'p_loss': [], 'q_loss': [],
        'I': [], 'loading': []
    }
    #Barras

def load_mpc2(path, sub_file=None):
    """ Carrega um caso do matpower
        Parâmetros:
            path -- caminho para o caso do matpower (.mat)
        Saída:
            case -- dicionário com os dados da rede
    """
    if sub_file:
        sub = {}
        feeders = []
        #Carrega um arquivo com os dados das subestações
        with open(sub_file, 'r') as f:
            reader = csv.reader(f)
            for i in reader:
                key = int(i[0])
                S = [float(i[1])]
                feed = map(int, i[2:])
                feeders += feed
                sub[key] = S+feed
    #Carrega o arquivo .mat
    mat = loadmat(path)
    #Potência base
    Sb = mat['baseMVA'][0][0]
    #Tensão base
    Vb = mat['mpc'][0][0][2][0][9]
    #Separa ele em dicionários
    bus = {
        'bus': [], 'type': [],
        'P': [], 'Q': [], 'S': [], 
        'G': [], 'B': [], 'Ysh': [],
        'Vm': [], 'Va': []
    }
    gen = {
        'bus': []
    }
    line = {
        'from_to': [],
        'r': [], 'x': [], 'Z': [],
        'max_i': [], 'status': [],
        'p_loss': [], 'q_loss': [],
        'I': [], 'loading': []
    }
    #Barras
    for i in mat['mpc'][0][0][2]:
        bus['bus'].append(int(i[0]))
        bus['type'].append(int(i[1]))
        bus['P'].append(i[2])
        bus['Q'].append(i[3])
        bus['S'].append(complex(i[2], i[3]))
        bus['G'].append(i[4])
        bus['B'].append(i[5])
        bus['Ysh'].append(complex(i[4], i[5]))
        # Saída de dados do fluxo de carga
        bus['Vm'].append(1)
        bus['Va'].append(0)
    #Alimentadores
    if sub_file:
        gen['bus'] = feeders
        gen['sub'] = sub
    else:
        for i in mat['mpc'][0][0][3]:
            gen['bus'].append(int(i[0]))
            gen['sub'] = None
    #Linhas
    for i in mat['mpc'][0][0][4]:
        line['from_to'].append((int(i[0]), int(i[1])))
        line['r'].append(i[2])
        line['x'].append(i[3])
        line['Z'].append(complex(i[2], i[3]))
        line['max_i'].append((i[5]*Sb/Vb)/math.sqrt(3))
        line['status'].append(int(i[10]))
        # Saída de dados do fluxo de carga
        line['p_loss'].append(0)
        line['q_loss'].append(0)
        line['I'].append(0)
        line['loading'].append(0)
        

    #Adiciona num dict
    case = {'bus': bus, 'gen': gen, 'line': line, 'Sb': Sb, 'Vb': Vb}
    #Cria os resultados do fluxo de carga
    case['output'] = {
        'sum_p_loss': 0,                 
        'sum_q_loss': 0,                 
        'max_line_loading': 0,
        'max_sub_loading': 0,
        'min_voltage': [],
        'max_voltage': []
    }
    return case



"""
case = load_mpc('cases/case84.mat', 'pn_84_2.txt')
print case['gen']
rnp = dp2rnp(case)
case = run_pf(case, rnp, verbose=0)
print case['output'].keys()
#print case.keys()
#print case['gen'].keys()
sys.exit()
case_name = 'case84.csv'
case = load_case(case_name)
"""
case = load_mpc('cases/case84.mat', 'pn_84_2.txt')
with open('case84_limpo.p', 'w') as pfile:
    pk.dump(case, pfile)

#with open('case84.p', 'r') as pfile:
    #case = pk.load(pfile)

#print case.keys()
#print case['gen'].keys()
