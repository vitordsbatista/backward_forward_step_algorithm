#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
PowerFlow with NDE
Vitor Batista
vitordsbatista@gmail.com
Python 2.7
"""

import os
import cti
import pickle as pk
import math
import cmath
import csv
import sys
import copy
import bfnd as bn
import timeit
from itertools import compress
from timeit import default_timer as timer

path = 'case70/'
# Lê um caso qualquer
case = bn.load_case(path+'case.csv', cc=True)
rnp = bn.dp2rnp(case)

# Executa o PF e o CC
case = bn.run_pf(case, rnp)
for i in rnp:
    case, Z = bn.icc_rnp(case, rnp=rnp[i])
    for j in Z:
        print j,
        for k in Z[j]:
            print k,
        print

"""
for i, j in zip(case['bus']['bus'], case['bus']['Icc']):
    if i == 107:
        print i, 
        print j['1f'],
        print j['1fm']
#"""
#sys.exit()
# Lê os arquivos dos dispositivos e da zona de proteção
disp = {}
with open(path+'disp.txt', 'r') as f:
    reader = csv.reader(f, delimiter='\t')
    for i in reader:
        i = map(int, i)
        disp[(i[0], i[1])] = i[2]

# Lê a zona de proteção
zona = {}
with open(path+'zona.txt', 'r') as f:
    #reader = csv.reader(f)
    for i in f:
        i = i.strip()
        i = i.split()
        i = map(int, i)
        zona[(i[0], i[1])] = [i[2], i[3:]]
        zona_max_size = len(i[3:])

# -.- #
# Escreve os arquivos no formato do matlab
for r in rnp:
    # Junta as correntes com os dispositivos
    with open(path+'top'+r+'.txt', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        ft = bn.rnp2dp(rnp[r])
        ft = sorted(ft, key=lambda (k,v): k)
        for i in ft:
            tmp2 = case['line']['from_to'].index(i)
            if not i in disp.keys():
                i = i[::-1]
            de, para = i
            de = case['bus']['bus'].index(de)
            para = case['bus']['bus'].index(para)
            icc_de = case['bus']['Icc'][de]
            icc_para = case['bus']['Icc'][para]
            tmp = list(i)
            writer.writerow(
                tmp + [disp[i]] + [case['line']['I'][tmp2]]
                + [icc_de['3f'], icc_de['2f'],
                icc_de['1f'], icc_de['1fm'],
                icc_para['3f'], icc_para['2f'],
                icc_para['1f'], icc_para['1fm']]
            )
    with open(path+'zona'+r+'.txt', 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        ft = bn.rnp2dp(rnp[r])
        ft = sorted(ft, key=lambda (k,v): k)
        for i in ft:
            if i in zona.keys():
                writer.writerow(list(i) + [zona[i][0]] + zona[i][1])
            else:
                writer.writerow(list(i) + [1] + [0] * zona_max_size)

# Executa o matlab para cada rnp
path_ml = '/home/vitor/Dropbox/workspace/matlab/coordenacao'
path_atual = '/home/vitor/Dropbox/workspace/python/bfnd/'+path
for i in rnp:
    print '==============Executando a rnp==============', i
    os.system('matlab -nodisplay -nojvm -r "cd(\''+path_ml+'\');dim_coord_func(\''+path_atual+'\', \''+i+'\')"')
# -.- #

# Lê todas as topologias e junta elas numa única topologia
# Lê a topologia gravada
top = {}
for i in rnp:
    top.update(cti.leitura2(path, i))
# Salva o caso como pickle
with open(path+'case.p', 'w') as pfile:
    pk.dump(top, pfile)

# Salva a rnp
with open(path+'rnp.p', 'w') as pfile:
    pk.dump(rnp, pfile)

