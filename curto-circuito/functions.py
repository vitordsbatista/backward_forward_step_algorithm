#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Functions form metaheuristics
Vitor Batista
vitordsbatista@gmail.com
Python 2.7
"""

import sys
import copy as cp
import bfnd as bn
import warnings
import random as rd

def n_maneuvers(ind, from_to, status_oc, father_nm):
    """ Calculate  the number of maneuvers form some configuration based
    on the fathers number of maneuvers
        Parameters:
            from_to
            status_cs - Status current solution
            status_oc - Status original configuration
            father_nm - Father's solution number of maneuvers
            open_sw   - Which swich open
            close_sw  - Which swich close
        Output:
            nm - Number of maneuvers from some solution
    """

    status_cs = ind['status']
    open_sw = ind['open_line']
    close_sw = ind['close_line']
    # Caso as linhas abertas estejam erradas, inverte a odem delas
    if not open_sw in from_to:
        open_sw = (open_sw[1], open_sw[0])
    if not close_sw in from_to:
        close_sw = (close_sw[1], close_sw[0])
    ix_open = from_to.index(open_sw)
    ix_close = from_to.index(close_sw)

    if (status_oc[ix_open] == status_cs[ix_open]
            and status_oc[ix_close] == status_cs[ix_close]):
        return father_nm - 2
    elif (status_oc[ix_open] != status_cs[ix_open]
            and status_oc[ix_close] != status_cs[ix_close]):
        return father_nm + 2

    return father_nm

def rnp2dp(rnp):
    """
        Transforma uma rnp num de-para

        Parâmetros:
            rnp     -- representação nó profundidade
        Saída:
            dePara  -- de-para
    """
    dePara = []
    # Inverte a RNP
    rnp = rnp[::-1]
    for n, (i, j) in enumerate(rnp):
        for m, (k, o) in enumerate(rnp[n+1:]):
            if o == j-1:
                dePara.append((k, i))
                break
    return dePara

def for2stat(forest, ft):
    """ Extrai o status de uma floresta
    """
    status = [0] * len(ft)
    for i in forest:
        tree = forest[i]
        tree = tree[::-1]
        for n, (i, j) in enumerate(tree):
            for m, (k, o) in enumerate(tree[n+1:]):
                if o == j-1:
                    dp = k, i
                    if not dp in ft:
                        dp = i, k
                    status[ft.index(dp)] = 1
                    break
    return status

def pao(forest, from_tree, to_tree, p, a):
    P = forest[from_tree]           # Árvore de poda
    A = forest[to_tree]             # Árvore de enxerto
    # Index and depths
    ix_p = [x[0] for x in P].index(p)
    ix_a = [x[0] for x in A].index(a)
    # Depths
    dp_p = P[ix_p][1]
    dp_a = A[ix_a][1]
    
    # Encontra o nó pai (f) de p
    for i in P[ix_p-1::-1]:         # Parte do nó anterior a p
        if i[1] == dp_p-1:
            f = i[0]
            break

    subtree = []
    # Pesquisa a partir do índice do nó de poda até o último nó
    #  da subárvore
    subtree.append(                 # Add o p à subarvore
        P.pop(ix_p)
    )
    # NOTA: como o nó de poda foi removido, o ix_p que era pra conter o nó
    #  de poda agora será referente ao nó seguinte ao nó de poda
    for i in P[ix_p:]:              # Add a subárvore os nós pendurados no p
        if i[1] <= dp_p:
            break
        subtree.append(i)
        P.remove(i)

    # Atualiza a profundidade da subárvore com base na profundidade do
    #  nó de adjacência (dp_a)
    subtree = [(i[0], i[1]-dp_p+dp_a+1) for i in subtree]
    
    # Adiciona a subárvore na árvore de adjacência
    A[ix_a+1:ix_a+1] = subtree

    # Atualiza a floresta
    forest[from_tree] = P
    forest[to_tree] = A

    # Chaves que foram abertas e fechadas no processo
    try:
        open_line = (f, p)
        close_line = (p, a)
        return forest, open_line, close_line
    except:
        return forest

def cao(forest, from_tree, to_tree, p, r, a):
    P = forest[from_tree]           # Árvore de poda
    A = forest[to_tree]             # Árvore de enxerto
    
    # Index and depths
    ix_p = [x[0] for x in P].index(p)
    ix_r = [x[0] for x in P].index(r)
    ix_a = [x[0] for x in A].index(a)
    # Depths
    dp_p = P[ix_p][1]
    dp_r = P[ix_r][1]
    dp_a = A[ix_a][1]

    # Encontra o nó pai (f) de p
    for i in P[ix_p-1::-1]:         # Parte do nó anterior a p
        if i[1] == dp_p-1:
            f = i[0]
            break

    subtree = []
    # Pesquisa a partir do índice do nó de poda até o último nó
    #  da subárvore
    subtree.append(                 # Add o p à subarvore
        P.pop(ix_p)
    )
    # NOTA: como o nó de poda foi removido, o ix_p que era pra conter o nó
    #  de poda agora será referente ao nó seguinte ao nó de poda
    for i in P[ix_p:]:              # Add a subárvore os nós pendurados no p
        if i[1] <= dp_p:
            break
        subtree.append(i)
        P.remove(i)

    # Indice do nó raiz
    ix_s_r = [x[0] for x in subtree].index(r)
    # Profundidade do nó raiz
    dp_tmp = subtree[ix_s_r][1]

    # Encontra o indice dos nós do caminho de p até r
    path_r_p = []
    for i in subtree[ix_s_r::-1]:
        if i[1] == dp_tmp:
            path_r_p.append(
                subtree.index(i)
            )
            dp_tmp -= 1             # Atualiza a profundidade

    # Econtra as subárvores de cada nó no caminho de r até p
    # Será atualizado as variáveis dp_r e dp_a com as profundidades baseadas
    #  nos nós presentes no caminho de r até p
    new_subtree = []
    for n, i in enumerate(path_r_p):
        tmp = []
        node = subtree[i]
        tmp.append((node[0], dp_a+1))                   # Atualiza a profundidade
        dp_r = node[1]                                  # Atualização do dp_r
        for j in subtree[i+1:]:
            if j[1] <= node[1]:
                break
            if not j[0] in [x[0] for x in new_subtree]:    # Verifica redundância
                j = (j[0], j[1]-dp_r+dp_a+1)               # Atualiza a profundidade
                tmp.append(j)
        dp_a = tmp[0][1]                               # Atualização do dp_a
        new_subtree += tmp

    # Adiciona a subárvore na árvore de adjacência
    A[ix_a+1:ix_a+1] = new_subtree

    # Atualiza a floresta
    forest[from_tree] = P
    forest[to_tree] = A

    # Chaves que foram abertas e fechadas no processo
    #  *Necessário para o cálculo do número de manobras
    try:
        open_line = (f, p)
        close_line = (r, a)
        return forest, open_line, close_line
    except:
        return forest

def find_p(forest, r, from_tree):
    # Encontra o nó de poda para um determinado nó raiz
    id_r = [x for x, _ in forest[from_tree]].index(r)  # Id do r
    depth = forest[from_tree][id_r][1]-1               # Profundidade do r
    slice_tree = []
    for i in forest[from_tree][id_r-1:0:-1]:
        if depth == i[1]:
            slice_tree.append(i[0])                 # Árvore do r até a raiz
            depth = depth - 1

    # Se o nó de poda for uma subestação (slice_tree vazia),
    # será retornado nada
    if slice_tree:
        # Seleciona aleatoriamente o p
        p = rd.choice(slice_tree)
        return p
    else:
        return None

def mut(ind, ft, fault, sub_nodes):
    #TODO: comentar o início das funções
    #TODO²: Colocar uma excessão quando uma árvore for muito pequena
    # com isso, não vai dar pra encontrar o nó de poda
    
    forest = ind['rnp']
    status = ind['status']


    # Chaves abertas do sistema fora a falta
    open_lines = [i for i, j in zip(ft, status) if j == 0]

    # Seleciona aleatoriamente uma chave aberta
    s_line = list(rd.choice(open_lines))
    # Define aleatoriamente o nó podaRaiz e o no adjacente
    rd.shuffle(s_line)
    pr, a = s_line
    # O nó podaRaiz nunca poderá ser uma subestação
    while pr in sub_nodes:
        s_line = list(rd.choice(open_lines))
        rd.shuffle(s_line)
        pr, a = s_line
        
    # Localiza as árvores onde estão cada nó (pode ser otimizado)
    from_tree = None
    to_tree = None
    for i in forest:
        if pr in [x for x, y in forest[i]]:
            from_tree = i
        if a in [x for x, y in forest[i]]:
            to_tree = i
        if from_tree and to_tree:
            break

    # Algoritmo para localizar os nós 'pr' e 'a' dentro de uma mesma rnp 
    # TODO
    if from_tree == to_tree:
        return None

    # Escolhe se será executado o pao ou cao
    op = rd.randint(0, 1)
    # Ececuta o PAO/CAO
    if op == 0:         # PAO
        rnp_mut, open_line, close_line = pao(
            forest, from_tree, to_tree, pr, a
        )
    elif op == 1:       # CAO
        r = pr
        p = find_p(forest, r, from_tree)
        # Caso o nó de poda seja uma subestação, será executado o PAO
        if p:
            rnp_mut, open_line, close_line = cao(
                forest, from_tree, to_tree, p, r, a
            )
        else:
            rnp_mut, open_line, close_line = pao(
                forest, from_tree, to_tree, r, a
            )

    # Status do indivíduo mutado
    new_status = for2stat(forest, ft)
    new_ind = {
        'rnp': rnp_mut,
        'status': new_status,
        'open_line': open_line,
        'close_line': close_line
    }
    return new_ind

def pcf(forest, ft, ft_off, status, status_off, fault, case):
    """
        Geração da primeira configuração factível
    """
    # Extrai o trecho isolado pela falta
    rnp_id = -1
    for i in forest:
        # Verifica se a falta está em uma rnp
        rnp = forest[i]
        if fault in [x for x, _ in rnp]:
            rnp_id = i
            break

    # Erro caso o nó de falta não esteja na floresta
    if rnp_id == -1:
        raise ValueError(
            "O nó {} não está presente no sistema elétrico".format(fault)
        )
    # Extrai a subarvore de falta
    # TODO: Organizar
    fault_id = [x for x, _ in rnp].index(fault)
    fault = rnp.pop(fault_id)

    depth = fault[1]
    depth_inc = fault[1]
    rnp_tmp = rnp
    rnp_off = []

    for i in rnp_tmp[fault_id:]:          # Elemento posterior a falta
        _, dp = i
        if dp <= depth:
            break
        else:
            rnp_off.append(i)
            rnp.remove(i)
            depth_inc = dp

    # Warning caso o nó de falta esteja no final do trecho
    if not rnp_off:
        message = "O nó {} está no fim do trecho, nada a fazer".format(fault[0])
        warnings.warn(message, RuntimeWarning)
        return forest                       # Retorna a floresta sem o nó de falta

    # Chaves NA (tie)
    open_lines = [i for i, j in zip(ft, status) if j == 0]

    # Splita a rnp_off em subárvores
    subtrees = []
    # Encontra os índices das raizes das subárvores
    tmp = [n for n, i in enumerate(rnp_off) if i[1] == depth+1]
    # Extrai as subárvores de rnp_off
    subtrees = [rnp_off[i:j] for i, j in zip(tmp[:-1], tmp[1:])]
    subtrees.append(rnp_off[j:])

    # Dict com as combinações possiveis de religamento
    po_switch = dict.fromkeys(range(len(subtrees)))
    for n, sub in enumerate(subtrees):
        po_switch[n] = []
        for m, i in enumerate(sub):
            for j in open_lines:
                if i[0] in j:
                    if m == 0:              # PAO
                        po_switch[n].append([i[0], j, 0])
                    else:                   # CAO
                        po_switch[n].append([i[0], j, 1])

    
    # Seleciona uma chave aleatória e aplica o CAO/PAO nela
    # TODO: comentar e organizar isso
    for i in po_switch:
        choose = rd.choice(po_switch[i])
        if choose[2] == 0:                  # Aplica o PAO
            rnp_from = subtrees[i]
            p = choose[0]
            switch = choose[1]
            # Nó de adjacencia
            a = switch[0] if switch[1] == p else switch[1]
            # Encontra a rnp do nó de adjacência
            rnp_a_id = [f for f in forest for x in forest[f] if a == x[0]][0]
            rnp_to = forest[rnp_a_id]
            # Floresta temporária para a execução do PAO
            forest_tmp = {
                0: rnp_from,
                1: rnp_to
            }
            # PAO
            forest_tmp = pao(forest_tmp, 0, 1, p, a)
            # Atualiza a floresta
            forest[rnp_a_id] = forest_tmp[1]
        elif choose[2] == 1:                # Aplica o CAO
            rnp_from = subtrees[i]
            p = rnp_from[0][0]
            r = choose[0]
            switch = choose[1]
            # Nó de adjacencia
            a = switch[0] if switch[1] == r else switch[1]
            # Encontra a rnp do nó de adjacência
            rnp_a_id = [f for f in forest for x in forest[f] if a == x[0]][0]
            rnp_to = forest[rnp_a_id]
            # Floresta temporária para a execução do PAO
            forest_tmp = {
                0: rnp_from,
                1: rnp_to
            }
            # PAO
            forest_tmp = cao(forest_tmp, 0, 1, p, r, a)
            # Atualiza a floresta
            forest[rnp_a_id] = forest_tmp[1]
    # === Avaliação da pcf === #
    # Status da pcf com a falta
    status_pcf = for2stat(forest, ft_off)
    # Número de manobras da pcf
    nm_pcf = sum([i ^ j for i, j in zip(status_pcf, status)])
    # Avalia a pcf
    fit_pcf = eval_pcf(case, forest, nm_pcf)
    # Status da pcf sem a falta
    status_pcf_off = for2stat(forest, ft_off)
    pcf = {
        'rnp': forest,
        'fit': fit_pcf,
        'status': status_pcf_off
    }
    return pcf

def testes():
    rnp1 = [
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 3),
        (5, 4),
        (13, 3),
        (14, 4),
        (15, 5)
    ]

    rnp2 = [
        (6, 1),
        (7, 2),
        (8, 3),
        (9, 4),
        (10, 4),
        (11, 5),
        (20, 5),
        (21, 6)
    ]

    forest = {
        0: rnp1,
        1: rnp2
    }

    ft = [
        (1, 2),
        (2, 3),
        (2, 4),
        (4, 5),
        (2, 13),
        (13, 14),
        (14, 15),
        (6, 7),
        (7, 8),
        (8, 9),
        (8, 10),
        (10, 11),
        (10, 20),
        (20, 21),
        (3, 9),
        (4, 11),
        (21, 15),
        #(15, 21)
        (14, 9), 
        (15, 11),
        (13, 8)
    ]
    status = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]

    #forest = pao(f, from_tree=0, to_tree=1, p=4, a=11)
    #forest = cao(f, from_tree=1, to_tree=0, p=8, r=9, a=3)
    #forest, o, c = cao(forest, from_tree=1, to_tree=0, p=8, r=11, a=4)

    #forest = mut(forest, 1, ft, status)

    forest = pcf(forest, ft, status, 2)
    for i in forest:
        print i, forest[i]
    #pcf(forest, ft, status, 7)
    #pcf(forest, ft, status, 9)


    #for i in forest: print i, forest[i]

def eval(case, ind, ft, status_oc, father_nm):
    case = cp.deepcopy(case)
    forest = ind['rnp']
    # Fluxo de carga
    try:
        data = bn.run_pf(case, forest)
        data = data['output']
        # Os valores de menor tensão e maior tensão serão utilizados
        # para visualização, o valor utilizado na fit é o 'voltage'
        fit = {
            'p_loss': data['sum_p_loss'],
            'max_v': data['max_voltage'],
            'min_v': data['min_voltage'],
            'line_load': data['max_loading'], 
            'sub_load': data['max_loading_sub']
        }
        # Cálculo do 'voltage'
        min_v = abs(1-data['min_voltage'][1])
        max_v = abs(1-data['max_voltage'][1])
        fit['voltage'] = max(min_v, max_v)

        # Cálculo do número de manobras
        fit['nm'] = n_maneuvers(ind, ft, 
                                status_oc, 
                                father_nm)

        # Cálculo da função de agregação
        fit['agg'] = agg(fit)
    except:
        # Cálculo do número de manobras
        nm = n_maneuvers(ind, ft, 
                         status_oc, 
                         father_nm)
        fit = {'agg': float('inf'),
               'nm': nm}
    return fit

def eval_pcf(case, forest, nm):
    case = cp.deepcopy(case)
    data = bn.run_pf(case, forest)
    data = data['output']
    # Os valores de menor tensão e maior tensão serão utilizados
    # para visualização, o valor utilizado na fit é o 'voltage'
    fit = {
        'p_loss': data['sum_p_loss'],
        'max_v': data['max_voltage'],
        'min_v': data['min_voltage'],
        'line_load': data['max_loading'], 
        'sub_load': data['max_loading_sub']
    }
    # Cálculo do 'voltage'
    min_v = abs(1-data['min_voltage'][1])
    max_v = abs(1-data['max_voltage'][1])
    fit['voltage'] = max(min_v, max_v)

    # Número de manobras
    fit['nm'] = nm

    # Cálculo da função de agregação
    fit['agg'] = agg(fit)
    return fit

def agg(fit):
    out = fit['nm']
    vg = fit['voltage']
    xg = fit['line_load']
    bg = fit['sub_load']
    if vg > 0.05:
        out += vg * 1000
    if xg > 100:
        out += xg * 100
    if bg > 100:
        out += bg * 100
    return out

def plot(bests):
    plt.subplot(2, 3, 1)
    plt.title("Numero de manobras")
    tmp = [x['nm'] for x in bests]
    plt.plot(tmp, color='r')

    plt.subplot(2, 3, 2)
    plt.title("Func de agregacao")
    tmp = [x['agg'] for x in bests]
    plt.plot(tmp, color='g')

    plt.subplot(2, 3, 3)
    plt.title("Perdas ativas")
    tmp = [x['p_loss'] for x in bests]
    plt.plot(tmp, color='b')

    plt.subplot(2, 3, 4)
    plt.title("Carr linhas")
    tmp = [x['line_load'] for x in bests]
    plt.plot([0, len(tmp)], [100, 100], '--', 
             color='k', linewidth=0.8)
    plt.plot(tmp, color='c')

    plt.subplot(2, 3, 5)
    plt.title("Carr subestacao")
    tmp = [x['sub_load'] for x in bests]
    plt.plot([0, len(tmp)], [100, 100], 
             '--', color='k', linewidth=0.8)
    plt.plot(tmp, color='m')

    plt.subplot(2, 3, 6)
    plt.title("Tensao")
    tmp1 = [x['max_v'][1] for x in bests]
    tmp2 = [x['min_v'][1] for x in bests]
    tmp = []
    for i, j in zip(tmp1, tmp2):
        i1 = 1-i
        j1 = 1-j
        if i1 > j1:
            tmp.append(i)
        else:
            tmp.append(j)
    plt.plot([0, len(tmp)], [0.95, 0.95], 
             '--', color='k', linewidth=0.8)
    plt.plot([0, len(tmp)], [1.05, 1.05], 
             '--', color='k', linewidth=0.8)
    plt.plot(tmp, color='y')

    plt.show()

def plano_manobra(ft, status_oc, ind):
    status_cs = for2stat(ind['rnp'], ft)
    status = [i ^ j for i, j in zip(status_oc, status_cs)]
    plano = []
    for n, [i, j] in enumerate(zip(status, status_cs)):
        if i:
            if j:
                plano.append("Fecha {}".format(ft[n]))
            else:
                plano.append("Abre {}".format(ft[n]))
    return plano
