#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Evolutionary Strategies with bfnd
Vitor Batista
vitordsbatista@gmail.com
Python 2.7
"""

import functions as f
import sys
import copy as cp
#import matplotlib.pyplot as plt
import random as rd
import pickle as pk
import bfnd as bn
import time

#============================================================================#
def ee(case, fault, mi=10, lam=5, it=1000, verbose=False):
    # Dados iniciais
    status_oc = case['line']['status']
    ft_oc = case['line']['from_to']
    sub_nodes =  case['gen']['bus']        # Não poderão ser desligados
    bests = []

    # Remove a falta do de-para original
    ft = [(i, j) for i, j in ft_oc if i != fault and j != fault]
    status_oc_off = [j for i, j in zip(ft_oc, status_oc) if i[0] != fault and i[1] != fault]

    # Gera a primeira RNP
    rnp = bn.dp2rnp(case)
    rnp_zip = dict()
    for i in rnp:
        rnp_zip[i] = zip(*rnp[i])

    # ========== Primeira Configuração Factível ========== #
    # Gera a primeira configuração factível
    pcf = f.pcf(rnp_zip, ft_oc, ft, 
                status_oc, status_oc_off,
                fault, case)
    
    # ========== População Inicial ========== #
    parents = [pcf]

    while len(parents) < mi:
        # Escolhe aleatoriamente um indivíduo dos pais
        someone = cp.deepcopy(rd.choice(parents))
        # Realiza a mutação no indivíduo selecionado e gera uma nova solução
        newone = f.mut(someone, ft, fault, sub_nodes)
        if newone:
            # Avalia o novo indivíduo
            father_nm = someone['fit']['nm']
            newone['fit'] = f.eval(case, newone, 
                                   ft, status_oc_off, 
                                   father_nm)
            # Adiciona ele aos pais
            parents.append(newone)
    
    # ========== Iterações ========== #
    for i in range(it):
        # Gera os filhos
        offspring = []
        
        while len(offspring) < lam:
            # Escolhe aleatoriamente um indivíduo dos pais
            someone = cp.deepcopy(rd.choice(parents))
            # Realiza a mutação no indivíduo selecionado e gera uma nova solução
            newone = f.mut(someone, ft, 
                           fault, sub_nodes)
            if newone:
                # Avalia o novo indivíduo
                father_nm = someone['fit']['nm']
                newone['fit'] = f.eval(case, newone, 
                                       ft, status_oc_off, 
                                       father_nm)
                # Adiciona ele aos filhos
                offspring.append(newone)

        # Junta os pais com os filhos
        pop = offspring + parents

        # Ordena com base na função de agregação
        pop = sorted(pop, key=lambda k: k['fit']['agg'])

        # Seleciona as mi melhores soluções
        parents = pop[:mi]

        # Armazena a fit da melhor solução
        bests.append(pop[0]['fit'])

        if verbose:
            print "Iteração:", i
            print "\tMelhor:", pop[0]['fit']['agg']
            print "\tManobras", pop[0]['fit']['nm']
            print "\tTensão máxima", pop[0]['fit']['max_v'][1]
            print "\tTensão mínima", pop[0]['fit']['min_v'][1]
            print "\tCarr. linhas", pop[0]['fit']['line_load']
            print "\tCarr. subestação", pop[0]['fit']['sub_load']

    best_guy = pop[0]
    return bests, ft_oc, status_oc, best_guy

# Carrega um caso
with open('case84_limpo2.p', 'r') as pfile:
    case = pk.load(pfile)

# Estratégias evolutivas
t0 = time.time()
bests, ft, status_oc, best = ee(case, 18, mi=20, lam=10, it=500, verbose=True)
print 'Tempo de execução {:5.2f}'.format(time.time() - t0)

p = f.plano_manobra(ft, status_oc, best)
#for i in p: 
    #print i
