#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Evolutionary Strategies with bfnd
Vitor Batista
vitordsbatista@gmail.com
Python 2.7
"""

import math
import cmath
import sys
sys.path.insert(0, '../')
import bfnd as bn

# Carrega um caso
#with open('case84_limpo2.p', 'r') as pfile:
#    case = pk.load(pfile)
# Gera a primeira RNP
case = bn.load_case('case33.csv', cc=True)
rnp = bn.dp2rnp(case)

def icc_rnp(data, Scc, Iccs, RX, Z2_Z1, X0_X1, R0_X0, Rf, bus=None, rnp=False):
    ft = data['line']['from_to']
    status = data['line']['status']
    Vb = data['Vb']
    phi = math.atan(1/RX)
    Zcc = Vb**2/Scc
    # Converter o Scc de polar para retangular
    z1 = cmath.rect(Zcc, phi)   # Seq positiva
    z2 = z1 * Z2_Z1             # Seq negativa
    # Seq Zero
    x0 = X0_X1 * z1.imag
    r0 = R0_X0 * x0
    z0 = complex(r0, x0)

    ft_rnp = [i for i, j in zip(ft, status) if j ]
    if not rnp:
        rnp = bn.dp2rnp(ft_rnp)
    if not bus or bus == 'all': 
        bus = [x[0] for x in rnp]
        buses = bus
    else:
        buses = [x[0] for x in rnp]
    Icc = dict.fromkeys(bus)
    Z = dict.fromkeys(bus)
    for b in bus:
        # Id da barra
        b_ix = buses.index(b)
        # Nó-Profundidade da barra
        b_nd = rnp[b_ix]
        # Splita a rnp no ponto da barra
        rnp_tmp = rnp[:b_ix+1]
        # Inverte a rnp
        rnp_tmp = rnp_tmp[::-1]
        # Encontra o caminha de b até a subestação na rnp
        depth = b_nd[1]
        path = [b_nd]
        for i in rnp_tmp[1:]:
            if depth-1 == i[1]:
                path.append(i)
                depth -= 1

        # Encontra o de-para de b até a subestação
        ft_tmp = []
        for n, i in enumerate(path):
            for j in path[n+1:]:
                if i[1]-1 == j[1]:
                    ft_tmp.append((j[0], i[0]))
                    break

        # == Icc Trifásico == #
        # Calcula o Z1th de b
        Z1th = 0
        for i in ft_tmp:
            if i not in ft:
                i = i[::-1]
            i_ix = ft.index(i)
            Z1th += data['line']['Z1_ohm'][i_ix]
        Z1th_t = Z1th + z1

        Vf = Vb/math.sqrt(3)
        Icc3f = Vf/Z1th_t
        Icc3f *= 1000

        # == Icc Bifásico == #
        Icc2f = (math.sqrt(3)/2)*Icc3f

        # == Icc Fase-terra == #
        # Calcula o Z2th de b
        Z2th = 0
        for i in ft_tmp:
            if i not in ft:
                i = i[::-1]
            i_ix = ft.index(i)
            Z2th += data['line']['Z2_ohm'][i_ix]
        Z2th_t = Z2th + z2
        # Calcula o Z0th de b
        Z0th = 0
        for i in ft_tmp:
            if i not in ft:
                i = i[::-1]
            i_ix = ft.index(i)
            Z0th += data['line']['Z0_ohm'][i_ix]
        Z0th_t = Z0th + z0

        Vf = Vb/math.sqrt(3)
        Icc1f = (3*Vf)/(Z0th_t + Z1th_t + Z2th_t)
        Icc1f *= 1000
        
        # == Icc Fase-terra mínimo== #
        Icc1fm = (3*Vf)/(Z0th_t + Z1th_t + Z2th_t + 3*Rf)
        Icc1fm *= 1000

        Icc[b] = {'3f': abs(Icc3f),
                  '2f': abs(Icc2f),
                  '1f': abs(Icc1f),
                  '1fm': abs(Icc1fm)
                 }
        Z[b] = [Z0th_t.real, Z0th_t.imag,
                Z1th_t.real, Z1th_t.imag,
                Z2th_t.real, Z2th_t.imag]
    return Icc, Z

rnp = rnp['0']
RX = 0.1
Z2_Z1 = 1
X0_X1 = 1
R0_X0 = 0.1
Rf=40
Icc, Z = icc_rnp(case, Scc=131.5666, bus='all', 
              Iccs=['3f', '2f', 'ft', 'ftm'],
              RX=RX, Z2_Z1=Z2_Z1, X0_X1=X0_X1, 
              R0_X0=R0_X0, Rf=Rf, rnp=rnp)

for i in Z:
    print '{:.6f},{:.6f},{:.6f},{:.6f},{:.6f},{:.6f}'.format(Z[i][0], Z[i][1], Z[i][2],
                                                            Z[i][3], Z[i][4], Z[i][5])
print '{:^7}{:>9}{:>11}{:>11}{:>11}'.format('Barra', 
                                            'Icc 3f', 
                                            'Icc 2f', 
                                            'Icc ft', 
                                            'Icc ftm')
for i in Icc:
    print '{:^7}{:>11.4f}{:>11.4f}{:>11.4f}{:>11.4f}'.format(i, Icc[i]['3f'], 
                                                             Icc[i]['2f'], 
                                                             Icc[i]['1f'], 
                                                             Icc[i]['1fm'])
for i in Icc:
    print '{:},{:.4f},{:.4f},{:.4f},{:.4f}'.format(i, Icc[i]['3f'], 
                                                             Icc[i]['2f'], 
                                                             Icc[i]['1f'], 
                                                             Icc[i]['1fm'])
"""
# ==== Curto Circuito ==== #
ft = case['line']['from_to']
rnp = rnp['6']
nodes = [58]
rnp = rnp[::-1]
icc = {}
for n, (i, j) in enumerate(rnp):
    icc[i] = {}
    icc3f = 0
    node = i
    dp = j
    for p, q in rnp[n+1:]:
        if q == dp-1:
            father = (p, node)
            if not father in ft:
                father = (father[1], father[0])
            father_ix = ft.index(father)
            icc3f = case['line']['Z'][father_ix]
            node = p
            dp = q

    icc[i]['3f'] = icc3f

    break
print icc[62]
"""
