# -*- coding: utf-8 -*-
import math
import timeit
import gc
import copy as cp
import sys

g = [
    (12, 13),
    (13, 14),
    (14, 15),
    (14, 16),
    (16, 21),
    (13, 49),
    (14, 81),
    (15, 86)
]

def criaZona(grafo, no, top, zona=None, visitados=None, numDisp=0):
    if zona is None:
        zona = []
    if visitados is None:
        visitados = []
    visitados.append(no)
    isEnd = True
    for i in grafo:
        if i[0] == no:
            isEnd = False
            if i[1] not in visitados:
                if numDisp > 0:
                    zona.append(no)
                else:
                    if i in top.keys():
                        if top[i]['type'] != 1:
                            visitados, zona = criaZona(grafo, i[1], top, zona, visitados, numDisp+1)
                        else:
                            visitados, zona = criaZona(grafo, i[1], top, zona, visitados, numDisp)
                    elif i[::-1] in top.keys():
                        if top[i[::-1]]['type'] != 1:
                            numDisp += 1
                            visitados, zona = criaZona(grafo, i[1], top, zona, visitados, numDisp+1)
                        else:
                            visitados, zona = criaZona(grafo, i[1], top, zona, visitados, numDisp)
                    else:
                        visitados, zona = criaZona(grafo, i[1], top, zona, visitados, numDisp)
    if isEnd:
        zona.append(no)
    return visitados, zona


top = {
    (12, 13): {'type': 2},
    (13, 14): {'type': 3},
    (14, 15): {'type': 5},
    (14, 16): {'type': 5},
    (16, 21): {'type': 5},
    (13, 49): {'type': 5},
    (14, 81): {'type': 5},
    (15, 86): {'type': 1}
}

l = cp.deepcopy(top.keys())
l = sorted(l)

#i = (13, 14)
#_, zona = criaZona(g, i[1], top)
#zona = list(set(zona))
#print zona
#sys.exit()

for i in l:
    if top[i]['type'] != 1:
        _, zona = criaZona(g, i[1], top)
        zona = list(set(zona))
        print i, 
        print zona
        print '--'

# Nota: Bug com a declaração de variáveis na chamada da função + função recursiva + laços de repetição
