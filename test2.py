#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import timeit
import sys

def dfs(graph, start):
    visited, stack = [], [[start, 1]]
    while stack:
        vertex = stack.pop()
        if vertex not in visited:
            visited.append(vertex)
            if vertex[0] in graph.keys():
                tmp = set([i[0] for i in visited]).intersection(graph[vertex[0]]).symmetric_difference(graph[vertex[0]]),
                for i in tmp[0]:
                    stack.append([i, vertex[1]+1])
    visited = np.transpose(np.array(visited))
    return visited
#    0  1  2  3   4
"""
a = [1, 4, 6, 8 ,10]
b = [1, 8]
#1
index = lambda y: a.index(y)
print map(index, b)
t1 = timeit.timeit('map(index, b)', setup='from __main__ import a, b, index')
#2
tmp = [a.index(x) for x in b]
t2 =  timeit.timeit('tmp=[a.index(x) for x in b]', setup='from __main__ import a, b, index')
print t1
print t2

a = [
    (1, 2)
]
a = a * 5000

#b = [(j, i) for i, j in a]
print timeit.timeit('b = [(j, i) for i, j in a]', setup='from __main__ import a', number=10000)

#b = [i[::-1] for i in a]
print timeit.timeit('[i[::-1] for i in a]', setup='from __main__ import a', number=10000)

#b = [tuple(reversed(i)) for i in a]
print timeit.timeit('[tuple(reversed(i)) for i in a]', setup='from __main__ import a', number=10000)

#b = map(x, a)
x = lambda x: tuple(reversed(x))
print timeit.timeit('map(x, a)', setup='from __main__ import a, x', number=10000)

"""

a = [1, 2, 2, 3, 3, 4, 4, 3, 3, 4, 3, 1, 3, 4]
def indexes(data, value):
    t = -1
    out = []
    while True:
        try:
            t = a.index(value, t + 1)
            out.append(t)
        except ValueError:
            break
    return out
def indexes2(data, value):
    out = []
    for n, i in enumerate(data):
        if i == value:
            out.append(n)
    return out
def indexes3(data, value):
    return [n for n, i in enumerate(data) if i==value]

print timeit.timeit('indexes(a, 3)', setup='from __main__ import a, indexes')
print timeit.timeit('indexes2(a, 3)', setup='from __main__ import a, indexes2')
print timeit.timeit('indexes3(a, 3)', setup='from __main__ import a, indexes3')
