function mpc = node_135
% These data are collected from:
% Jos� R. S. Mantovani(1) Fernando Casari(2) Rub�n A. Romero(1),
% Reconfigura��o de sistemas de distribui��o radiais utilizando o crit�rio de queda de tens�o

%% MATPOWER Case Format : Version 2
mpc.version = '2';

%%-----  Power Flow Data  -----%%
%% system MVA base
mpc.baseMVA = 1;   %% resultado obtido!! Sistema apto a ser escolhido
%% system kV base
Vb = 13.8;% Valor de Tens�o dada em kV

%% bus data
% bus_i	type	Pd	Qd	Gs	Bs	area	Vm	Va	baseKV	zone	Vmax	Vmin
mpc.bus = [
 
1   3	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
2	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
3	1	0.047780	0.019010	0	0	1	1	0	Vb	1	1.05	0.95
4	1	0.042550	0.016930	0	0	1	1	0	Vb	1	1.05	0.95
5	1	0.087020	0.034620	0	0	1	1	0	Vb	1	1.05	0.95
6	1	0.311310	0.123855	0	0	1	1	0	Vb	1	1.05	0.95
7	1	0.148869	0.059230	0	0	1	1	0	Vb	1	1.05	0.95
8	1	0.238672	0.094960	0	0	1	1	0	Vb	1	1.05	0.95
9	1	0.062300	0.024790	0	0	1	1	0	Vb	1	1.05	0.95
10	1	0.124598	0.049570	0	0	1	1	0	Vb	1	1.05	0.95
11	1	0.140175	0.055770	0	0	1	1	0	Vb	1	1.05	0.95
12	1	0.116813	0.046470	0	0	1	1	0	Vb	1	1.05	0.95
13	1	0.249203	0.099150	0	0	1	1	0	Vb	1	1.05	0.95
14	1	0.291447	0.115952	0	0	1	1	0	Vb	1	1.05	0.95
15	1	0.303720	0.120835	0	0	1	1	0	Vb	1	1.05	0.95
16	1	0.215396	0.085700	0	0	1	1	0   Vb	1	1.05	0.95
17	1	0.198586	0.079010	0	0	1	1	0	Vb	1	1.05	0.95

18	3	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
19	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
20	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
21	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
22	1	0.030127	0.014729	0	0	1	1	0	Vb	1	1.05	0.95
23	1	0.230972	0.112920	0	0	1	1	0	Vb	1	1.05	0.95
24	1	0.060256	0.029458	0	0	1	1	0	Vb	1	1.05	0.95
25	1	0.230972	0.112920	0	0	1	1	0	Vb	1	1.05	0.95
26	1	0.120507	0.058915	0	0	1	1	0	Vb	1	1.05	0.95
27	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
28	1	0.056981	0.027857	0	0	1	1	0	Vb	1	1.05	0.95
29	1	0.364665	0.178281	0	0	1	1	0	Vb	1	1.05	0.95
30	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
31	1	0.124647	0.060939	0	0	1	1	0	Vb	1	1.05	0.95
32	1	0.056981	0.027857	0	0	1	1	0	Vb	1	1.05	0.95
33	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
34	1	0.085473	0.041787	0	0	1	1	0	Vb	1	1.05	0.95
35	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
36	1	0.396735	0.193960	0	0	1	1	0	Vb	1	1.05	0.95
37	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
38	1	0.181152	0.088563	0	0	1	1	0	Vb	1	1.05	0.95
39	1	0.242172	0.118395	0	0	1	1	0	Vb	1	1.05	0.95
40	1	0.075316	0.036821	0	0	1	1	0	Vb	1	1.05	0.95

41	3	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
42	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
43	1	0.001254	0.000531	0	0	1	1	0	Vb	1	1.05	0.95
44	1	0.006274	0.002660	0	0	1	1	0	Vb	1	1.05	0.95
45	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
46	1	0.117880	0.049971	0	0	1	1	0	Vb	1	1.05	0.95
47	1	0.062668	0.026566	0	0	1	1	0	Vb	1	1.05	0.95
48	1	0.172285	0.073034	0	0	1	1	0	Vb	1	1.05	0.95
49	1	0.458556	0.194388	0	0	1	1	0	Vb	1	1.05	0.95
50	1	0.262962	0.111473	0	0	1	1	0	Vb	1	1.05	0.95
51	1	0.235761	0.099942	0	0	1	1	0	Vb	1	1.05	0.95
52	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
53	1	0.109215	0.046298	0	0	1	1	0	Vb	1	1.05	0.95
54	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
55	1	0.072809	0.030865	0	0	1	1	0	Vb	1	1.05	0.95
56	1	0.258473	0.109570	0	0	1	1	0	Vb	1	1.05	0.95
57	1	0.069169	0.029322	0	0	1	1	0	Vb	1	1.05	0.95
58	1	0.021843	0.009260	0	0	1	1	0	Vb	1	1.05	0.95
59	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
60	1	0.020527	0.008702	0	0	1	1	0	Vb	1	1.05	0.95
61	1	0.150548	0.063819	0	0	1	1	0	Vb	1	1.05	0.95
62	1	0.220687	0.093552	0	0	1	1	0	Vb	1	1.05	0.95
63	1	0.092384	0.039163	0	0	1	1	0	Vb	1	1.05	0.95
64	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
65	1	0.226693	0.096098	0	0	1	1	0	Vb  1	1.05	0.95

66	3	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
67	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
68	1	0.294016	0.116974	0	0	1	1	0	Vb	1	1.05	0.95
69	1	0.083015	0.033028	0	0	1	1	0	Vb	1	1.05	0.95
70	1	0.083015	0.033028	0	0	1	1	0	Vb	1	1.05	0.95
71	1	0.103770	0.041285	0	0	1	1	0	Vb	1	1.05	0.95
72	1	0.176408	0.070184	0	0	1	1	0	Vb	1	1.05	0.95
73	1	0.083015	0.033028	0	0	1	1	0	Vb	1	1.05	0.95
74	1	0.217917	0.086698	0	0	1	1	0	Vb	1	1.05	0.95
75	1	0.023294	0.009267	0	0	1	1	0	Vb	1	1.05	0.95
76	1	0.005075	0.002019	0	0	1	1	0	Vb	1	1.05	0.95
77	1	0.072638	0.028899	0	0	1	1	0	Vb	1	1.05	0.95
78	1	0.405990	0.161523	0	0	1	1	0	Vb	1	1.05	0.95

79	3	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
80	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
81	1	0.100182	0.042468	0	0	1	1	0	Vb	1	1.05	0.95
82	1	0.142523	0.060417	0	0	1	1	0	Vb	1	1.05	0.95
83	1	0.096042	0.040713	0	0	1	1	0	Vb	1	1.05	0.95
84	1	0.300454	0.127366	0	0	1	1	0	Vb	1	1.05	0.95
85	1	0.141238	0.059873	0	0	1	1	0	Vb	1	1.05	0.95
86	1	0.279847	0.118631	0	0	1	1	0	Vb	1	1.05	0.95
87	1	0.087312	0.037013	0	0	1	1	0	Vb	1	1.05	0.95
88	1	0.243849	0.103371	0	0	1	1	0	Vb	1	1.05	0.95
89	1	0.247750	0.105025	0	0	1	1	0	Vb	1	1.05	0.95

90	3	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
91	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
92	1	0.089878	0.038101	0	0	1	1	0	Vb	1	1.05	0.95
93	1	1.137280	0.482108	0	0	1	1	0	Vb	1	1.05	0.95
94	1	0.458339	0.194296	0	0	1	1	0	Vb	1	1.05	0.95
95	1	0.385197	0.163290	0	0	1	1	0	Vb	1	1.05	0.95
96	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
97	1	0.079608	0.033747	0	0	1	1	0	Vb	1	1.05	0.95
98	1	0.087312	0.037013	0	0	1	1	0	Vb	1	1.05	0.95
99	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
100	1	0.074001	0.031370	0	0	1	1	0	Vb	1	1.05	0.95
101	1	0.232050	0.098369	0	0	1	1	0	Vb	1	1.05	0.95
102	1	0.141819	0.060119	0	0	1	1	0	Vb	1	1.05	0.95
103	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
104	1	0.076449	0.032408	0	0	1	1	0	Vb	1	1.05	0.95

105	3	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
106	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
107	1	0.051322	0.021756	0	0	1	1	0	Vb	1	1.05	0.95
108	1	0.059874	0.025381	0	0	1	1	0	Vb	1	1.05	0.95
109	1	0.009065	0.003843	0	0	1	1	0	Vb	1	1.05	0.95
110	1	0.002092	0.000887	0	0	1	1	0	Vb	1	1.05	0.95
111	1	0.016735	0.007094	0	0	1	1	0	Vb	1	1.05	0.95
112	1	1.506522	0.638634	0	0	1	1	0	Vb	1	1.05	0.95
113	1	0.313023	0.132694	0	0	1	1	0	Vb	1	1.05	0.95
114	1	0.079831	0.033842	0	0	1	1	0	Vb	1	1.05	0.95
115	1	0.051322	0.021756	0	0	1	1	0	Vb	1	1.05	0.95
116	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
117	1	0.202435	0.085815	0	0	1	1	0	Vb	1	1.05	0.95
118	1	0.060823	0.025784	0	0	1	1	0	Vb	1	1.05	0.95
119	1	0.045618	0.019338	0	0	1	1	0	Vb	1	1.05	0.95
120	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
121	1	0.157070	0.066584	0	0	1	1	0	Vb	1	1.05	0.95
122	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
123	1	0.250148	0.106041	0	0	1	1	0	Vb	1	1.05	0.95
124	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
125	1	0.069809	0.029593	0	0	1	1	0	Vb	1	1.05	0.95
126	1	0.032072	0.013596	0	0	1	1	0	Vb	1	1.05	0.95
127	1	0.061084	0.025894	0	0	1	1	0	Vb	1	1.05	0.95

128	3	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
129	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
130	1	0.094622	0.046260	0	0	1	1	0	Vb	1	1.05	0.95
131	1	0.049858	0.024375	0	0	1	1	0	Vb	1	1.05	0.95
132	1	0.123164	0.060214	0	0	1	1	0	Vb	1	1.05	0.95
133	1	0.078350	0.038304	0	0	1	1	0	Vb	1	1.05	0.95
134	1	0.145475	0.071121	0	0	1	1	0	Vb	1	1.05	0.95
135	1	0.021369	0.010447	0	0	1	1	0	Vb	1	1.05	0.95
136	1	0.074789	0.036564	0	0	1	1	0	Vb	1	1.05	0.95
137	1	0.227926	0.111431	0	0	1	1	0	Vb	1	1.05	0.95
138	1	0.035614	0.017411	0	0	1	1	0	Vb	1	1.05	0.95
139	1	0.249295	0.121877	0	0	1	1	0	Vb	1	1.05	0.95
140	1	0.316722	0.154842	0	0	1	1	0	Vb	1	1.05	0.95
141	1	0.333817	0.163199	0	0	1	1	0	Vb	1	1.05	0.95
142	1	0.249295	0.121877	0	0	1	1	0	Vb	1	1.05	0.95
143	1	0.000000	0.000000	0	0	1	1	0	Vb	1	1.05	0.95
];
%% generator data
%	bus	Pg	Qg	Qmax	Qmin	Vg	mBase	status	Pmax	Pmin	Pc1	Pc2	Qc1min	Qc1max	Qc2min	Qc2max	ramp_agc	ramp_10	ramp_30	ramp_q	apf
mpc.gen = [
	1	20	20	100	-100	1 1 1	100	0	0	0	0	0	0	0	0	0	0	0	0;
   18	20	20	100	-100	1 1 1	100	0	0	0	0	0	0	0	0	0	0	0	0;
   41	20	20	100	-100	1 1 1	100	0	0	0	0	0	0	0	0	0	0	0	0;
   66	20	20	100	-100	1 1 1	100	0	0	0	0	0	0	0	0	0	0	0	0;
   79	20	20	100	-100	1 1 1	100	0	0	0	0	0	0	0	0	0	0	0	0;
   90	20	20	100	-100	1 1 1	100	0	0	0	0	0	0	0	0	0	0	0	0;
  105	20	20	100	-100	1 1 1	100	0	0	0	0	0	0	0	0	0	0	0	0;
  128	20	20	100	-100	1 1 1	100	0	0	0	0	0	0	0	0	0	0	0	0;
];
%% branch data   
%                matriz DE- PARA valores reais das imped�ncias
% %	       fbus	        tbus	   r	        x	    b	rateA	rateB	rateC	ratio	angle	status	angmin	angmax
mpc.branch = [

            1            2      0.33205      0.76653 	0	   4	  4	       4	  0	      0	       1	-360     360
            2            3      0.00188      0.00433 	0	   4	  4	       4	  0	      0	       1	-360     360
            3            4      0.22324      0.51535 	0	   4	  4	       4	  0	      0	       1	-360     360
            4            5      0.09943      0.22953 	0	   4	  4	       4	  0	      0	       1	-360     360
            5            6      0.15571      0.35945 	0	   4	  4	       4	  0	      0	       1	-360     360
            6            7      0.16321      0.37677 	0	   4	  4	       4	  0	      0	       1	-360     360
            7            8      0.11444      0.26417 	0	   2	  2	       2	  0	      0	       1	-360     360
            7            9      0.05675      0.05666 	0	   2	  2	       2	  0	      0	       1	-360     360
            9           10      0.52124      0.27418 	0	   2	  2	       2	  0	      0	       1	-360     360
            9           11      0.10877      0.10860 	0	   2	  2	       2	  0	      0	       1	-360     360
           11           12      0.39803      0.20937 	0	   1	  1	       1	  0	      0	       1	-360     360
           11           13      0.91744      0.31469 	0	   1	  1	       1	  0	      0	       1	-360     360
           11           14      0.11823      0.11805 	0	   1	  1	       1	  0	      0	       1	-360     360
           14           15      0.50228      0.26421 	0	   1	  1	       1	  0	      0	       1	-360     360
           14           16      0.05675      0.05666 	0	   2	  2	       2	  0	      0	       1	-360     360
           16           17      0.29379      0.15454 	0	   1	  1	       1	  0	      0	       1	-360     360
           %%% Alm 2 conc
           18           19      0.33205      0.76653 	0	   4	  4	       4	  0	      0	       1	-360     360
           19           20      0.00188      0.00433 	0	   4	  4	       4	  0	      0	       1	-360     360
           20           21      0.22324      0.51535 	0	   4	  4	       4	  0	      0	       1	-360     360
           21           22      0.10881      0.25118 	0	   4	  4	       4	  0	      0	       1	-360     360
           22           23      0.71078      0.37388 	0	   1	  1	       1	  0	      0	       1	-360     360
           22           24      0.18197      0.42008 	0	   4	  4	       4	  0	      0	       1	-360     360
           24           25      0.30326      0.15952 	0	   1	  1	       1	  0	      0	       1	-360     360
           24           26      0.02439      0.05630 	0	   4	  4	       4	  0	      0	       1	-360     360
           26           27      0.04502      0.10394 	0	   3	  3	       3	  0	      0	       1	-360     360
           27           28      0.01876      0.04331 	0	   2	  2	       2	  0	      0	       1	-360     360
           28           29      0.11823      0.11805 	0	   2	  2	       2	  0	      0	       1	-360     360
           29           30      0.02365      0.02361 	0	   2	  2	       2	  0	      0	       1	-360     360
           30           31      0.18954      0.09970 	0	   1	  1	       1	  0	      0	       1	-360     360
           31           32      0.39803      0.20937 	0	   1	  1	       1	  0	      0	       1	-360     360
           30           33      0.05675      0.05666 	0	   2	  2	       2	  0	      0	       1	-360     360
           33           34      0.09477      0.04985 	0	   2	  2	       2	  0	      0	       1	-360     360
           34           35      0.41699      0.21934 	0	   1	  1	       1	  0	      0	       1	-360     360
           35           36      0.11372      0.05982 	0	   1	  1	       1	  0	      0	       1	-360     360
           33           37      0.07566      0.07555 	0	   2	  2	       2	  0	      0	       1	-360     360
           37           38      0.36960      0.19442 	0	   1	  1	       1	  0	      0	       1	-360     360
           38           39      0.26536      0.13958 	0	   1	  1	       1	  0	      0	       1	-360     360
           37           40      0.05675      0.05666 	0	   2	  2	       2	  0	      0	       1	-360     360
           
           %%%%% Alm 3 conc
           41           42      0.33205      0.76653 	0	   4	  4	       4	  0	      0	       1	-360     360
           42           43      0.11819      0.27283 	0	   4	  4	       4	  0	      0	       1	-360     360
           43           44      2.96288      1.01628 	0	   1	  1	       1	  0	      0	       1	-360     360
           43           45      0.00188      0.00433 	0	   4	  4	       4	  0	      0	       1	-360     360
           45           46      0.06941      0.16024 	0	   4	  4	       4	  0	      0	       1	-360     360
           46           47      0.81502      0.42872 	0	   1	  1	       1	  0	      0	       1	-360     360
           46           48      0.06378      0.14724 	0	   4	  4	       4	  0	      0	       1	-360     360
           48           49      0.13132      0.30315 	0	   4	  4	       4	  0	      0	       1	-360     360
           49           50      0.06191      0.14291 	0	   4	  4	       4	  0	      0	       1	-360     360
           50           51      0.11444      0.26417 	0	   3	  3	       3	  0	      0	       1	-360     360
           51           52      0.28374      0.28331 	0	   1	  1	       1	  0	      0	       1	-360     360
           52           53      0.28374      0.28331 	0	   1	  1	       1	  0	      0	       1	-360     360
           51           54      0.04502      0.10394 	0	   3	  3	       3	  0	      0	       1	-360     360
           54           55      0.02626      0.06063 	0	   3	  3	       3	  0	      0	       1	-360     360
           55           56      0.06003      0.13858 	0	   3	  3	       3	  0	      0	       1	-360     360
           56           57      0.03002      0.06929 	0	   3	  3	       3	  0	      0	       1	-360     360
           57           58      0.02064      0.04764 	0	   3	  3	       3	  0	      0	       1	-360     360
           55           59      0.10881      0.25118 	0	   2	  2	       2	  0	      0	       1	-360     360
           59           60      0.25588      0.13460 	0	   2	  2	       2	  0	      0	       1	-360     360
           60           61      0.41699      0.21934 	0	   2	  2	       2	  0	      0	       1	-360     360
           61           62      0.50228      0.26421 	0	   1	  1	       1	  0	      0	       1	-360     360
           62           63      0.33170      0.17448 	0	   1	  1	       1	  0	      0	       1	-360     360
           63           64      0.20849      0.10967 	0	   1	  1	       1	  0	      0	       1	-360     360
           50           65      0.13882      0.32047 	0	   2	  2	       2	  0	      0	       1	-360     360
           
           %%% Alm 4 conc
           66           67      0.00750      0.01732 	0	   3	  3	       3	  0	      0	       1	-360     360
           67           68      0.27014      0.62362 	0	   3	  3	       3	  0	      0	       1	-360     360
           68           69      0.38270      0.88346 	0	   3	  3	       3	  0	      0	       1	-360     360
           69           70      0.33018      0.76220 	0	   3	  3	       3	  0	      0	       1	-360     360
           70           71      0.32830      0.75787 	0	   2	  2	       2	  0	      0	       1	-360     360
           71           72      0.17072      0.39409 	0	   2	  2	       2	  0	      0	       1	-360     360
           72           73      0.55914      0.29412 	0	   1	  1	       1	  0	      0	       1	-360     360
           72           74      0.05816      0.13425 	0	   2	  2	       2	  0	      0	       1	-360     360
           74           75      0.70130      0.36890 	0	   1	  1	       1	  0	      0	       1	-360     360
           75           76      1.02352      0.53839 	0	   1	  1	       1	  0	      0	       1	-360     360
           74           77      0.06754      0.15591 	0	   2	  2	       2	  0	      0	       1	-360     360
           77           78      1.32352      0.45397 	0	   1	  1	       1	  0	      0	       1	-360     360
           
           %%%% Alm 5 concl
           79           80      0.01126      0.02598 	0	   3	  3	       3	  0	      0	       1	-360     360
           80           81      0.72976      1.68464 	0	   3	  3	       3	  0	      0	       1	-360     360
           81           82      0.22512      0.51968 	0	   2	  2	       2	  0	      0	       1	-360     360
           82           83      0.20824      0.48071 	0	   2	  2	       2	  0	      0	       1	-360     360
           83           84      0.04690      0.10827 	0	   2	  2	       2	  0	      0	       1	-360     360
           84           85      0.61950      0.61857 	0	   2	  2	       2	  0	      0	       1	-360     360
           85           86      0.34049      0.33998 	0	   2	  2	       2	  0	      0	       1	-360     360
           86           87      0.56862      0.29911 	0	   1	  1	       1	  0	      0	       1	-360     360
           86           88      0.10877      0.10860 	0	   2	  2	       2	  0	      0	       1	-360     360
           88           89      0.56862      0.29911 	0	   2	  2	       2	  0	      0	       1	-360     360
           
           %%%%% Alm 6 conc
           90           91      0.01126      0.02598 	0	   5	  5	       5	  0	      0	       1	-360     360
           91           92      0.41835      0.96575 	0	   5	  5	       5	  0	      0	       1	-360     360
           92           93      0.10499      0.13641 	0	   1	  1	       1	  0	      0	       1	-360     360
           92           94      0.43898      1.01338 	0	   5	  5	       5	  0	      0	       1	-360     360
           94           95      0.07520      0.02579 	0	   5	  5	       5	  0	      0	       1	-360     360
           95           96      0.07692      0.17756 	0	   5	  5	       5	  0	      0	       1	-360     360
           96           97      0.33205      0.76653 	0	   3	  3	       3	  0	      0	       1	-360     360
           97           98      0.08442      0.19488 	0	   3	  3	       3	  0	      0	       1	-360     360
           98           99      0.13320      0.30748 	0	   3	  3	       3	  0	      0	       1	-360     360
           99          100      0.29320      0.29276 	0	   1	  1	       1	  0	      0	       1	-360     360
          100          101      0.21753      0.21721 	0	   1	  1	       1	  0	      0	       1	-360     360
          101          102      0.26482      0.26443 	0	   1	  1	       1	  0	      0	       1	-360     360
           99          103      0.10318      0.23819 	0	   3	  3	       3	  0	      0	       1	-360     360
          103          104      0.13507      0.31181 	0	   3	  3	       3	  0	      0	       1	-360     360
           
           %%% Alm 7 concl
          105          106      0.00938      0.02165 	0	   5	  5	       5	  0	      0	       1	-360     360
          106          107      0.16884      0.38976 	0	   5	  5	       5	  0	      0	       1	-360     360
          107          108      0.11819      0.27283 	0	   5	  5	       5	  0	      0	       1	-360     360
          108          109      2.28608      0.78414 	0	   1	  1	       1	  0	      0	       1	-360     360
          108          110      0.45587      1.05236 	0	   5	  5	       5	  0	      0	       1	-360     360
          110          111      0.69600      1.60669 	0	   4	  4	       4	  0	      0	       1	-360     360
          111          112      0.45774      1.05669 	0	   3	  3	       3	  0	      0	       1	-360     360
          112          113      0.20298      0.26373 	0	   3	  3	       3	  0	      0	       0	-360     360
          113          114      0.21348      0.27737 	0	   3	  3	       3	  0	      0	       1	-360     360
          114          115      0.54967      0.28914 	0	   2	  2	       2	  0	      0	       1	-360     360
          115          116      0.54019      0.28415 	0	   1	  1	       1	  0	      0	       1	-360     360
          114          117      0.04550      0.05911 	0	   2	  2	       2	  0	      0	       1	-360     360
          117          118      0.47385      0.24926 	0	   1	  1	       1	  0	      0	       1	-360     360
          118          119      0.86241      0.45364 	0	   1	  1	       1	  0	      0	       1	-360     360
          119          120      0.56862      0.29911 	0	   1	  1	       1	  0	      0	       1	-360     360
          115          121      0.77711      0.40878 	0	   1	  1	       1	  0	      0	       1	-360     360
          121          122      1.08038      0.56830 	0	   1	  1	       1	  0	      0	       1	-360     360
          116          123      1.09933      0.57827 	0	   1	  1	       1	  0	      0	       1	-360     360
          123          124      0.47385      0.24926 	0	   1	  1	       1	  0	      0	       1	-360     360
          111          125      0.32267      0.74488 	0	   3	  3	       3	  0	      0	       1	-360     360
          125          126      0.14633      0.33779 	0	   3	  3	       3	  0	      0	       1	-360     360
          126          127      0.12382      0.28583 	0	   3	  3	       3	  0	      0	       1	-360     360
          
          %%% Alm 8 conc
          128          129      0.01126      0.02598 	0	   4	  4	       4	  0	      0	       1	-360     360
          129          130      0.64910      1.49842 	0	   4	  4	       4	  0	      0	       1	-360     360
          130          131      0.04502      0.10394 	0	   4	  4	       4	  0	      0	       1	-360     360
          131          132      0.52640      0.18056 	0	   1	  1	       1	  0	      0	       1	-360     360
          131          133      0.02064      0.04764 	0	   4	  4	       4	  0	      0	       1	-360     360
          133          134      0.53071      0.27917 	0	   2	  2	       2	  0	      0	       1	-360     360
          133          135      0.09755      0.22520 	0	   4	  4	       4	  0	      0	       1	-360     360
          135          136      0.11819      0.27283 	0	   2	  2	       2	  0	      0	       1	-360     360
          135          137      0.13882      0.32047 	0	   3	  3	       3	  0	      0	       1	-360     360
          137          138      0.04315      0.09961 	0	   2	  2	       2	  0	      0	       1	-360     360
          138          139      0.09192      0.21220 	0	   2	  2	       2	  0	      0	       1	-360     360
          139          140      0.16134      0.37244 	0	   2	  2	       2	  0	      0	       1	-360     360
          140          141      0.37832      0.37775 	0	   2	  2	       2	  0	      0	       1	-360     360
          141          142      0.39724      0.39664 	0	   2	  2	       2	  0	      0	       1	-360     360
          142          143      0.29320      0.29276 	0	   2	  2	       2	  0	      0	       0	-360     360
          
          % Chaves NA
          
            8           77      0.13132      0.30315 	0	   2	  2	       2	  0	      0	       0	-360     360
           10           26      0.26536      0.13958 	0	   2	  2	       2	  0	      0	       0	-360     360
           16           88      0.14187      0.14166 	0	   2	  2	       2	  0	      0	       0	-360     360
           40          143      0.08512      0.08499 	0	   2	  2	       2	  0	      0	       1	-360     360
           27           54      0.04502      0.10394 	0	   2	  2	       2	  0	      0	       0	-360     360
           53          102      0.14187      0.14166 	0	   2	  2	       2	  0	      0	       0	-360     360
           58          104      0.14187      0.14166 	0	   2	  2	       2	  0	      0	       0	-360     360
           65          127      0.03940      0.09094 	0	   2	  2	       2	  0	      0	       0	-360     360
           70           84      0.12944      0.29882 	0	   2	  2	       2	  0	      0	       0	-360     360
           84          139      0.01688      0.03898 	0	   2	  2	       2	  0	      0	       0	-360     360
           89          143      0.33170      0.17448 	0	   2	  2	       2	  0	      0	       0	-360     360
           97          111      0.14187      0.14166 	0	   2	  2	       2	  0	      0	       0	-360     360
           96          137      0.07692      0.17756 	0	   2	  2	       2	  0	      0	       0	-360     360
           96          110      0.07692      0.17756 	0	   2	  2	       2	  0	      0	       0	-360     360
           98          111      0.07692      0.17756 	0	   2	  2	       2	  0	      0	       0	-360     360
           98          140      0.07692      0.17756 	0	   2	  2	       2	  0	      0	       0	-360     360
          102          127      0.26482      0.26443 	0	   2	  2	       2	  0	      0	       0	-360     360
          117           50      0.49696      0.64567 	0	   2	  2	       2	  0	      0	       1	-360     360
          134           81      0.17059      0.08973 	0	   2	  2	       2	  0	      0	       0	-360     360
          136           82      0.05253      0.12126 	0	   2	  2	       2	  0	      0	       0	-360     360
          143          104      0.29320      0.29276 	0	   2	  2	       2	  0	      0	       0	-360     360
];
                       

mpc.branch(:,3:4) = mpc.branch(:,3:4)*((mpc.baseMVA)/(Vb.^2));
