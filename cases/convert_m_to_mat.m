function q = convert_m_to_mat(name, dir)
	a = loadcase(name);
	savecase(strcat(dir, '/', name(1:length(name)-2), '.mat'), a);
	exit;
end %func
