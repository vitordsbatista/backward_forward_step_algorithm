function mpc = node_84new
% These data are collected from:
% C. Su and C. Lee, "network reconfiguration of distribution systems
% using improved mixed integer hybrid differential evolution," IEEE Trans. Power Del., vol. 18, no. 3, July 2003
%% MATPOWER Case Format : Version 2
mpc.version = '2';
%%-----  Power Flow Data  -----%%% VERIFICADO E APTO A SER ESCOLHIDO
%% system MVA base
mpc.baseMVA = 1;% BAse verificada para Pot�ncias e Imped�ncias
%% system kV base
Vb = 11.4;% BAse verificada para Tens�o
%%
%% bus data
%	bus_i	type	Pd	 Qd	 Gs	Bs	area	Vm	Va	baseKV	zone	Vmax	Vmin
mpc.bus = [
      1      3	   0	 0	   0	 0	  1    	 1	0	 Vb	     1	     1.05	0.95
      2	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
      3	     1	   0.10	 0.05  0	 0	  1	     1	0	 Vb  	 1	     1.05	0.95
      4	     1	   0.30	 0.20  0	 0	  1	     1	0	 Vb 	 1	     1.05	0.95
      5	     1	   0.35	 0.25  0	 0	  1	     1	0	 Vb 	 1	     1.05	0.95
      6      1	   0.22	 0.10  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
      7	     1	   1.10	 0.80  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
      8	     1	   0.40	 0.32  0	 0	  1    	 1	0	 Vb	     1	     1.05	0.95
      9	     1	   0.30	 0.20  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     10	     1	   0.30	 0.23  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     11	     1	   0.30	 0.26  0	 0    1	     1	0	 Vb	     1	     1.05	0.95
     
     12	     3	   0	 0	   0	 0	  1      1	0	 Vb	     1	     1.05	0.95
     13	     1	   0	 0	   0	 0    1	     1	0	 Vb	     1	     1.05	0.95
     14	     1	   1.20	 0.80  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     15	     1	   0.80	 0.60  0 	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     16	     1	   0.70	 0.50  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     
     17	     3	   0	 0     0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     18	     1	   0	 0     0	 0	  1      1	0	 Vb	     1	     1.05	0.95
     19	     1	   0.30	 0.15  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     20	     1	   0.50	 0.35  0	 0	  1      1	0	 Vb	     1	     1.05	0.95
     21	     1	   0.70	 0.40  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     22	     1	   1.20	 1.00  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     23	     1	   0.30	 0.30  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     24	     1	   0.40	 0.35  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     25	     1	   0.05	 0.02  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     26	     1	   0.05	 0.02  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     27	     1	   0.05	 0.01  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     
     28	     3	   0	 0     0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     29	     1	   0.05	 0.03  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     30	     1	   0.10	 0.06  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     31	     1	   0.10	 0.07  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     32	     1	   1.80	 1.30  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     33	     1	   0.20	 0.12  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     
     34	     3	   0	 0     0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     35	     1	   0	 0     0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     36	     1	   1.80	 1.60  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     37	     1	   0.20	 0.15  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     38	     1	   0.20	 0.10  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     39	     1	   0.80	 0.60  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     40	     1	   0.10	 0.06  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     41	     1	   0.10	 0.06  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     42	     1	   0.02	 0.01  0	 0	  1 	 1	0	 Vb	     1	     1.05	0.95
     43	     1	   0.02	 0.01  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     44	     1	   0.02	 0.01  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     45	     1	   0.02	 0.01  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     46	     1	   0.20	 0.16  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     47	     1	   0.05	 0.03  0	 0	  1      1	0	 Vb	     1	     1.05	0.95
     
     48	     3     0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     49	     1     0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     50	     1	   0.03	 0.02  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     51	     1	   0.80	 0.70  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     52	     1	   0.20	 0.15  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     
     53	     3	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     54	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     55	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     56	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     57	     1	   0.20	 0.16  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     58	     1	   0.80	 0.60  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     59	     1	   0.50	 0.30  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     60	     1	   0.50	 0.35  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     61	     1	   0.50	 0.30  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     62	     1	   0.20	 0.08  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     
     63	     3	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     64	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     65	     1	   0.03	 0.02  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     66	     1	   0.60	 0.42  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     67	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     68	     1	   0.02	 0.01  0	 0	  1 	 1	0	 Vb	     1	     1.05	0.95
     69	     1	   0.02	 0.01  0	 0	  1	     1	0    Vb	     1	     1.05	0.95
     70	     1	   0.20	 0.13  0	 0	  1	     1	0    Vb	     1	     1.05	0.95
     71	     1	   0.30	 0.24  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     72	     1	   0.30	 0.20  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     
     73	     3	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     74	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     75	     1	   0.05	 0.03  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     76	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     77	     1	   0.40	 0.36  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     78	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     79	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     80	     1	   2	 1.50  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     81	     1	   0.20	 0.15  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     
     82	     3	   0	 0	   0 	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     83	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     84	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     85	     1	   1.2	 0.95  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     86	     1	   0.3	 0.18  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     
     
     87	     3	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     88	     1	   0	 0	   0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     89	     1	   0.40	 0.36  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     90	     1	   2.00	 1.30  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     91	     1	   0.20	 0.14  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     92	     1	   0.50	 0.36  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
     93	     1	   0.10	 0.03  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95    
     94	     1	   0.40	 0.36  0	 0	  1	     1	0	 Vb	     1	     1.05	0.95
];

%% generator data
%	bus	Pg	Qg	Qmax	Qmin	Vg	mBase	status	Pmax	Pmin	Pc1	Pc2	Qc1min	Qc1max	Qc2min	Qc2max	ramp_agc	ramp_10	ramp_30	ramp_q	apf
mpc.gen = [
	 1	0	 0	 0 	      0	    1     0       1	     0	      0	     0	 0	  0	       0	   0	  0	       0	       0	   0	  0 	0;
    12	0	 0	 0 	      0	    1     0       1	     0	      0	     0	 0	  0	       0	   0	  0	       0	       0	   0	  0 	0;
    17	0	 0	 0 	      0	    1     0       1	     0	      0	     0	 0	  0	       0	   0	  0	       0	       0	   0	  0 	0;
    28	0	 0	 0 	      0	    1     0       1	     0	      0	     0	 0	  0	       0	   0	  0	       0	       0	   0	  0 	0;
	34	0	 0	 0 	      0	    1     0       1	     0	      0	     0	 0	  0	       0	   0	  0	       0	       0	   0	  0 	0;
    48	0	 0	 0 	      0	    1     0       1	     0	      0	     0	 0	  0	       0	   0	  0	       0	       0	   0	  0 	0;
    53	0	 0	 0 	      0	    1     0       1	     0	      0	     0	 0	  0	       0	   0	  0	       0	       0	   0	  0 	0;
    63	0	 0	 0 	      0	    1     0       1	     0	      0	     0	 0	  0	       0	   0	  0	       0	       0	   0	  0 	0;
    73	0	 0	 0 	      0	    1     0       1	     0	      0	     0	 0	  0	       0	   0	  0	       0	       0	   0	  0 	0;
    82	0	 0	 0 	      0	    1     0       1	     0	      0	     0	 0	  0	       0	   0	  0	       0	       0	   0	  0 	0;
    87	0	 0	 0 	      0	    1     0       1	     0	      0	     0	 0	  0	       0	   0	  0	       0	       0	   0	  0 	0;
];

%% branch data
% fbus	tbus 	   r              x	       b   rateA   rateB  rateC	 ratio	angle	status	angmin	angmax
% mpc.branch = [
%    1	 2	  0.001495845	0.005096953	   0	100	    100	   100 	  0	      0	       1	 -360	360
%    2	 3    0.001612804	0.003311788	   0	100	    100	   100	  0	      0	       1	 -360	360
%    3	 4	  0.001814404	0.003725762	   0 	100	    100	   100	  0	      0	       1	 -360	360
%    4	 5	  0.000705602	0.001448907	   0	100	    100    100    0	      0	       1	 -360	360
%    5	 6	  0.001612804	0.003311788	   0	100	    100	   100	  0	      0	       1	 -360	360
%    6	 7	  0.000302401	0.00062096	   0	100	    100	   100	  0	      0	       1	 -360	360
%    7	 8	  0.000311634	0.001061865	   0	100	    100	   100	  0	      0	       1	 -360	360
%    8	 9	  0.000806402	0.001655894	   0	100	    100	   100	  0	      0	       1	 -360	360
%    8	10	  0.001814404	0.003725762	   0	100	    100	   100	  0	      0	       1	 -360	360
%    8	11	  0.000806402	0.001655894	   0	100	    100	   100	  0	      0	       1  	 -360	360
%    1	12	  0.000604801	0.001241921	   0	100	    100	   100	  0	      0	       1	 -360	360
%   12	13	  0.002620806	0.005343183	   0	100	    100	   100	  0 	  0 	   1	 -360	360
%   13	14	  0.0002016	    0.000413974	   0	100	    100	   100	  0	      0	       1 	 -360	360
%   13	15	  0.000604801	0.001241921	   0	100	    100	   100	  0	      0	       1	 -360	360
%    1	16	  0.000872576	0.002973223	   0	100	    100	   100	  0	      0	       1	 -360	360
%   16	17	  0.000403201	0.000827947	   0	100	    100	   100	  0	      0	       1	 -360	360
%   17	18	  0.000403201	0.000827947	   0	100	    100	   100	  0	      0	       1	 -360	360
%   18	19	  0.001209603	0.002483841	   0	100	    100	   100	  0	      0	       1	 -360	360
%   19	20	  0.000302401	0.00062096	   0	100	    100	   100	  0	      0	       1	 -360	360
%   20	21	  0.001310403	0.002690828	   0	100	    100	   100	  0	      0	       1	 -360	360
%   21	22	  0.001814404	0.003725762	   0	100	    100	   100	  0	      0	       1	 -360	360
%   22	23	  0.001209603	0.002483841	   0	100	    100	   100	  0	      0	       1	 -360	360
%   22	24	  0.001512004	0.003104801	   0	100	    100	   100	  0	      0	       1	 -360	360
%   24	25	  0.001008002	0.002069868	   0	100	    100	   100	  0	      0	       1	 -360	360
%    1	26	  0.000436288	0.001486611	   0	100 	100	   100	  0	      0	       1	 -360	360
%   26	27	  0.000806402	0.001655894	   0	100	    100	   100	  0	      0	       1	 -360	360
%   27	28	  0.001915205	0.003932749	   0	100 	100	   100	  0	      0	       1	 -360	360
%   28	29	  0.000373961	0.001274238	   0	100 	100	   100	  0	      0	       1	 -360	360
%   29	30	  0.001008002	0.002069868	   0	100	    100	   100	  0	      0	       1	 -360	360
%    1	31	  0.001512004	0.003047091	   0	100	    100	   100	  0	      0	       1	 -360	360
%   31	32	  0.001008002	0.002069868	   0	100	    100	   100	  0	      0	       1	 -360	360
%   32	33	  0.001008002	0.002069868	   0	100	    100	   100	  0	      0	       1	 -360	360
%   33	34	  0.0002016	    0.000413974	   0	100	    100	   100	  0	      0	       1	 -360	360
%   34	35	  0.001310403	0.002690828	   0	100	    100	   100	  0	      0	       1	 -360	360
%   35	36	  0.000403201	0.000827947	   0	100	    100	   100	  0 	  0	       1	 -360	360
%   36	37	  0.003830409	0.007865497	   0	100	    100	   100	  0	      0	       1  	 -360	360
%   37	38	  0.000302401	0.00062096	   0	100	    100	   100	  0	      0	       1  	 -360	360
%   38	39	  0.000302401	0.00062096	   0	100	    100	   100	  0	      0	       1	 -360	360
%   39	40	  0.000604801	0.001241921	   0	100	    100	   100	  0	      0	       1	 -360	360
%   40	41	  0.001612804	0.003311788	   0	100	    100	   100	  0	      0	       1	 -360	360
%   39	42	  0.001512004	0.003104801	   0	100	    100	   100	  0	      0	       1	 -360	360
%   42	43	  0.001612804	0.003311788	   0	100	    100	   100	  0	      0	       1	 -360	360
%    1	44	  0.000373961	0.001274238	   0	100	    100	   100	  0	      0	       1	 -360	360
%   44	45	  0.000302401	0.00062096	   0	100	    100	   100	  0	      0	       1	 -360	360
%   45	46	  0.001008002	0.002069868	   0	100	    100	   100	  0	      0	       1	 -360	360
%   46	47	  0.001814404	0.003725762	   0	100	    100	   100	  0	      0	       1	 -360	360
%    1	48	  0.001869806	0.006371191	   0	100	    100	   100	  0	      0	       1	 -360	360
%   48	49	  0.000504001	0.001034934	   0	100 	100	   100	  0	      0	       1	 -360	360
%   49	50	  0.000504001	0.001034934	   0	100	    100	   100	  0	      0	       1	 -360	360
%   50	51	  0.000302401	0.00062096	   0	100	    100	   100	  0 	  0	       1	 -360	360
%   51	52	  0.000604801	0.001241921	   0	100	    100	   100	  0	      0	       1	 -360	360
%   52	53	  0.000302401	0.00062096	   0 	100	    100	   100	  0	      0	       1	 -360	360
%   53	54	  0.000604801	0.001241921	   0	100	    100	   100	  0	      0	       1	 -360	360
%   54	55	  0.000403201	0.000827947	   0	100	    100	   100	  0	      0	       1     -360	360
%   55	56	  0.001008002	0.002069868	   0	100	    100	   100	  0	      0	       1	 -360	360
%    1	57	  0.001745152	0.005946445	   0	100	    100	   100	  0	      0	       1  	 -360	360
%   57	58	  0.00413281	0.008486457	   0	100	    100	   100	  0	      0	       1	 -360	360
%   58	59	  0.000403201	0.000827947	   0	100	    100	   100 	  0	      0	       1	 -360	360
%   59	60	  0.000311634	0.001061865	   0	100	    100	   100	  0	      0	       1	 -360	360
%   60	61	  0.000302401	0.00062096     0	100	    100	   100	  0	      0	       1	 -360	360
%   61	62	  0.000201600	0.000413974	   0	100	    100	   100	  0	      0	       1	 -360	360
%   62	63	  0.000806402	0.001655894	   0	100	    100	   100	  0	      0	       1	 -360	360
%   63	64	  0.001814404	0.003725762	   0	100	    100	   100	  0	      0	       1	 -360	360
%   64	65	  0.000186981	0.000637119	   0	100	    100	   100	  0	      0	       1	 -360	360
%    1	66	  0.000373961	0.001274238	   0	100	    100	   100	  0	      0	       1	 -360	360
%   66	67	  0.001310403	0.002690828	   0	100	    100	   100	  0	      0	       1	 -360	360
%   67	68	  0.000934903	0.003185596	   0	100	    100	   100	  0	      0	       1	 -360	360
%   68	69	  0.001682825	0.005734072	   0	100	    100	   100	  0 	  0	       1	 -360	360
%   69	70	  0.000373961	0.001274238	   0	100	    100	   100	  0	      0	       1 	 -360	360
%   70	71	  0.000560942	0.001911357	   0	100	    100	   100	  0	      0	       1	 -360	360
%   71	72	  0.000436288	0.001486611	   0	100	    100	   100 	  0	      0	       1	 -360	360
%   72	73	  0.0002016	    0.000406279    0	100	    100	   100	  0	      0	       1	 -360	360
%    1	74	  0.002493075	0.008494922	   0	100	    100	   100	  0	      0	       1	 -360	360
%   74	75	  0.000249307	0.000849492	   0	100	    100	   100	  0	      0	       1	 -360	360
%   75	76	  0.000436288	0.001486611	   0	100	    100	   100	  0	      0	       1	 -360	360
%   76	77	  0.000373961	0.001274238	   0	100	    100	   100	  0	      0	       1	 -360	360
%    1	78	  0.001932133	0.006583564	   0	100	    100	   100	  0	      0	       1	 -360	360
%   78	79	  0.00099723	0.003397969	   0	100	    100	   100	  0	      0	       1	 -360	360
%   79	80	  0.000373961	0.001274238	   0	100	    100	   100	  0	      0	       1	 -360	360
%   80	81	  0.001008002	0.002031394	   0	100	    100	   100	  0	      0	       1	 -360	360
%   81	82	  0.001008002	0.002031394	   0	100	    100	   100	  0	      0	       1	 -360	360
%   82	83	  0.000705602	0.001448907	   0	100	    100	   100	  0	      0	       1	 -360	360
%   83	84	  0.002419206	0.004967682	   0	100	    100	   100	  0	      0	       1	 -360	360
% % 
%    6	56	  0.001008002	0.002069868	   0	100	    100	   100	  0 	  0	       0	 -360	360
%    8	61	  0.001008002	0.002069868	   0	100	    100	   100	  0	      0	       0	 -360	360
%   12	44	  0.001008002	0.002069868	   0	100	    100	   100	  0	      0	       0	 -360	360
%   13	73	  0.002620806	0.005381656	   0	100	    100	   100	  0	      0	       0	 -360	360
%   14	77	  0.003528009	0.007244537	   0	100	    100	   100	  0	      0	       0	 -360	360
%   15	19	  0.00413281	0.008328717	   0	100	    100	   100	  0	      0	       0	 -360	360
%   17	27	  0.000705602	0.001448907	   0	100	    100	   100	  0	      0	       0	 -360	360
%   21	84	  0.000604801	0.001241921	   0	100	    100	   100	  0	      0	       0	 -360	360
%   29	33	  0.000403201	0.000827947	   0	100	    100	   100	  0	      0	       0	 -360	360
%   30	40	  0.000604801	0.001241921	   0	100	    100	   100	  0	      0	       0	 -360	360
%   35	47	  0.0002016	    0.000413974	   0	100	    100	   100	  0	      0	       0	 -360	360
%   41	43	  0.001512004	0.003104801	   0	100	    100	   100	  0	      0	       0	 -360	360
%   54	65	  0.000302401	0.000620960	   0	100	    100	   100	  0	      0	       0	 -360	360
% ];





% %% branch real data
% % fbus	tbus 	 r        x	     b   rateA   rateB  rateC	 ratio	angle	status	angmin	angmax
mpc.branch = [
     1    2   0.1944   0.6624    0	  007	  007	 007 	   0	  0	       1	 -360	 360
     2    3   0.2096   0.4304    0	  007	  007	 007 	   0	  0	       1	 -360	 360
     3    4   0.2358   0.4842    0	  006	  006	 006 	   0	  0	       1	 -360	 360
     4    5   0.0917   0.1883    0	  006	  006	 006 	   0	  0	       1	 -360	 360
     5    6   0.2096   0.4304    0	  006	  006	 006 	   0	  0	       1	 -360	 360
     6    7   0.0393   0.0807    0	  004	  004	 004 	   0	  0	       1	 -360	 360
     7    8   0.0405   0.1380    0	  004	  004	 004 	   0	  0	       1	 -360	 360
     8    9   0.1048   0.2152    0	  002	  002	 002 	   0	  0	       1	 -360	 360
     8   10   0.2358   0.4842    0	  002	  002	 002 	   0	  0	       1	 -360	 360
     8   11   0.1048   0.2152    0	  002	  002	 002 	   0	  0	       1	 -360	 360
     
    12   13   0.0786   0.1614    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    13   14   0.3406   0.6944    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    14   15   0.0262   0.0538    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    14   16   0.0786   0.1614    0	  004	  004	 004 	   0	  0	       1	 -360	 360
    
    17   18   0.1134   0.3864    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    18   19   0.0524   0.1076    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    19   20   0.0524   0.1076    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    20   21   0.1572   0.3228    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    21   22   0.0393   0.0807    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    22   23   0.1703   0.3497    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    23   24   0.2358   0.4842    0	  002	  002	 002 	   0	  0	       1	 -360	 360
    24   25   0.1572   0.3228    0	  002	  002	 002 	   0	  0	       1	 -360	 360
    24   26   0.1965   0.4035    0	  002	  002	 002 	   0	  0	       1	 -360	 360
    26   27   0.1310   0.2690    0	  002	  002	 002 	   0	  0	       1	 -360	 360
    
    28   29   0.0567   0.1932    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    29   30   0.1048   0.2152    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    30   31   0.2489   0.5111    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    31   32   0.0486   0.1656    0	  003	  003	 003 	   0	  0	       1	 -360	 360
    32   33   0.1310   0.2690    0	  002	  002	 002 	   0	  0	       1	 -360	 360
    
    34   35   0.1965   0.3960    0	  008	  008	 008 	   0	  0	       1	 -360	 360
    35   36   0.1310   0.2690    0	  008	  008	 008 	   0	  0	       1	 -360	 360
    36   37   0.1310   0.2690    0	  008	  008	 008 	   0	  0	       1	 -360	 360
    37   38   0.0262   0.0538    0	  008	  008	 008 	   0	  0	       1	 -360	 360
    38   39   0.1703   0.3497    0	  008	  008	 008 	   0	  0	       1	 -360	 360
    39   40   0.0524   0.1076    0	  008	  008	 008 	   0	  0	       1	 -360	 360
    40   41   0.4978   1.0222    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    41   42   0.0393   0.0807    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    42   43   0.0393   0.0807    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    43   44   0.0786   0.1614    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    44   45   0.2096   0.4304    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    43   46   0.1965   0.4035    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    46   47   0.2096   0.4304    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    
    48   49   0.0486   0.1656    0	  004	  004	 004 	   0	  0	       1	 -360	 360
    49   50   0.0393   0.0807    0	  004	  004	 004 	   0	  0	       1	 -360	 360
    50   51   0.1310   0.2690    0	  004	  004	 004 	   0	  0	       1	 -360	 360
    51   52   0.2358   0.4842    0	  002	  002	 002 	   0	  0	       1	 -360	 360
    
    53   54   0.2430   0.8280    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    54   55   0.0655   0.1345    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    55   56   0.0655   0.1345    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    56   57   0.0393   0.0807    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    57   58   0.0786   0.1614    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    58   59   0.0393   0.0807    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    59   60   0.0786   0.1614    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    60   61   0.0524   0.1076    0	  004	  004	 004 	   0	  0	       1	 -360	 360
    61   62   0.1310   0.2690    0	  004	  004	 004 	   0	  0	       1	 -360	 360
    
    63   64   0.2268   0.7728    0	  004	  004	 004 	   0	  0	       1	 -360	 360
    64   65   0.5371   1.1029    0	  004	  004	 004 	   0	  0	       1	 -360	 360
    65   66   0.0524   0.1076    0	  004	  004	 004 	   0	  0	       1	 -360	 360
    66   67   0.0405   0.1380    0	  004	  004	 004 	   0	  0	       1	 -360	 360
    67   68   0.0393   0.0807    0	  004	  004	 004 	   0	  0	       1	 -360	 360
    68   69   0.0262   0.0538    0	  003	  003	 003 	   0	  0	       1	 -360	 360
    69   70   0.1048   0.2152    0	  003	  003	 003 	   0	  0	       1	 -360	 360
    70   71   0.2358   0.4842    0	  003	  003	 003 	   0	  0	       1	 -360	 360
    71   72   0.0243   0.0828    0	  003	  003	 003 	   0	  0	       1	 -360	 360
    
    73   74   0.0486   0.1656    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    74   75   0.1703   0.3497    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    75   76   0.1215   0.4140    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    76   77   0.2187   0.7452    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    77   78   0.0486   0.1656    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    78   79   0.0729   0.2484    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    79   80   0.0567   0.1932    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    80   81   0.0262   0.0528    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    
    82   83   0.3240   1.1040    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    83   84   0.0324   0.1104    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    84   85   0.0567   0.1932    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    85   86   0.0486   0.1656    0	  006	  006	 006 	   0	  0	       1	 -360	 360
    
    87   88   0.2511   0.8556    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    88   89   0.1296   0.4416    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    89   90   0.0486   0.1656    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    90   91   0.1310   0.2640    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    91   92   0.1310   0.2640    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    92   93   0.0917   0.1883    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    93   94   0.3144   0.6456    0	  007	  007	 007 	   0	  0	       1	 -360	 360
    
    6    62   0.1310   0.2690    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    8    68   0.1310   0.2690    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    13   49   0.1310   0.2690    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    14   81   0.3406   0.6994    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    15   86   0.4585   0.9415    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    16   21   0.5371   1.0824    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    19   30   0.0917   0.1883    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    23   94   0.0786   0.1614    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    32   37   0.0524   0.1076    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    33   44   0.0786   0.1614    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    39   52   0.0262   0.0538    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    45   47   0.1965   0.4035    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    60   72   0.0393   0.0807    0	  006	  006	 006 	   0	  0	       0	 -360	 360
    
    ];
mpc.branch(:,3:4) = mpc.branch(:,3:4)*((mpc.baseMVA)/(Vb.^2));
