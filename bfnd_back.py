#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
PowerFlow with NDE
Vitor Batista
vitordsbatista@gmail.com
Python 2.7
"""

#import numpy as np
import os
import pickle as pk
import math
import csv
import sys
from scipy.io import loadmat
from itertools import compress
import copy
import timeit
from timeit import default_timer as timer

def dfs(graph, start): 
    """
        Realiza uma busca em profundidade num grafo

        Parâmetros:
            graph   -- dicionário com o grafo
            start   -- nó de início da busca
        Saída:
            visited -- nós visitados pela busca
    """
    visited, stack = [], [[start, 1]]
    while stack:
        vertex = stack.pop()
        if vertex not in visited:
            visited.append(vertex)
            if vertex[0] in graph.keys():
                tmp = set([i[0] for i in visited]).intersection(graph[vertex[0]]).symmetric_difference(graph[vertex[0]]),
                for i in tmp[0]:
                    stack.append([i, vertex[1]+1])
    visited = map(list, zip(*visited))              #Transposição da rnp
    return visited

def dp2rnp(case):
    """
        Gera a floresta com todas as rnp's da rede

        Parâmetros:
            case     -- dicionário com os dados da rede elétrica
        Saída:
            floresta -- rnps num dicionário
       
    """
    status = case['line']['status']
    dePara = list(compress(case['line']['from_to'], status))
    paraDe = [(j, i) for i, j in dePara]
    roots = case['gen']['bus']
    #Transforma o De-Para num grafo
    dp = dict()
    for i in dePara:     
        if dp.has_key(i[0]):
            dp[i[0]].append(i[1])
        else:
            dp[i[0]] = [i[1]]
    for i in paraDe:     
        if dp.has_key(i[0]):
            dp[i[0]].append(i[1])
        else:
            dp[i[0]] = [i[1]]
    floresta = dict()
    for n, i in enumerate(roots): #For com todos os nós raizes para gerar todas as rnps
        tmp = dfs(dp, i)
        floresta[str(n)] = tmp
    for i in floresta:
        floresta[i] = zip(*floresta[i])
    return floresta

def load_mpc(path, sub_file=None):
    """
        Lê um caso do matpower

        Parâmetros:
            path -- caminho para o caso do matpower (.mat)
        Saída:
            case -- dicionário com os dados da rede
       
    """
    if sub_file:
        bus_gen = []
        st_gen = []
        loading_gen = []
        #Carrega um arquivo com os dados das subestações
        with open(sub_file, 'r') as f:
            reader = csv.reader(f)
            for i in reader:
                bus_gen.append(int(i[0]))
                st_gen.append(float(i[1]))
                loading_gen.append(0)
    #Carrega o arquivo .mat
    mat = loadmat(path)
    #Potência base
    Sb = float(mat['baseMVA'][0][0])
    #Tensão base
    Vb = float(mat['mpc'][0][0][2][0][9])
    #Separa ele em dicionários
    bus = {
        'bus': [], 'type': [],
        'P': [], 'Q': [], 'S': [], 
        'G': [], 'B': [], 'Ysh': [],
        'Vm': [], 'Va': []
    }
    gen = {
        'bus': [],
        'st': [],
        'loading': []
    }
    line = {
        'from_to': [],
        'r': [], 'x': [], 'Z': [],
        'max_i': [], 'status': [],
        'p_loss': [], 'q_loss': [],
        'I': [], 'loading': []
    }
    #Barras
    for i in mat['mpc'][0][0][2]:
        bus['bus'].append(int(i[0]))
        bus['type'].append(int(i[1]))
        bus['P'].append(float(i[2]))
        bus['Q'].append(float(i[3]))
        bus['S'].append(complex(i[2], i[3]))
        bus['G'].append(float(i[4]))
        bus['B'].append(float(i[5]))
        bus['Ysh'].append(complex(i[4], i[5]))
        # Saída de dados do fluxo de carga
        bus['Vm'].append(1)
        bus['Va'].append(0)
    #Alimentadores
    if sub_file:
        gen['bus'] = bus_gen
        gen['st'] = st_gen
        gen['loading'] = loading_gen
    else:
        for i in mat['mpc'][0][0][3]:
            gen['bus'].append(int(i[0]))
            gen['st'].append(None)
            gen['loading'].append(None)
    #Linhas
    for i in mat['mpc'][0][0][4]:
        line['from_to'].append((int(i[0]), int(i[1])))
        line['r'].append(float(i[2]))
        line['x'].append(float(i[3]))
        line['Z'].append(complex(i[2], i[3]))
        line['max_i'].append(float(i[5])/(Vb*math.sqrt(3))*1000)
        line['status'].append(int(i[10]))
        # Saída de dados do fluxo de carga
        line['p_loss'].append(0)
        line['q_loss'].append(0)
        line['I'].append(0)
        line['loading'].append(0)

    Zb = Vb**2/Sb
    bus['P_kw'] = [i*Sb*1000 for i in bus['P']]
    bus['Q_kvar'] = [i*Sb*1000 for i in bus['Q']]
    line['r_ohm'] = [i*Zb for i in line['r']]
    line['x_ohm'] = [i*Zb for i in line['x']]

    #Adiciona num dict
    case = {'bus': bus, 'gen': gen, 'line': line, 'Sb': Sb, 'Vb': Vb}
    #Cria os resultados do fluxo de carga
    case['output'] = {
        'sum_p_loss': 0,
        'sum_q_loss': 0,
        'max_line_loading': 0,
        'max_sub_loading': 0,
        'min_voltage': [],
        'max_voltage': []
    }
    return case

def rnp2dp(rnp):
    """
        Transforma uma rnp num de-para

        Parâmetros:
            rnp     -- representação nó profundidade
        Saída:
            dePara  -- de-para
    """
    dePara = []
    #Inverte a RNP
    # REMOVE - rnp = [rnp[0][::-1], rnp[1][::-1]]          #Inverte a rnp
    rnp = rnp[::-1]
    # REMOVER - for n, (i, j) in enumerate(zip(*rnp)):
    for n, (i, j) in enumerate(rnp):
        # REMOVER - for m, (k, l) in enumerate(zip(*rnp)[n+1:]):
        for m, (k, l) in enumerate(rnp[n+1:]):
            if l == j-1:
                dePara.append((k, i))
                break
    return dePara

def run_pf(case, forest, verbose=0):
    """
        Executa o fluxo de carga para cada uma rnps

        Parâmetros:
            case    -- dicionário com os dados da rede elétrica
            forest     -- floresta com as rnps estruturadas
            verbose -- 1 para saída visual de dados
        Saída:
            case    -- dicionário com os dados da rede elétrica e
                        os resultados do fluxo de carga e dos dados
                        formatados
            
    """
    t0 = timer()
    bus_order = case['bus']['bus']
    line_order = case['line']['from_to']
    it = []
    for i in forest:
        case, tmp = bfsa(case, forest[i])
        it.append(tmp)
    tn = timer() - t0
    #====Saída de dados para o algoritmo====#
    #Perdas
    case['output']['sum_p_loss'] = sum(case['line']['p_loss'])
    case['output']['sum_q_loss'] = sum(case['line']['q_loss'])
    #Tensão máxima
    vol_max = max(case['bus']['Vm'])
    id_max = [n for n, i in enumerate(case['bus']['Vm']) if i == vol_max]   #Encontra as barras
    bus_max = [case['bus']['bus'][i] for i in id_max]
    case['output']['max_voltage'] = [bus_max, vol_max]
    #Tensão mínima
    vol_min = min(case['bus']['Vm'])
    id_min = [n for n, i in enumerate(case['bus']['Vm']) if i == vol_min]   #Encontra as barras
    bus_min = [case['bus']['bus'][i] for i in id_min]
    case['output']['min_voltage'] = [bus_min, vol_min]
    #Máximo carregamento das linhas
    # TODO: verificar pq o trecho abaixo está repetido
    loading = [i/j*100 if j else 0.0 for i, j in zip(case['line']['I'], case['line']['max_i'])]
    loading = []
    for i, j in zip(case['line']['from_to'], case['line']['I']):
        ix = case['line']['from_to'].index(i)
        max_i = case['line']['max_i'][ix]
        if max_i:
            loading.append(j/max_i)
        else:
            loading.append(0.)
    case['line']['loading'] = loading
    max_loading_line = max(loading)
    case['output']['max_line_loading'] = max_loading_line
    #Máximo carregamento das subestações
    #Calcula as cargas de cada alimentador
    loads = {}
    case['output']['total_loads'] = 0
    for i in forest:
        soma_p = 0.
        soma_q = 0.
        for j, _ in forest[i]:
            ix = case['bus']['bus'].index(j)
            soma_p += case['bus']['P'][ix]
            soma_q += case['bus']['Q'][ix]
        loads[forest[i][0][0]] = [soma_p, soma_q]
        case['output']['total_loads'] += complex(soma_p, soma_q)
    if all(case['gen']['st']):
        case['gen']['loading'] = []
        for i, st in zip(case['gen']['bus'], case['gen']['st']):
            s = complex(loads[i][0], loads[i][1])
            s = abs(s)
            case['gen']['loading'].append(s/st)
        case['output']['max_sub_loading'] = max(case['gen']['loading'])
        max_loading_sub = case['output']['max_sub_loading']
    else:
        max_loading_sub = -1
        case['output']['max_sub_loading'] = -1
    #Tempo de execução
    case['output']['time'] = tn
    #====Saída de dados para o usuário====#
    if verbose:
        #-Dados da barra
        print '  {:=^22}'.format('Bus Data')
        print '{:^9}{:^8}{:^8}'.format('Bus', 'Vm', 'Va')
        print '  {:->22}'.format('')
        for b, m, a in zip(case['bus']['bus'], case['bus']['Vm'], case['bus']['Va']):
            print '{:^9}{:^8.3f}{:^8.3f}'.format(b, m, a)
        #-Dados da linha
        print '\n  {:=^55}'.format("Line Data")
        print '{:^8}{:^6}{:^10}{:^9}{:^7}{:^9}{:^9}'.format('From', 'To', 'p_loss', 'q_loss', 'I', 'Max_I', 'Loading')
        print '  {:->55}'.format('')
        for ft, p, q, i, m, l in zip(case['line']['from_to'], case['line']['p_loss'], case['line']['q_loss'], case['line']['I'], case['line']['max_i'], case['line']['loading']):
            f, t = ft
            i = i.real
            print '{:^9}{:^5}{:^11.3f}{:^7.3f}{:^9.3f}{:^7.3f}{:^11.2f}'.format(f, t, p, q, i, m, l)
        #DADOS GERAIS
        print '\n  {:=^65}'.format("General Data")
        print '  {:^8}{:^6}{:^9}{:^9}{:^7}{:^14}{:^9}{:^9}'.format('Time', 'Iter', 'p_loss', 'q_loss', 'V_max', 'V_min', 'max_line', 'max_sub')
        print '  {:->65}'.format('')
        print '  {:^8.4f}{:^6.2f}{:^9.4f}{:^8.4f}{:^8.4f}{:^8.4f}@ {}{:^11.2f}{:^8.2f}'.format(
           tn, sum(it)/float(len(it)), case['output']['sum_p_loss'], case['output']['sum_q_loss'], 
           vol_max, vol_min, bus_min, max_loading_line, max_loading_sub
        )
    return case

def bfsa(case, rnp):
    """
        Fluxo de carga de varredura direta e inversa

        Parâmetros:
            case    -- dicionário com os dados da rede elétrica
            rnp     -- floresta com as rnps estruturadas
        Saída:
            case    -- dicionário com os dados da rede elétrica e
                        os resultados do fluxo de carga e dos dados
                        formatados
    """
    #Gera uma rnp invertida
    #rnp_rev = [rnp[0][::-1], rnp[1][::-1]]
    # REMOVER
    rnp_rev = rnp[::-1]

    #Leitura de dados
    # REMOVER - bus = rnp[0]                           #Barras da rnp
    bus = [x for x, _ in rnp]                          #Barras da rnp
    dp_all = case['line']['from_to']        #Todos as linhas possiveis
    dp = rnp2dp(rnp)                       #Transforma a rnp em dp
    for n, i in enumerate(dp):              #Validação do de-para
        if not i in dp_all:
            dp[n] = (i[1], i[0])
    #==============================================================#
    #Estrutura de dados
    #Será um dicionário, onde o índice é uma tupla (no caso de linhas), 
    #e os outros valores serão números complexos da rede
    #0 - V (Tensões)
    #1 - S (Potência)
    #2 - I (Corrente)
    #0 - Z (impedãncia)
    #1 - I_line (corrente na linha)
    #line_data = dict.fromkeys(dp2)
    bus_data = dict()#.fromkeys(bus)
    line_data = dict.fromkeys(dp)
    #==============================================================#
    # REMOVER - rnp_len = len(rnp[0])
    rnp_len = len(rnp)
    #Cálculo da tensão inicial
    tmp = complex(1, 0)
    busd = [case['bus']['bus'].index(i) for i in bus]
    for i, j in zip(bus, busd):
        bus_data[i] = [
            tmp, case['bus']['S'][j], 
            0, case['bus']['Ysh'][j]
        ]
    #Cálculo da impedância
    line_id = [case['line']['from_to'].index(i) for i in dp]
    for i, j in zip(dp, line_id):
        line_data[i] = [case['line']['Z'][j], 0]
    #==============================================================#
    #Loop
    v_err = 1
    #=========Passo 1=========#
    V_ant = dict()
    for i in bus_data:
        V = bus_data[i][0]
        Y = bus_data[i][3]
        S = bus_data[i][1]
        #S = bus_data[i][1] + Y.conjugate()*V**2
        bus_data[i][2] = (S/V).conjugate() + Y*V        #Calcula as correntes
        V_ant[i] = V
    max_it = 100
    it = 1
    while v_err > 0.00000001:
        """
        t0 = timer()
        """
        it += 1
        if it > max_it:
            raise Exception('Power Flow did not converge after %d iterations!' %max_it)
        #=========Passo 2=========#
        # REMOVER - for ix, (node, depth) in enumerate(zip(*rnp_rev)[:-1]):
        for ix, (node, depth) in enumerate(rnp_rev[:-1]):
            # REMOVER - for node_bef, depth_bef in zip(*rnp_rev)[ix+1:]:
            for node_bef, depth_bef in rnp_rev[ix+1:]:
                #Se for o nó pai
                if depth-1 == depth_bef:
                    #Percorre a rnp e identifica os nós que saem do node
                    adj_nodes = []
                    # REMOVER - for node_aft in zip(*rnp)[rnp_len-ix:]:
                    for node_aft in rnp[rnp_len-ix:]:
                        if node_aft[1] <= depth:
                            break
                        if node_aft[1] == depth+1:
                            adj_nodes.append(node_aft[0])
                    #Somatório das correntes que saem
                    sum_I_line = 0
                    #Soma as correntes deles
                    for adj in adj_nodes:
                        tup = (node, adj)
                        if not tup in dp:
                            tup = (adj, node)
                        sum_I_line += line_data[tup][1]
                    #Caso no de-para os nós estejam ao contrário
                    tup = (node_bef, node)
                    if not tup in dp:
                        tup = (node, node_bef)
                    #Cálculo da corrente de linha
                    line_data[tup][1] = bus_data[node][2] + sum_I_line
                    break
        """
        t = timer() - t0
        t2.append(t)
        print 'passo 2', t
        t0 = timer()
        """
        #=========Passo 3=========#
        #Obrigado a fazer a pesquisa direta
        # REMOVER - rs = len(rnp[0])    #Tamanho da rnp
        rs = len(rnp)    #Tamanho da rnp
        #for ix, (node, depth) in enumerate(zip(*rnp)[1:]):
        # REMOVER
        #for ix, (node, depth) in enumerate(rnp[1:]):
        #   for i in zip(*rnp_rev)[rs-ix-1:]:
        # REMOVER
        for ix, (node, depth) in enumerate(rnp):
            for i in rnp_rev[rs-ix-1:]:
                if depth == i[1]+1:
                    before_node = i[0]
                    tup = (before_node, node)
                    if not tup in dp:
                        tup = (node, before_node)
                    bus_data[node][0] = bus_data[before_node][0] - line_data[tup][0] * line_data[tup][1]
                    break
        """
        t = timer() - t0
        t3.append(t)
        print 'passo 3', t
        t0 = timer()
        """
        # ========= Passo 1 e erro ========= #
        p_err = []
        q_err = []
        vm_err = []
        va_err = []
        v_err_a = []
        for i in bus_data:
            #Passo 1
            V = bus_data[i][0]
            Y = bus_data[i][3]
            S = bus_data[i][1]
            I = (S/V).conjugate() + Y*V
            bus_data[i][2] = I      #Adiciona o I no bus_data
            #Erro
            V_err = V - V_ant[i]
            v_err_a.append(abs(V - V_ant[i]))
            vm_err.append(abs(V_err.real))
            va_err.append(abs(V_err.imag))
            V_ant[i] = V
        vm_err = max(vm_err)
        va_err = max(va_err)
        v_err = max(v_err_a)
    """
        t = timer() - t0
        t1.append(t)
        print 'passo 1 e erro', t
    tf = timer() - ti
    print '-----'
    print 'tempo total:', tf
    print 'Tempo total'
    print 'passo 1', sum(t1)
    print 'passo 2', sum(t2)
    print 'passo 3', sum(t3)
    print 'Percentual'
    print 'passo 1', sum(t1)/tf*100
    print 'passo 2', sum(t2)/tf*100
    print 'passo 3', sum(t3)/tf*100
    """
    # ===== Saída de dados ===== #
    root = rnp[0][0]
    bus = [[root, 1, 0]]
    res_branch = [[-1, -1, -1, -1]]
    #Barras
    # REMOVER - for i in rnp[0][1:]:
    for i, _ in rnp:
        V = bus_data[i][0]
        real = V.real
        imag = V.imag
        #Vm = np.hypot(real, imag)
        Vm = math.hypot(real, imag)
        #Va = np.arctan(imag/real)
        Va = math.atan(imag/real)
        #Va = np.rad2deg(Va)
        Va = math.degrees(Va)
        ix = case['bus']['bus'].index(i)
        case['bus']['Vm'][ix] = Vm
        case['bus']['Va'][ix] = Va
    #Linhas
    #Cálculo das perdas
    for n, i in enumerate(line_data):
        I = line_data[i][1]
        Z = line_data[i][0]
        r = Z.real
        x = Z.imag
        p_loss = r*abs(I)**2
        q_loss = x*abs(I)**2
        ix = case['line']['from_to'].index(i)
        case['line']['p_loss'][ix] = p_loss
        case['line']['q_loss'][ix] = q_loss
        # Corrente em amper (A)
        I = abs(I) * case['Sb']/(case['Vb']*math.sqrt(3)) * 1000
        case['line']['I'][ix] = I
    return case, it

def save_case(case, name):
    with open(name, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        #Dados das base (potência e tensão)
        writer.writerow(["@BASE"])
        writer.writerow(['S_base (MVA)', 'V_base (kVA)'])
        writer.writerow([case['Sb'], case['Vb']])
        #Dados das barras
        writer.writerow(["@BUS"])
        writer.writerow(['bus', 'type', 'P (kW)', 'Q (kVAr)', 'G', 'B', 
                            'V_mag', 'V_ang'])
        for i in zip(case['bus']['bus'],
                     case['bus']['type'],
                     case['bus']['P_kw'],
                     case['bus']['Q_kvar'],
                     case['bus']['G'],
                     case['bus']['B'],
                     case['bus']['Vm'],
                     case['bus']['Va']):
            writer.writerow(i)
        #Dados das linhas
        writer.writerow(["@LINE"])
        writer.writerow(['from', 'to', 'status', 'r (ohm)', 'x (ohm)', 'P_loss',
                         'Q_loss', 'I (A)', 'I_max (A)', 'loading'])
        for i in zip(case['line']['from_to'],
                     case['line']['status'],
                     case['line']['r_ohm'],
                     case['line']['x_ohm'],
                     case['line']['p_loss'],
                     case['line']['q_loss'],
                     case['line']['I'],
                     case['line']['max_i'],
                     case['line']['loading']):
            i = list(i)
            tmp = i.pop(0)
            writer.writerow(list(tmp) + i)
        #Dados do gerador
        writer.writerow(["@GEN"])
        writer.writerow(['bus', 'St', 'loading'])
        # Se o fluxo de carga foi executado
        if case['gen']['st']:
            for i in zip(case['gen']['bus'],
                         case['gen']['st'],
                         case['gen']['loading']):
                writer.writerow(i)
        else:
            for i in case['gen']['bus']:
                writer.writerow([i, None, None])
    return

#TODO: ler o caso a partir de um csv
def load_case(case_name):
    """ Carrega um caso do csv
        Parâmetros:
            path -- caminho para o caso do matpower (.mat)
        Saída:
            case -- dicionário com os dados da rede
    """
    with open(case_name, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        base = []
        bus = []
        line = []
        gen = []
        tmp = []
        for i in reader:
            if any(i):
                if i[0] == '@BASE':
                    pass
                elif i[0] == '@BUS':
                    #TODO: colocar a partir do 2
                    base = tmp[1:]
                    tmp = []
                elif i[0] == '@LINE':
                    bus = tmp[2:]
                    tmp = []
                elif i[0] == '@GEN':
                    line = tmp[1:]
                    tmp = []
                tmp.append(i)
        gen = tmp[1:]
    #Geração do dicionário
    #Barras
    bus = zip(*bus)
    bus = {
        'bus': list(
            map(int, bus[0])
        ),
        'type': list(
            map(int, bus[1])
        ),
        'P_kw': list(
            map(float, bus[2])
        ),
        'Q_kvar': list(
            map(float, bus[3])
        ),
        'G': list(
            map(float, bus[4])
        ),
        'B_kvar': list(
            map(float, bus[5])
        ),
        'Vm': list(
            map(float, bus[6])
        ),
        'Va': list(
            map(float, bus[7])
        ),
        #'S': list(bus[0]),
        #'Ysh': list(bus[0])
    }
    line = zip(*line[1:])
    line = {
        'from_to': list(
            zip(
                map(int, line[0]), 
                map(int, line[1])
            )
        ),
        'status': list(
            map(int, line[2])
        ),
        'r_ohm': list(
            map(float, line[3])
        ), 
        'x_ohm': list(
            map(float, line[4])
        ), 
        'p_loss': list(
            map(float, line[5])
        ), 
        'q_loss': list(
            map(float, line[6])
        ),
        'I': list(
            map(float, line[7])
        ), 
        'max_i': list(
            map(float, line[8])
        ), 
        'loading': list(
            map(float, line[9])
        )
        #'Z': [],
    }
    gen = zip(*gen[1:])
    if all(gen[1]):
        gen = {
            'bus': list(
                map(int, gen[0])
            ),
            'st': list(
                map(float, gen[1])
            ),
            'loading': list(
                map(float, gen[2])
            )
        }
    else:
        gen = {
            'bus': list(
                map(int, gen[0])
            ),
            'st': [None for _ in range(len(gen[1]))],
            'loading': [None for _ in range(len(gen[2]))]
        }
    Sb = float(base[1][0])
    Vb = float(base[1][1])

    # Transforma em p.u
    bus['P'] = [i/(Sb*1000) for i in bus['P_kw']]
    bus['Q'] = [i/(Sb*1000) for i in bus['Q_kvar']]
    line['r'] = [i*(Sb/Vb**2) for i in line['r_ohm']]
    line['x'] = [i*(Sb/Vb**2) for i in line['x_ohm']]
    bus['B'] = [i/(Sb*1000) for i in bus['B_kvar']]

    # Não transforma a corrente que já está em A
    #line['max_i'] = line['max_i_A']
    #line['max_i'] = [i*math.sqrt(3) for i in line['max_i_A']]
    #line['max_i'] = [i*(math.sqrt(3)*Vb/Sb) for i in line['max_i_A']]

    # Cálculo dos números complexos
    bus['S'] = [complex(i, j) for i, j in zip(bus['P'], bus['Q'])]
    bus['Ysh'] = [complex(i, j) for i, j in zip(bus['G'], bus['B'])]
    line['Z_ohm'] = [complex(i, j) for i, j in zip(line['r_ohm'], line['x_ohm'])]
    line['Z'] = [complex(i, j) for i, j in zip(line['r'], line['x'])]
    
    case = {
        'bus': bus,
        'line': line,
        'gen': gen,
        'Sb': Sb,
        'Vb': Vb
    }
    #Cria os resultados do fluxo de carga
    case['output'] = {
        'sum_p_loss': 0,                 
        'sum_q_loss': 0,                 
        'max_line_loading': 0,
        'max_sub_loading': 0,
        'min_voltage': [],
        'max_voltage': []
    }
    return case




"""
#TESTES
path = 'cases/case84new.mat'
case = load_mpc(path)
rnp = dp2rnp(case)
run_pf(case, rnp, verbose=1)
rnp['10'][0].pop()
rnp['10'][1].pop()
rnp['2'][0].append(94)
rnp['2'][1].append(8)
run_pf(case, rnp, verbose=1)
#TESTES
#"""
"""
Pickle
#import pickle as pk
#with open('test.p', 'r') as pfile:
#case = pk.load(pfile)

case = 'case84.mat'
path = 'cases/'+case
#subprocess.call("convert-cases ./cases/"+case)

#os.system("cd cases; convert-cases "+case[:-2])
case = load_mpc(path, 'pn_84_2.txt')
rnp = dp2rnp(case)
case = run_pf(case, rnp, verbose=0)
#save_case(case, 'case84.csv')

with open('case84.p', 'w') as pfile:
    pk.dump(case, pfile)

case = load_mpc('cases/case84.mat', 'pn_84_2.txt')
save_case(case, 'case84.csv')

with open('case84.p', 'w') as pfile:
    pk.dump(case, pfile)
"""

"""
case = load_mpc('cases/case84.mat', 'pn_84_2.txt')
print case['gen']
rnp = dp2rnp(case)
case = run_pf(case, rnp, verbose=0)
print case['output'].keys()
#print case.keys()
#print case['gen'].keys()
sys.exit()
case_name = 'case84.csv'
case = load_case(case_name)
case = load_mpc('cases/case84.mat', 'pn_84_2.txt')
with open('case84_limpo.p', 'w') as pfile:
    pk.dump(case, pfile)

#with open('case84.p', 'r') as pfile:
    #case = pk.load(pfile)

case = 'case69.mat'
os.system("cd cases; convert-cases "+case[:-2])
#print case.keys()
#print case['gen'].keys()
case = load_mpc3('cases/'+case)#, 'pn_84_2.txt')
with open('case33.p', 'w') as pfile:
    pk.dump(case, pfile)
#--------------------
case = 'case70new.mat'
os.system("cd cases; convert-cases "+case[:-2])
case = load_mpc('cases/'+case)#, 'pn_84_2.txt')
with open('case70new.p', 'w') as pfile:
    pk.dump(case, pfile)
for i, j, k in zip(*case2['gen'].values()):
    print i, j, k
#--------------------#
case2 = load_mpc('cases/case84.mat', 'pn_84.txt')
rnp = dp2rnp(case2)
save_case(case2, 'test.csv')
case2 = run_pf(case2, rnp)
print case2['output']

case_name = 'case119.csv'
case1 = load_case(case_name)
rnp = dp2rnp(case1) 
case1 = run_pf(case1, rnp)

for i, j, k in zip(case1['line']['from_to'], case1['line']['r'], case1['line']['x']):
    print i, j, k
print case1['output']

case2 = load_mpc('cases/case119.mat')
rnp = dp2rnp(case2) 
case2 = run_pf(case2, rnp)
print case2['output']

# TODO: Colocar os valores reais no arquivo .csv e converter para p.u 
# antes de rodar o fluxo de carga
"""
case2 = load_case('antigos/case16.csv')
rnp = dp2rnp(case2) 
case2 = run_pf(case2, rnp, verbose=1)

