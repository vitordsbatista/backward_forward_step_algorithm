#!/usr/bin/python
# -*- coding: utf-8 -*-
#===================================#
# File name: 						#
# Author: Vitor dos Santos Batista	#
# Date created: 					#
# Date last modified: 				#
# Python Version: 2.7				#
#===================================#

import numpy as np
import sys
from scipy.io import loadmat
from itertools import compress
import pandas as pd
import copy
import timeit
from timeit import default_timer as timer

def dfs(graph, start):
    visited, stack = [], [[start, 1]]
    while stack:
        vertex = stack.pop()
        if vertex not in visited:
            visited.append(vertex)
            if vertex[0] in graph.keys():
                tmp = set([i[0] for i in visited]).intersection(graph[vertex[0]]).symmetric_difference(graph[vertex[0]]),
                for i in tmp[0]:
                    stack.append([i, vertex[1]+1])
    visited = map(list, zip(*visited))              #Transposição da rnp
    return visited

def dp2rnp2(case):
    """
        Gera a floresta com todas as rnp's da rede

        Parâmetros:
            case     -- caso formatado
        Saída:
            floresta -- rnps num dicionário
       
    """
    status = case['line']['status']
    dePara = list(compress(case['line']['from_to'], status))
    paraDe = [(j, i) for i, j in dePara]
    roots = case['gen']['bus']
    #Transforma o De-Para num grafo
    dp = dict()
    for i in dePara:     
        if dp.has_key(i[0]):
            dp[i[0]].append(i[1])
        else:
            dp[i[0]] = [i[1]]
    for i in paraDe:     
        if dp.has_key(i[0]):
            dp[i[0]].append(i[1])
        else:
            dp[i[0]] = [i[1]]
    floresta = dict()
    for n, i in enumerate(roots): #For com todos os nós raizes para gerar todas as rnps
        tmp = dfs(dp, i)
        floresta[str(n)] = tmp
    return floresta

def load_mpc(path):
    """
        Lê um caso do matpower

        Parâmetros:
            path -- caminho para o caso do matpower (.mat)
        Saída:
            case -- caso formatado
       
    """
    #Carrega o arquivo .mat
    mat = loadmat(path)
    #Potência base
    Sb = mat['baseMVA'][0][0]
    #Tensão base
    Vb = mat['mpc'][0][0][2][0][9]
    #Separa ele em dicionários
    bus = {
        'bus_i': [], 'type': [],
        'P': [], 'Q': [], 'S': [], 
        'G': [], 'B': [], 'Ysh': [],
        'Vmax': [], 'Vmin': []
    }
    gen = {
        'bus': []
    }
    line = {
        'from_to': [],
        'r': [], 'x': [], 'Z': [],
        'max_i': [], 'status': []
    }
    #Barras
    for i in mat['mpc'][0][0][2]:
        bus['bus_i'].append(int(i[0]))
        bus['type'].append(int(i[1]))
        bus['P'].append(i[2])
        bus['Q'].append(i[3])
        bus['S'].append(complex(i[2], i[3]))
        bus['G'].append(i[4])
        bus['B'].append(i[5])
        bus['Ysh'].append(complex(i[4], i[5]))
        bus['Vmax'].append(i[11])
        bus['Vmin'].append(i[12])
    #Geradores
    for i in mat['mpc'][0][0][3]:
        gen['bus'].append(int(i[0]))
    #Linhas
    for i in mat['mpc'][0][0][4]:
        line['from_to'].append((int(i[0]), int(i[1])))
        line['r'].append(i[2])
        line['x'].append(i[3])
        line['Z'].append(complex(i[2], i[3]))
        line['max_i'].append(i[5]/Vb)
        line['status'].append(int(i[10]))
    #Adiciona num dict
    case = {'bus': bus, 'gen': gen, 'line': line, 'Sb': Sb, 'Vb': Vb}
    return case

def rnp2dp2(rnp):
    DP = np.zeros((1, 2), dtype=int)[:-1]
    dePara = []
    #Inverte a RNP
    rnp = [rnp[0][::-1], rnp[1][::-1]]          #Inverte a rnp
    for n, (i, j) in enumerate(zip(*rnp)):
        for m, (k, l) in enumerate(zip(*rnp)[n+1:]):
            if l == j-1:
                dePara.append((k, i))
                break
    return dePara

#Executa um fluxo de carga utilizando a rnp
def run_pf2(case, rnp):
    #Cria a estrutura dos resultados do fluxo de carga
    case['res_bus'] = {
        'bus': [] ,
        'Vm': [],
        'Va': []
    }
    case['res_line'] = {
        'from_to': [],
        'p_loss': [],
        'q_loss': [],
        'I': [],
        'loading': []
    }
    case['output'] = {
        'sum_p_loss': 0,                 
        'sum_q_loss': 0,                 
        'max_line_loading': 0,
        'max_sub_loading': 0,
        'min_voltage': [],
        'max_voltage': []
    }
    #Executa o fluxo de carga para cada uma das árvores
    for i in rnp:
        case = bfsa2(case, rnp[i])
    #TODO: Verificar se é necessário ordenar os resultados, ou só no final
    #==Saída de dados==#
    #Perdas
    case['output']['sum_p_loss'] = sum(case['res_line']['p_loss'])
    case['output']['sum_q_loss'] = sum(case['res_line']['q_loss'])
    #Tensão máxima
    vol_max = max(case['res_bus']['Vm'])
    id_max = [n for n, i in enumerate(case['res_bus']['Vm']) if i == vol_max]   #Encontra as barras
    bus_max = [case['res_bus']['bus'][i] for i in id_max]
    case['output']['max_voltage'] = [bus_max, vol_max]
    #Tensão mínima
    vol_min = min(case['res_bus']['Vm'])
    id_min = [n for n, i in enumerate(case['res_bus']['Vm']) if i == vol_min]   #Encontra as barras
    bus_min = [case['res_bus']['bus'][i] for i in id_min]
    case['output']['min_voltage'] = [bus_min, vol_min]
    return case
#Fluxo de carga
def bfsa2(case, rnp):
    t0 = timer()
    #Gera uma rnp invertida
    rnp_rev = [rnp[0][::-1], rnp[1][::-1]]
    #Leitura de dados
    bus = rnp[0]                            #Barras da rnp
    dp_all = case['line']['from_to']        #Todos as linhas possiveis
    dp = rnp2dp2(rnp)                       #Transforma a rnp em dp
    for n, i in enumerate(dp):              #Validação do de-para
        if not i in dp_all:
            dp[n] = (i[1], i[0])
    #==============================================================#
    #Estrutura de dados
    #Será um dicionário, onde o índice é uma tupla (no caso de linhas), 
    #e os outros valores serão números complexos da rede
    #0 - V (Tensões)
    #1 - S (Potência)
    #2 - I (Corrente)
    bus_data = dict()#.fromkeys(bus)
    #0 - Z (impedãncia)
    #1 - I_line (corrente na linha)
    #line_data = dict.fromkeys(dp2)
    line_data = dict.fromkeys(dp)
    #==============================================================#
    rnp_len = len(rnp[0])
    #Cálculo da tensão inicial
    tmp = complex(1, 0)
    bus_id = [case['bus']['bus_i'].index(i) for i in bus]
    for i, j in zip(bus, bus_id):
        bus_data[i] = [
            tmp, case['bus']['S'][j], 
            0, case['bus']['Ysh'][j]
        ]
    #Cálculo da impedância
    line_id = [case['line']['from_to'].index(i) for i in dp]
    for i, j in zip(dp, line_id):
        line_data[i] = [case['line']['Z'][j], 0]
    #==============================================================#
    #Loop
    p_err = 1
    q_err = 1
    vm_err = 1
    va_err = 1
    v_err = 1
    qq = 0
    #=========Passo 1=========#
    t0 = timer()
    V_ant = dict()
    for i in bus_data:
        V = bus_data[i][0]
        Y = bus_data[i][3]
        S = bus_data[i][1]
        #S = bus_data[i][1] + Y.conjugate()*V**2
        bus_data[i][2] = (S/V).conjugate() + Y*V        #Calcula as tensões
        V_ant[i] = V
    it = 0
    max_it = 100
    while v_err > 0.00000001:
        it += 1
        if it > max_it:
            raise Exception('Power Flow did not converge after %d iterations!' %max_it)
        #=========Passo 2=========#
        #for ix, [node, depth] in enumerate(rnp.T[::-1][:-1, :]):
        for ix, (node, depth) in enumerate(zip(*rnp_rev)[:-1]):
            #for node_bef, depth_bef in rnp.T[::-1][ix+1:, :]:
            for node_bef, depth_bef in zip(*rnp_rev)[ix+1:]:
                #Se for o nó pai
                if depth-1 == depth_bef:
                    #Somatório das correntes que saem
                    sum_I_line = 0
                    #Percorre a rnp e identifica os nós que saem do node
                    adj_nodes = []
                    #for node_aft in rnp.T[rnp_len-ix:, :]:
                    for node_aft in zip(*rnp)[rnp_len-ix:]:
                        if node_aft[1] <= depth:
                            break
                        if node_aft[1] == depth+1:
                            adj_nodes.append(node_aft[0])
                    #Soma as correntes deles
                    for adj in adj_nodes:
                        tup = (node, adj)
                        if not tup in dp:
                            tup = (adj, node)
                        sum_I_line += line_data[tup][1]
                    #Cálculo da corrente de linha
                    #Caso no de-para os nós estejam ao contrário
                    tup = (node_bef, node)
                    if not tup in dp:
                        tup = (node, node_bef)
                    line_data[tup][1] = bus_data[node][2] + sum_I_line
                    break
        # bus_data #
        #0 - V (Tensões)
        #1 - S (Potência)
        #2 - I (Corrente)
        #3 - Y (shunt)
        #=================#
        # line_data #
        #0 - Z (impedãncia)
        #1 - I_line (corrente na linha)
        #=========Passo 3=========#
        #Obrigado a fazer a pesquisa direta
        rs = len(rnp[0])    #Tamanho da rnp
        for ix, (node, depth) in enumerate(zip(*rnp)[1:]):
            for i in zip(*rnp_rev)[rs-ix-1:]:
                if depth == i[1]+1:
                    before_node = i[0]
                    break
            tup = (before_node, node)
            if not tup in dp:
                tup = (node, before_node)
            bus_data[node][0] = bus_data[before_node][0] - line_data[tup][0] * line_data[tup][1]
        # ========= Passo 1 e erro ========= #
        p_err = []
        q_err = []
        vm_err = []
        va_err = []
        v_err_a = []
        for i in bus_data:
            #Passo 1
            V = bus_data[i][0]
            Y = bus_data[i][3]
            S = bus_data[i][1]
            I = (S/V).conjugate() + Y*V
            bus_data[i][2] = I      #Adiciona o I no bus_data
            #Erro
            V_err = V - V_ant[i]
            v_err_a.append(abs(V - V_ant[i]))
            vm_err.append(abs(V_err.real))
            va_err.append(abs(V_err.imag))
            V_ant[i] = V
        vm_err = max(vm_err)
        va_err = max(va_err)
        v_err = max(v_err_a)
    # ===== Saída de dados ===== #
    root = rnp[0][0]
    res_bus = [[root, 1, 0]]
    res_branch = [[-1, -1, -1, -1]]
    #Barras
    for i in rnp[0][1:]:
        V = bus_data[i][0]
        real = V.real
        imag = V.imag
        Vm = np.hypot(real, imag)
        Va = np.arctan(imag/real)
        Va = np.rad2deg(Va)
        #res_bus.append([i, Vm, Va])
        case['res_bus']['bus'].append(i)
        case['res_bus']['Vm'].append(Vm)
        case['res_bus']['Va'].append(Va)

    #Linhas
    #Cálculo das perdas
    for n, i in enumerate(line_data):
        I = line_data[i][1]
        Z = line_data[i][0].conjugate()
        z1 = Z.real
        z2 = Z.imag
        t1 = z1*abs(I)**2
        t2 = z2*abs(I)**2
        p_loss = t1
        q_loss = t2
        case['res_line']['from_to'].append(i)
        case['res_line']['p_loss'].append(p_loss)
        case['res_line']['q_loss'].append(q_loss)
        case['res_line']['I'].append(I)
    return case


#==========================================#

#Executa um fluxo de carga utilizando a rnp
def run_pf(case, rnp):
    #Cria a estrutura dos resultados do fluxo de carga
    case['res_bus'] = pd.DataFrame(columns=['bus', 
                                            'V_mag', 'V_ang'])
    case['res_branch'] = pd.DataFrame(columns=['to', 'from',
                                               'p_loss', 'q_loss'])
    #Executa o fluxo de carga para cada uma das árvores
    for i in rnp:
        bfsa(case, rnp[i])
    #Ordena os resultados
    case['res_bus'] = case['res_bus'].sort_values(by='bus')
    #Ordena os resultados
    case['res_branch'] = case['res_branch'].sort_values(by='to')
    case['res_branch'] = case['res_branch'].set_index([range(len(case['res_branch']))])
    return case
#Transforma uma rnp em de-para
def rnp2dp(rnp):
    DP = np.zeros((1, 2), dtype=int)[:-1]
    rnp = rnp[:,::-1]
    for n, i in enumerate(rnp.T):
        for m, j in enumerate(rnp.T[n+1:,:]):
            if j[1] == i[1]-1:
                DP = np.vstack((DP, [j[0], i[0]]))
                break
    return  DP[::-1]

def dp2rnp(case):
    #de = case.line.ix[:, 2].values + 1 
    #para = case.line.ix[:, 3].values + 1
    #la = f.tf2la(de, para)  #Criação da Lista de Adjacência
    #Transformação do De-Para em uma RNP
    #de = case.line.ix[case.line.ix[:,-1].values].ix[:, 2].values + 1 
    #para = case.line.ix[case.line.ix[:,-1].values].ix[:, 3].values + 1
    status = case['branch'].status.values.astype(bool)
    de = case['branch'].fbus.values[status]
    para = case['branch'].tbus.values[status]
    dePara = np.vstack((de, para))
    dePara = np.transpose(dePara)
    paraDe = np.vstack((para, de))
    paraDe = np.transpose(paraDe)
    roots = case['gen'].bus.values
    #Transforma o De-Para num grafo
    dp = dict()
    for i in dePara:     
        if dp.has_key(i[0]):
            dp[i[0]].append(i[1])
        else:
            dp[i[0]] = [i[1]]
    for i in paraDe:     
        if dp.has_key(i[0]):
            dp[i[0]].append(i[1])
        else:
            dp[i[0]] = [i[1]]
    floresta = dict()
    for n, i in enumerate(roots): #For com todos os nós raizes para gerar todas as rnps
        tmp = dfs(dp, i)
        tmp = np.array(tmp)
        l = tmp.shape[1]
        floresta[str(n)] = np.vstack((tmp))
    return floresta
#Fluxo de carga
def bfsa(case, rnp):
    times = []
    t0 = timer()
    #Leitura de dados
    ids = []                                    #Ids
    id_rnp = range(len(rnp[0]))                 #id da rnp
    bus = rnp[0,:]                              #Barras da rnp
    bus_i = case['bus'].bus_i.values            #Todas as barras
    #Forma mais segura de se ter os dados
    dp2 = zip(case['branch'].fbus.values, case['branch'].tbus.values)
    dp = rnp2dp(rnp)                            #Transforma a rnp em dp
    dp = [(i, j) for i, j in dp]
    #Validação do de-para
    for n, i in enumerate(dp):
        if not i in dp2:
            dp[n] = (i[1], i[0])
    #==============================================================#
    #Estrutura de dados
    #Será um dicionário, onde o índice é uma tupla (no caso de linhas), 
    #e os outros valores serão números complexos da rede
    #0 - V (Tensões)
    #1 - S (Potência)
    #2 - I (Corrente)
    bus_data = dict.fromkeys(bus)
    #0 - Z (impedãncia)
    #1 - I_line (corrente na linha)
    #line_data = dict.fromkeys(dp2)
    line_data = dict.fromkeys(dp)
    #==============================================================#
    rnp_len = len(rnp[0, :])
    #++++++++++++++++++++++++++++++++++++++#
    times.append(timer() - t0)
    print 'Leitura dos dados\t%.4f' %times[-1]
    t0 = timer()
    #++++++++++++++++++++++++++++++++++++++#
    #Cálculo da tensão inicial
    for i in bus_data:
        Vm = case['bus'].Vm.loc[case['bus'].bus_i == i].values
        Va = case['bus'].Va.loc[case['bus'].bus_i == i].values
        P = case['bus'].Pd.loc[case['bus'].bus_i == i].values
        Q = case['bus'].Qd.loc[case['bus'].bus_i == i].values
        G = case['bus'].Gs.loc[case['bus'].bus_i == i].values
        B = case['bus'].Bs.loc[case['bus'].bus_i == i].values
        bus_data[i] = [complex(Vm, Va), complex(P, Q), 0, complex(G, B)]
    print '%.4f' %(timer() - t0)
    t0 = timer()
    #==============================================================#

    #Cálculo da impedância
    Z = []
    for n in line_data:
        i, j = n
        tmp = np.where((case['branch'].fbus == i) 
                     & (case['branch'].tbus == j))[0]
        r = case['branch'].r[tmp]
        x = case['branch'].x[tmp]
        line_data[n] = [complex(r, x), 0]

    #++++++++++++++++++++++++++++++++++++++#
    times.append(timer() - t0)
    print 'Preprocessamento\t%.4f' %times[-1]
    t0 = timer()
    #++++++++++++++++++++++++++++++++++++++#
    #==============================================================#
    #Loop
    p_err = 1
    q_err = 1
    vm_err = 1
    va_err = 1
    v_err = 1
    qq = 0
    #=========Passo 1=========#
    V_ant = dict()
    t0 = timer()
    for i in bus_data:
        V = bus_data[i][0]
        Y = bus_data[i][3]
        S = bus_data[i][1]
        #S = bus_data[i][1] + Y.conjugate()*V**2
        #print (S/V).conjugate() - Y*V
        bus_data[i][2] = (S/V).conjugate() + Y*V
        V_ant[i] = V
    print 'passo 1', timer() - t0
    while v_err > 0.00000001:
        #=========Passo 2=========#
        t0 = timer()
        for ix, [node, depth] in enumerate(rnp.T[::-1][:-1, :]):
            for node_bef, depth_bef in rnp.T[::-1][ix+1:, :]:
                #Se for o nó pai
                if depth-1 == depth_bef:
                    #Somatório das correntes que saem
                    sum_I_line = 0
                    #Percorre a rnp e identifica os nós que saem do node
                    adj_nodes = []
                    for node_aft in rnp.T[rnp_len-ix:, :]:
                        if node_aft[1] <= depth:
                            break
                        if node_aft[1] == depth+1:
                            adj_nodes.append(node_aft[0])
                    #Soma as correntes deles
                    for adj in adj_nodes:
                        tup = (node, adj)
                        if not tup in dp:
                            tup = (adj, node)
                        sum_I_line += line_data[tup][1]
                    #Cálculo da corrente de linha
                    #Caso no de-para os nós estejam ao contrário
                    tup = (node_bef, node)
                    if not tup in dp:
                        tup = (node, node_bef)
                    line_data[tup][1] = bus_data[node][2] + sum_I_line
                    break
        # bus_data #
        #0 - V (Tensões)
        #1 - S (Potência)
        #2 - I (Corrente)
        #3 - Y (shunt)
        #=================#
        # line_data #
        #0 - Z (impedãncia)
        #1 - I_line (corrente na linha)
        print 'passo 2', timer() - t0
        t0 = timer()
        #=========Passo 3=========#
        #Obrigado a fazer a pesquisa direta
        rs = len(rnp[0])    #Tamanho da rnp
        for ix, [node, depth] in enumerate(rnp.T[1:, :]):
            for i in rnp.T[::-1][rs-ix-1:, :]:
                if depth == i[1]+1:
                    before_node = i[0]
                    break
            tup = (before_node, node)
            if not tup in dp:
                tup = (node, before_node)
            #bus_data[node][0] = (bus_data[before_node][0] -
            #                    line_data[ix][0] * 
            #                    (line_data[ix][1] * (-1)))
            bus_data[node][0] = bus_data[before_node][0] - line_data[tup][0] * line_data[tup][1]
        print 'passo 3', timer() - t0
        t0 = timer()
        # ========= Passo 1 e erro ========= #
        p_err = []
        q_err = []
        vm_err = []
        va_err = []
        v_err_a = []
        for i in bus_data:
            #Passo 1
            V = bus_data[i][0]
            Y = bus_data[i][3]
            S = bus_data[i][1]
            I = (S/V).conjugate() + Y*V
            bus_data[i][2] = I      #Adiciona o I no bus_data
            #Erro
            V_err = V - V_ant[i]
            v_err_a.append(abs(V - V_ant[i]))
            vm_err.append(abs(V_err.real))
            va_err.append(abs(V_err.imag))
            V_ant[i] = V
        vm_err = max(vm_err)
        va_err = max(va_err)
        v_err = max(v_err_a)
        print 'passo 1 e erro', timer() - t0
    #++++++++++++++++++++++++++++++++++++++#
    times.append(timer() - t0)
    print 'Algoritmo\t\t%.4f' %times[-1]
    t0 = timer()
    #++++++++++++++++++++++++++++++++++++++#
    # ===== Saída de dados ===== #
    root = rnp[0, 0]
    res_bus = [[root, 1, 0]]
    res_branch = [[-1, -1, -1, -1]]
    #Barras
    for i in rnp[0, 1:]:
        V = bus_data[i][0]
        real = V.real
        imag = V.imag
        Vm = np.hypot(real, imag)
        Va = np.arctan(imag/real)
        Va = np.rad2deg(Va)
        res_bus.append([i, Vm, Va])

    #Linhas
    #Cálculo das perdas
    for n, i in enumerate(line_data):
        #c = complex(line_data[i][1], I_line[n, 3])
        I = line_data[i][1]
        Z = line_data[i][0].conjugate()
        z1 = Z.real
        z2 = Z.imag
        t1 = z1*abs(I)**2
        t2 = z2*abs(I)**2
        p_loss = t1
        q_loss = t2

        res_branch.append([i[0], i[1], p_loss, q_loss])
    res_branch = res_branch[1:]
    """
    #ADICIONAR AS TENSOES E PERDAS NO CASE
    #Adiciona as variáveis no case
    """
    case['res_bus'] = case['res_bus'].append(pd.DataFrame(res_bus, 
                                      columns=['bus', 'V_mag', 'V_ang']))
    case['res_branch'] = case['res_branch'].append(
        pd.DataFrame(res_branch, columns=['to', 'from', 
                                          'p_loss', 'q_loss']))

    #++++++++++++++++++++++++++++++++++++++#
    times.append(timer() - t0)
    print 'Finalização\t\t%.4f' %times[-1]
    t0 = timer()
    #++++++++++++++++++++++++++++++++++++++#
    print 'Tempo total\t\t%.4f' %sum(times)
    return case
#Carrega um caso e joga ele num dict de pandas
    def load_case(path):
        #Carrega o arquivo .mat
        mat = loadmat(path)
        #Separa ele em pandas
        #Barras
        bus = pd.DataFrame(mat['mpc'][0][0][2], columns=['bus_i', 'type', 
                                                         'Pd', 'Qd', 'Gs', 
                                                         'Bs', 'area', 'Vm', 
                                                         'Va', 'baseKV', 
                                                         'zone', 'Vmax', 
                                                         'Vmin'])
        #Geradores
        gen = pd.DataFrame(mat['mpc'][0][0][3], columns=['bus', 'Pg', 'Qg', 
                                                         'Qmax', 'Qmin', 'Vg', 
                                                         'mBase', 'status', 'Pmax', 
                                                         'Pmin', 'Pc1', 'Pc2', 
                                                         'Qc1min', 'Qc1max', 
                                                         'Qc2min', 'Qc2max', 
                                                         'ramp_agc', 'ramp_10', 
                                                         'ramp_30', 'ramp_q', 
                                                         'apf'])
        #Linhas
        branch = pd.DataFrame(mat['mpc'][0][0][4], columns= ['fbus', 'tbus', 'r', 
                                                             'x', 'b', 'rateA', 
                                                             'rateB', 'rateC',  
                                                             'ratio', 'angle', 
                                                             'status', 'angmin', 
                                                             'angmax'])
        #Adiciona num dict
        case = {'bus': bus, 'gen': gen, 'branch': branch}
        return case


"""
path = 'cases/case33.mat'
case = load_case(path)              #Carrega um caso
rnp = dp2rnp(case)                  #Transforma ele numa rnp
tt = timer()
case = run_pf(case, rnp)
#print 'Tempo total de execução:', timer() - tt
------
"""
path = 'cases/case119.mat'
case = load_mpc(path)              #Carrega um caso
rnp = dp2rnp2(case)
tt = timer()
run_pf2(case, rnp)
print 'Tempo total de execução:', timer() - tt
