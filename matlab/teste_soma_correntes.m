clc
clear

Vpu = 1;

S_13 = 1.0+0.9*i;
S_14 = 1.0+0.7*i;
S_15 = 1.0+0.9*i;
S_16 = 2.1+1.0*i;

Q_14 = -i*1.8;
Q_16 = -i*1.8;

i_15_c = S_15/Vpu 
i_16_c = S_16/Vpu + Q_16
i_14_c = S_14/Vpu + Q_14
i_13_c = S_13/Vpu

i_15 = conj(i_15_c)
i_16 = conj(i_16_c)
i_14 = conj(i_14_c)
i_13 = conj(i_13_c)


i_15to16 = -(i_16)+0
i_13to15 = -(i_15)+i_15to16
i_13to14 = -(i_14)+ 0
i_03to13 = -(i_13)+i_13to15+i_13to14

Stotal = S_13+S_14+S_15+S_16+Q_14+Q_16


