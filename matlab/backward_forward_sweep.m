clear all
%branch no sending reciving  R(PU)            X(pu)
LD=[  1     1      2       0.04997         0.06644; 
      2     2      3       0.02332         0.03310 ; 
      3     1      4       0.04664         0.06201;
      4     1      5       0.02082         0.02768;
      5     5      6       0.02500         0.03322;
      6     1      7       0.02665         0.03543;
      7     7      8       0.02748         0.03654;
      8     1      9       0.03331         0.04430;
      9     1      10      0.02082         0.02768];   
     % 10   2      11      0.02082         0.02768];
  
     % bus no     activepower   reactivepower
  BD=[ 1          0              0
         2         1.22          0.916
         3         0.032         0.024 
         4         0.778         0.584
         5         0.673         0.595
         6         1.22          0.916
         7         0.0488        0.0366
         8         0.956         0.717
         9         0.698         0.523
         10        1.265         0.949 ] ; 
         %11        0.265         0.0949];

  dp=LD(:,2:3);					%De Para
M=max(LD(:,2:3));				%Máximo de-para
N=max(M);						%Máximo
f=[1:N]';
%Cria o h
for i=1:N
    g=find(dp(:,:)==i);
    h(i)=length(g);
end
k(:,1)=f;
k(:,2)=h';
  cent=input('central bus  ');
% this section of the code is to adjust line data to the standard 
  NLD=zeros(N,size(LD,2));
c=find(LD(:,2:3)==cent);
NLD=LD(c,:);
LD(c,:)=[];
t=find(k(:,1)==cent);
k(t,2)=k(t,2)-size(c,1);
j=size(c,1);
i=1;
while sum(k(:,2))>0
    c=[];
    b=[];
    t=[];
    [c e]=find(LD(:,2:3)==NLD(i,3));
    if size(c,2)~=0
        b=LD(c,:);
        LD(c,:)=[];
        t=find(k(:,1)==NLD(i,3));
        k(t,2)=k(t,2)-(size(c,1)+1);
        d=find(b(:,3)==NLD(i,3));
        b(d,2:3)=[b(d,3),b(d,2)];
        NLD(j+1:j+size(c,1),:)=b;
        j=j+size(c,1);
    end
    i=i+1;
end
LD=sortrows(NLD,3);
% end the data is represented in standard format

%code for  bus-injection to branch-current matrix
bibc=zeros(size(LD,1),size(LD,1));
for i=1:size(LD,1)
    
   
    if LD(i,2)==1
      bibc(LD(i,3)-1,LD(i,3)-1)=1;
    else
       bibc(:,LD(i,3)-1)=bibc(:,LD(i,2)-1);
       bibc(LD(i,3)-1,LD(i,3)-1)=1;
    end
end
S=complex(BD(:,2),BD(:,3));% complex power
Vo=ones(size(LD,1),1);% initial bus votage% 10 change to specific data value
S(1)=[];
VB=Vo;
iteration=input('number of iteration : ');

for i=1:iteration
     %backward sweep
    I=conj(S./VB);% injected current
    Z=complex(LD(:,4),LD(:,5));%branch impedance
    ZD=diag(Z);%makeing it diagonal
    IB=bibc*I;  %branch current
    
    %forward sweep
    TRX=bibc'*ZD*bibc;
    VB=Vo-TRX*I;
end
Vo

Vbus=[1;VB]
display(Vbus);
display(IB);
% reference 
%1)An Effective Approach for Distribution System Power Flow Solution A. Alsaadi, and B. Gholam
%2)The Standard Backward/Forward Sweep Power Flow Paulo M. De Oliveira-De Jesus, Member, IEEE
