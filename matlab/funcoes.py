# -*- coding: utf-8 -*-
#===================================#
# File name: funcoes.py             #
# Author: Vitor dos Santos Batista  # # Date created: 14/06/2016          #
# Date last modified: 20/06/2016    #
# Python Version: 2.7               #
#===================================#
import numpy as np
import pandas as pd
import pandapower as pp
import copy

#Operador 1 PAO para a mutação
def op12(de, para, p, a, la=None, falta=None):
    """
    Verifica a LA para encontrar 
      os possíveis casos de união
    Vou partir do presuposto de 
      que p e a são compatíveis
    print 'poda', p
    print 'adj', a
    """
    if la:
        if not isNeighbor(p, a, la):        #Erro dos nodos
            raise ValueError(erro['na'])
    Ip = np.where(de[0, :] == p)[0][0]      #Índice do nó p
    Pp = de[1, Ip]                          #Profundidade do nó p
    Ia = np.where(para[0, :] == a)[0][0]    #Índice do nó a
    Pa = para[1, Ia]                        #Profundidade do nó a
    Iu = Ip                                 #Índice do nó u
    #Encontra o pai do P
    for i in range(Ip, -1, -1):
        if de[1, i] < Pp:
            pai_p = de[0, i]
            break
    tmp = de[:, Ip]                         #Criação da sub-arvore tmp
    tmp = np.reshape(tmp, (2, 1))
    for i in range(Ip+1, de.shape[1]):
        if de[1, i] <= Pp:
            break
        tmp = np.hstack((tmp,               #Finalização da sub-arvore tpm 
            np.reshape(de[:, i], 
                       (2, 1))))
        Iu = Iu + 1                         #Atualização do Iu
    tmp[1, :] = tmp[1, :] - Pp + Pa + 1     #Ataulização da profundidade
    para = np.insert(para, obj=[Ia+1],      #Atualização da RNP para 
                     values=tmp, axis=1)
    del_falta = tmp[0, :]                   #Valores para remover da falta 
    de = np.delete(de,                      #Atualização da RNP de 
                   range(Ip, Iu+1), 1)
    #Alterações
    sai = [pai_p, p]
    entra = [p, a]
    if not falta == None:
        for d in del_falta:                     #Busca para remover os nos da falta
            ind_d = np.where(d == falta[:, 0])
            falta = np.delete(falta, ind_d, 0)
        return de, para, falta
    return de, para, sai, entra
#Operador 2 CAO para a mutação
def op22(de, para, p, a, r, la=None, falta=None):
    """
    Verifica a LA para encontrar 
      os possíveis casos de união
    Vou partir do presuposto de 
      que p e a são compatíveis
    print 'poda', p
    print 'raiz', r
    print 'adj', a
    """
    if la:
        if not isNeighbor(r, a, la):        #Erro dos nodos
            raise ValueError(erro['na'])
    Ip = np.where(de[0, :] == p)[0][0]      #índice do nó p
    Pp = de[1, Ip]                          #profundidade do nó p
    Ia = np.where(para[0, :] == a)[0][0]    #índice do nó a
    Pa = para[1, Ia]                        #profundidade do nó a
    Ir = np.where(de[0, :] == r)[0][0]    #Índice do nó r
    Pr = de[1, Ir]                        #Profundidade do nó r
    Iu = Ip                                 #Índice do nó u
    #Encontra o pai do P
    for i in range(Ip, -1, -1):
        if de[1, i] < Pp:
            pai_p = de[0, i]
            break
    tmp = np.zeros((2, 1), dtype=int)[:, :-1]
    lista_r_p = caminho(Ir, Ip, de)                          #lista com os nós de r até p
    for i in lista_r_p:
        de, tmp2, Pa = cortaArvore(de, i, Pa)
        tmp = np.hstack((tmp, tmp2))

    para = np.insert(para, [Ia+1],          #Atualização da RNP para 
                     tmp, axis=1)
    #Alterações
    sai = [pai_p, p]
    entra = [r, a]
    #print 'pai do p', pai_p
    #print 'poda', p
    #print 'raiz', r
    #print 'adj', a
    if falta:
        del_falta = tmp[0, :]                   #Valores para remover da falta 
        for d in del_falta:                     #Busca para remover os nos da falta
            ind_d = np.where(d == falta[:, 0])
            falta = np.delete(falta, ind_d, 0)
        return de, para, falta
    else:
        return de, para, sai, entra

#Operador 1 PAO
def op1(de, para, p, a, la=None, falta=None):
    """
    Verifica a LA para encontrar 
      os possíveis casos de união
    Vou partir do presuposto de 
      que p e a são compatíveis
    """
    if la:
        if not isNeighbor(p, a, la):        #Erro dos nodos
            raise ValueError(erro['na'])
    Ip = np.where(de[0, :] == p)[0][0]      #Índice do nó p
    Pp = de[1, Ip]                          #Profundidade do nó p
    Ia = np.where(para[0, :] == a)[0][0]    #Índice do nó a
    Pa = para[1, Ia]                        #Profundidade do nó a
    Iu = Ip                                 #Índice do nó u
    tmp = de[:, Ip]                         #Criação da sub-arvore tmp
    tmp = np.reshape(tmp, (2, 1))
    for i in range(Ip+1, de.shape[1]):
        if de[1, i] <= Pp:
            break
        tmp = np.hstack((tmp,               #Finalização da sub-arvore tpm 
            np.reshape(de[:, i], 
                       (2, 1))))
        Iu = Iu + 1                         #Atualização do Iu
    tmp[1, :] = tmp[1, :] - Pp + Pa + 1     #Ataulização da profundidade
    para = np.insert(para, obj=[Ia+1],      #Atualização da RNP para 
                     values=tmp, axis=1)
    del_falta = tmp[0, :]                   #Valores para remover da falta 
    de = np.delete(de,                      #Atualização da RNP de 
                   range(Ip, Iu+1), 1)
    if not falta == None:
        for d in del_falta:                     #Busca para remover os nos da falta
            ind_d = np.where(d == falta[:, 0])
            falta = np.delete(falta, ind_d, 0)
        return de, para, falta
    return de, para
#Operador 2 CAO
def op2(de, para, p, a, r, la=None, falta=None):
    """
    Verifica a LA para encontrar 
      os possíveis casos de união
    Vou partir do presuposto de 
      que p e a são compatíveis
    """
    if la:
        if not isNeighbor(r, a, la):        #Erro dos nodos
            raise ValueError(erro['na'])
    Ip = np.where(de[0, :] == p)[0][0]      #índice do nó p
    Pp = de[1, Ip]                          #profundidade do nó p
    Ia = np.where(para[0, :] == a)[0][0]    #índice do nó a
    Pa = para[1, Ia]                        #profundidade do nó a
    Ir = np.where(de[0, :] == r)[0][0]    #Índice do nó r
    Pr = de[1, Ir]                        #Profundidade do nó r
    Iu = Ip                                 #Índice do nó u
    tmp = np.zeros((2, 1), dtype=int)[:, :-1]
    lista_r_p = caminho(Ir, Ip, de)                          #lista com os nós de r até p
    for i in lista_r_p:
        de, tmp2, Pa = cortaArvore(de, i, Pa)
        tmp = np.hstack((tmp, tmp2))

    para = np.insert(para, [Ia+1],          #Atualização da RNP para 
                     tmp, axis=1)
    if falta:
        del_falta = tmp[0, :]                   #Valores para remover da falta 
        for d in del_falta:                     #Busca para remover os nos da falta
            ind_d = np.where(d == falta[:, 0])
            falta = np.delete(falta, ind_d, 0)
        return de, para, falta
    else:
        return de, para

def caminho(ir, ip, de):
    cam = de[:, ir]
    cam = np.reshape(cam, (2, 1))
    de = de[:, ip:ir]
    tmp = cam[1][0]
    de = np.fliplr(de)
    #inverteo array
    for i in de.T:
        if i[1] < tmp:
            tmp = i[1]
            cam = np.hstack((cam, np.reshape(i,(2, 1))))

    return cam[0, :]

def cortaArvore(de, p, Pa):
    Ip = np.where(de[0, :] == p)[0][0]      #índice do nó p
    Pp = de[1, Ip]                          #profundidade do nó p
    Iu = Ip                                 #Índice do nó u
    tmp = de[:, Ip]                         #Criação da sub-arvore tmp
    tmp = np.reshape(tmp, (2, 1))
    for i in range(Ip+1, de.shape[1]):
        if de[1, i] <= Pp:
            break
        tmp = np.hstack((tmp,               #Finalização da sub-arvore tpm 
            np.reshape(de[:, i], 
                       (2, 1))))
        Iu = Iu + 1                         #Atualização do Iu
    de = np.delete(de,                      #Atualização da RNP de 
                   range(Ip, Iu+1), 1)
    tmp[1, :] = tmp[1, :] - Pp + Pa + 1     #Ataulização da profundidade
    return de, tmp, tmp[1, 0] 

def isNeighbor(p, a, la):
    #la = pd.read_csv(la, header=None, sep=',')
    p = int(p)
    #a = (la.loc[la.iloc[0] == p] == a).values[0]
    return True in la.loc[p]

#Fluxo de carga
def rnp_depara_fluxo_pcf(ind, case):
    """
    Esta função varre toda a rnp, transforma ela no de-para e logo em
    seguida, adiciona o valor de True no 'case' e por fim, executa p fluxo de
    carga. Na adição dos valores no 'case', é adiciona na ida e na volta, pois
    o algoritmo que transforma a rnp em de-para, as vezes (por causa do CAO),
    pode transformar o de-para num para-de. Por isso, é realizado desta forma.
    """
    case.line.ix[:,-1] = False        #Todos os valores como o caso pai
    #print np.hstack((case.line.ix[:,2:4].values+1, case.line.ix[:,-1:].values))
    for i in ind:
        tmp = ind[i][:2,:]                  #tmp recebe só o nó e a prof
        if len(tmp[0]) > 1:                 #Se as árvores forem > 1
            for l, k in enumerate(tmp.T):
                for m in tmp.T[l+1:]:
                    if m[1] == k[1]+1:
                        case.line.loc[(case.line.ix[:,2].values == k[0]-1) &
                                      (case.line.ix[:,3].values == m[0]-1), 
                                      'in_service'] = True
                        case.line.loc[(case.line.ix[:,2].values == m[0]-1) &
                                      (case.line.ix[:,3].values == k[0]-1), 
                                      'in_service'] = True
    #Calcula o Fluxo de carga
    pp.runpp(case)
    #print np.hstack((case.line.ix[:,2:4].values+1, case.line.ix[:,-1:].values))


#Fluxo de carga
def rnp_depara_fluxo(entra, sai, case2, basic_case, father_case, nm_pai):
    """
    Esta função varre toda a rnp, transforma ela no de-para e logo em
    seguida, adiciona o valor de True no 'case' e por fim, executa p fluxo de
    carga. Na adição dos valores no 'case', é adiciona na ida e na volta, pois
    o algoritmo que transforma a rnp em de-para, as vezes (por causa do CAO),
    pode transformar o de-para num para-de. Por isso, é realizado desta forma.
    """
    case = copy.deepcopy(case2)             #Problemas com POO
    case.line.ix[:,-1] = father_case        #Todos os valores como o caso pai
    #Índice dos nós que serão alterados
    isai = case.line.loc[(case.line.ix[:,2].values == sai[0]-1) &
                         (case.line.ix[:,3].values == sai[1]-1)].index.tolist()
    ientra = case.line.loc[(case.line.ix[:,2].values == entra[0]-1) &
                       (case.line.ix[:,3].values == entra[1]-1)].index.tolist()
    #print case.line.iloc[(case.line.ix[:,2].values == entra[0]-1) &
    #                                  (case.line.ix[:,3].values == entra[1]-1)]
    #Altera o de-para
    if isai:
        case.line.ix[isai, -1] = False
    else:
        isai = case.line.loc[(case.line.ix[:,2].values == sai[1]-1) &
                             (case.line.ix[:,3].values == sai[0]-1)].index.tolist()
        case.line.ix[isai, -1] = False
    if ientra:
        case.line.ix[ientra, -1] = True
    else:
        ientra = case.line.loc[(case.line.ix[:,2].values == entra[1]-1) &
                               (case.line.ix[:,3].values == entra[0]-1)].index.tolist()
        case.line.ix[ientra, -1] = True

    new_case = case.line.ix[:, -1].values
    #Calculo do número de manobras
    if (basic_case[ientra] == new_case[ientra]) & (basic_case[isai] == new_case[isai]):
        nm = nm_pai-2 
    elif (basic_case[ientra] != new_case[ientra]) & (basic_case[isai] != new_case[isai]): 
        nm = nm_pai+2 
    else:
        nm = nm_pai


    #Calcula o Fluxo de carga
    #print 'entra', entra
    #print 'sai', sai
    #print case.line.ix[:,-1]
    #print np.hstack((case.line.ix[:,2:4].values+1, case.line.ix[:,-1:].values))
    #pp.to_excel(case, 'analise.xlsx')

    pp.runpp(case)
    #print 'Caso executado com sucesso'
    """
    try:
        pp.runpp(case)
        pp.to_excel(case, 'rodou.xlsx')
    except:
        print 'dados salvos para anlálise'
        pp.to_excel(case, 'para_analisar.xlsx')
        raise
    """
    fit = fitness_for_table(case)
    vg, xg, yg = fit
    agg = agg_function(nm, fit)
    return new_case, agg, nm, vg, xg, yg

    """
    #print np.hstack((case.line.ix[:,2:4].values+1, case.line.ix[:,-1:].values))
    if any((case.line.ix[:,2].values == sai[0]-1) & 
           (case.line.ix[:,3].values == sai[1]-1)):
        case.line.loc[(case.line.ix[:,2].values == sai[0]-1) &
                      (case.line.ix[:,3].values == sai[1]-1), 
                      'in_service'] = False
    else:
        case.line.loc[(case.line.ix[:,2].values == sai[1]-1) &
                      (case.line.ix[:,3].values == sai[0]-1), 
                      'in_service'] = False
    if any((case.line.ix[:,2].values == entra[0]-1) & 
           (case.line.ix[:,3].values == entra[1]-1)):
        case.line.loc[(case.line.ix[:,2].values == entra[0]-1) &
                      (case.line.ix[:,3].values == entra[1]-1), 
                      'in_service'] = True
    else:
        case.line.loc[(case.line.ix[:,2].values == entra[1]-1) &
                      (case.line.ix[:,3].values == entra[0]-1), 
                      'in_service'] = True
        """
    #Calculo do número de manobras
    


    """
    #print np.hstack((case.line.ix[:,2:4].values+1, case.line.ix[:,-1:].values))
    for i in ind:
        tmp = ind[i][:2,:]                  #tmp recebe só o nó e a prof
        if len(tmp[0]) > 1:                 #Se as árvores forem > 1
            for l, k in enumerate(tmp.T):
                for m in tmp.T[l+1:]:
                    if m[1] == k[1]+1:
                        case.line.loc[(case.line.ix[:,2].values == k[0]-1) &
                                      (case.line.ix[:,3].values == m[0]-1), 
                                      'in_service'] = True
                        case.line.loc[(case.line.ix[:,2].values == m[0]-1) &
                                      (case.line.ix[:,3].values == k[0]-1), 
                                      'in_service'] = True
    """
#Mutação
def mut(ind_mut, mut_type, la):
    """
    O laço é necessário, pois não é possível selecionar as árvores com apenas a
    subestação para ser realizado a poda.
    """
    while True:
        #Indivíduo mutado
        t_origem = str(np.random.randint(len(ind_mut)))             #Seleciona aleatoriamente uma árvore
        tmp = ind_mut[str(t_origem)][0, 1:]                         #Verifica se a árvore não está vazia
        if any(tmp):
            Ip = np.random.randint(len(tmp)+1)                    #Seleciona aleatoriamente um índice para o nó de poda
            p = ind_mut[t_origem][0,Ip]                             #Nó de poda
            ar_tmp = ind_mut[t_origem]                              #Árvore t_origem
            #Seleciona os vizinhos de p - V(p)
            Pp = ar_tmp[1,Ip]                                   #Profundidade de p
            vp = ar_tmp[0, np.where(                            #Nós com a prof do p mais
                ar_tmp[1,:] == Pp+1)]                           #um (são os imediatamente após ele)
            vp = np.append(vp, ar_tmp[0, np.where(              #Nós com a prof do p menos um
                ar_tmp[1,:] == Pp-1)])
            #Seleciona os adjacentes a p - A(p)
            ap = la.loc[p].values 
            ap = ap[~np.isnan(ap)]
            #Diferença entre ap e vp (A(p)/V(p))
            diff_ap_vp = np.setdiff1d(ap, vp)
            """
            print ind_mut[t_origem]
            print len(tmp)
            print 'poda', p
            print 'vizinhos do poda', vp
            print 'adjacentes do poda', ap
            print 'diff ap, vp', diff_ap_vp
            """
            #Verifica se existe algo no diff_ap_vp
            if any(diff_ap_vp):
                #print 'teste 1'
                a = np.random.choice(diff_ap_vp)                #Seleciona um nó aleatório 
                #Verifica se o nó adjacente está em t_origem
                #Se estiver, ele não é executado
                if not any(ind_mut[t_origem][0,:] == a):
                    if mut_type == 0:                               #Se for 0, aplica o PAO
                        #Procura a rnp que está o 'a'
                        for i in ind_mut:
                            #print ind_mut[i][0] == a
                            if any(ind_mut[i][0] == a):
                                #print 'teste 2'
                                fa = str(i)                         #Índice da floresta do a
                                #Aplica o PAO
                                ind_mut[t_origem], ind_mut[fa], sai, entra = op12(
                                    ar_tmp[:2,:],
                                    ind_mut[fa][:2,:], p, a)
                                #Atualização dos índices
                                #Árvore De
                                ln = ind_mut[t_origem].shape[1]     #Tamanho da nova árvore
                                ind_mut[t_origem] = np.vstack((     #Atualiza os indices
                                    ind_mut[t_origem], range(ln)))
                                #Árvore Para
                                ln = ind_mut[fa].shape[1]           #Tamanho da nova árvore
                                ind_mut[fa] = np.vstack((           #Atualiza os indices
                                    ind_mut[fa], range(ln)))
                                #print 'Entra', entra
                                #print 'Sai', sai
                                return [ind_mut, sai, entra, p, a]
                    elif mut_type == 1:                             #Se for 1, aplica o CAO
                        r = p                                       #Convenção
                        Ir = Ip                                     #Convenção
                        Pr = Pp                                     #Convenção
                        #Encontra o nó de poda (p)
                        #Seleciona o caminho do novo nó raiz até a subestação
                        p = caminho(Ir, 0, ind_mut[t_origem][:2,:])
                        p = p[:-1]
                        #print 'caminho', p
                        #print 'a', a
                        if any(p):
                            p = np.random.choice(p)                     #Escolhe um nó de poda aleatório
                        else:
                            p = r
                        #Procura a rnp que está o 'a'
                        for i in ind_mut:
                            #print ind_mut[i][0] == a
                            if any(ind_mut[i][0] == a):
                                #print 'teste 3'
                                fa = i                              #Índice da floresta do a
                                #Aplica o CAO
                                ind_mut[t_origem], ind_mut[fa], sai, entra = op22(
                                    ar_tmp[:2,:],
                                    ind_mut[fa][:2,:], 
                                    p, a, r)
                                #Atualização dos índices
                                #Árvore De
                                ln = ind_mut[t_origem].shape[1]     #Tamanho da nova árvore
                                ind_mut[t_origem] = np.vstack((     #Atualiza os indices
                                    ind_mut[t_origem], range(ln)))
                                #Árvore Para
                                ln = ind_mut[fa].shape[1]           #Tamanho da nova árvore
                                ind_mut[fa] = np.vstack((           #Atualiza os indices
                                    ind_mut[fa], range(ln)))
                                #print 'Entra', entra
                                #print 'Sai', sai
                                return [ind_mut, sai, entra, p, r, a]
def agg_function(nm, pf_out):
    #Pesos (Serão colocados no futuro)
    """
    pv = 1 
    px = 1
    pb = 1
    py = 1
    pm = 1
    """
    vg, xg, yg = pf_out
    #vg, xg, bg, yg = pf_out
    #Restrições
    if vg > 0.5:
        vg += 100
    if xg > 100:
        xg += 100
    """
    if bg > 100:
        bg += 100
    """
    return nm+vg+xg+yg
    #return nm+vg+xg+bg+yg

def fitness_for_table(case):
    #Maior queda de tensão
    vg = max(abs(1-case.res_bus.vm_pu.dropna()))
    #Maior carregamento da rede
    xg = max(case.res_line.loading_percent.dropna())
    #Perdas ativa totais
    yg = sum(case.res_line.pl_kw.dropna())
    #Maior carregamento da subestação
    bg = 0
    return [vg, xg, yg]
    #return [vg, xg, yg, bg]

import pandapower.converter as pc
#Transforma o de-para numa lista de adjacência
def tf2la(de, para):
    la = np.union1d(de, para)
    l = dict()
    for n, i in enumerate(la):
            ind_de = np.where(de == i)[0]
            ind_para = np.where(para == i)[0]
            if (ind_de+1).any():
                if l.has_key(i):
                    l[i] = np.append(l[i], para[ind_de])
                else:
                    l[i] = para[ind_de]
            if (ind_para+1).any():
                if l.has_key(i):
                    l[i] = np.append(l[i], de[ind_para])
                else:
                    l[i] = de[ind_para]
    df = pd.DataFrame.from_dict(l, orient='index')
    return df
