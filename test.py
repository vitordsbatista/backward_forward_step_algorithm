#!/usr/bin/python
# -*- coding: utf-8 -*-
#===================================#
# File name: 						#
# Author: Vitor dos Santos Batista	#
# Date created: 					#
# Date last modified: 				#
# Python Version: 2.7				#
#===================================#

import numpy as np
import pandapower as pp
import pandapower.converter as pc
import pandas as pd
import bfnd as p
from timeit import default_timer as timer
import sys

def open_switch(de, para):
    case2.line.in_service.loc[(case2.line.from_bus.values == de-1) & (case2.line.to_bus.values == para-1)] = False
    case2.line.in_service.loc[(case2.line.from_bus.values == para-1) & (case2.line.to_bus.values == de-1)] = False

#Testes
"""
path = 'case84_12_npu.mat'
ath = 'case84_12.mat'
path = 'case84new.mat'
path = 'case53new.mat'
path = 'case16new.mat'
path = 'case135new.mat'
"""

def test_time(path):
    """
    #Meu
    case = p.load_case(path)              #Carrega um caso
    rnp = p.dp2rnp(case)                  #Transforma ele numa rnp

    t0m = timer()
    case = p.run_pf(case, rnp)
    tfm = timer()
    case['res_bus'].bus = case['res_bus'].bus.astype(int) - 1
    case['res_bus'] = case['res_bus'].set_index('bus')
    a = case['res_bus'].V_mag
    a1 = case['res_bus'].V_ang
    pm = abs(case['res_branch'].p_loss)
    qm = abs(case['res_branch'].q_loss)
    print 'BFSA: %.3f' %(sum(pm)*1000)
    print sum(pm) * 1000
    print 'Menor tensão:',
    print '%.3f' %min(a)
    print 'Na Barra:',
    print (case['res_bus'].index.values[np.where(a == min(a))]+1)
    tm = (tfm - t0m)
    print 'BFSA: %.3f segundos' %tm
    """
    #Meu
    case2 = p.load_mpc(path)              #Carrega um caso
    rnp2 = p.dp2rnp2(case2)                  #Transforma ele numa rnp
    t0m2 = timer()
    case3 = p.run_pf2(case2, rnp2)
    tfm2 = timer()

    #Pandapower
    case2 = pc.from_mpc(path)
    t0p = timer()
    pp.runpp(case2)#, algorithm='bfsw')
    tfp = timer()
    b = case2.res_bus.vm_pu
    b1 = case2.res_bus.va_degree
    p_p = case2.res_line.pl_kw
    q_p = case2.res_line.ql_kvar

    print '===='
    print 'Perdas ativas'
    print 'PandaPower: %.3f' %sum(p_p)
    print 'Menor Tensão:',
    print '%.3f' %case2.res_bus.vm_pu.loc[np.where(b == min(b))].values[0]
    print 'Na(s) barra(s):', 
    print case2.bus.name.loc[np.where(b == min(b))].values+1
    print '----'
    print 'BFSA: %.3f' %(case3['output']['sum_p_loss']*1000)
    print 'Menor tensão:',
    print '%.3f' %case3['output']['min_voltage'][1]
    print 'Na(s) barra(s):',
    print case3['output']['min_voltage'][0]

    print '===='

    print 'Tempo computacional'
    tm2 = (tfm2 - t0m2)
    tp = (tfp - t0p)
    print 'BFSA: %.4f segundos' %tm2
    print 'Pandapower: %.4f segundos' %tp
    return [tm2, tp]

#Novos casos
paths = [
    'case16new.mat',
    'case16new_best.mat',
    'case33.mat',
    #'case69.mat',
    'case70.mat',
    'case84new.mat',
    'case119.mat',
    'case135new.mat',
    'case135new_best.mat'
]
paths = [
    'case84new.mat'
]
print '===================================='
print '-------Testes em vários casos-------'
print '===================================='
meu = []
panda = []
for path in paths:
    pm, pt = test_time('cases/'+path)
    meu.append(pm)
    panda.append(pt)

tm_meu = sum(meu)/len(meu)
tm_panda = sum(panda)/len(panda)
print '===================================='
print '----------Tempo Médio Final---------'
print '===================================='
print 'Meu: %.4f' %tm_meu
print 'PP:  %.4f' %tm_panda
#path = 'cases/case84new.mat'
#case = p.load_mpc(path)              #Carrega um caso
#rnp = p.dp2rnp2(case)
#t0 = timer()
#case = p.run_pf2(case, rnp)
#print timer() - t0
#print case['output']['sum_p_loss']
#print case['output']['max_voltage']

