#!/usr/bin/python
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np

iterables = [
    ['Dispositivos']*2 + ['Localizacao']*4 + ['Lim. das ICCs']*4
  + ['Tempo do Protegido']*4 + ['Tempo do Protetor']*4 + ['CTI']*4,

    ['Protegido', 'Protetor'] + ['Protegido']*2 + ['Protetor']*2
  + ['Neutro']*2 + ['Fase']*2
  + ['Neutro']*2 + ['Fase']*2
  + ['Neutro']*2 + ['Fase']*2
  + ['Neutro']*2 + ['Fase']*2,

    ['', ''] + ['De', 'Para']*2
  + ['1f', '1fm', '3f', '2f']*2
  + ['1f', '1fm', '3f', '2f']*2
  + ['1f', '1fm', '3f', '2f']*2
  + ['1f', '1fm', '3f', '2f']*2
]
iterables = zip(*iterables)
index = pd.MultiIndex.from_tuples(iterables)

a = pd.DataFrame(np.random.rand(5, 22), columns=index)
a.loc[len(a)] = range(22)
a.to_excel('teste.xlsx')
