#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
PowerFlow with NDE
Vitor Batista
vitordsbatista@gmail.com
Python 2.7
"""

import os
from operator import itemgetter
import cti
import pickle as pk
import math
import pandas as pd
import cmath
import csv
import sys
import copy
import bfnd as bn
import timeit
from itertools import compress
from timeit import default_timer as timer
sys.path.insert(0, '../meta_bfnd')
import aemt

# Barra de falta
fault = 68
case_num = 135
# Lê o caso original
case_source = bn.load_case('case'+str(case_num)+'_source.csv', cc=True)
# Executa o PF e o CC para o caso original
rnp = bn.dp2rnp(case_source)
case_source = bn.run_pf(case_source, rnp)
for i in rnp:
    case_source = bn.icc_rnp(case_source, rnp=rnp[i])

# Lê a topologia
path = 'case'+str(case_num)+'/'
with open(path+'case.p', 'r') as pfile:
    top = pk.load(pfile)

# Executa o aemt
t0 = timer()
bests, ft, status_oc, best, indice_icc = aemt.aemt(case_source, fault, verbose=True, disp=top)
save_data_plot = [bests, ft, status_oc, best, indice_icc]
t1 = timer()
# Tempo de execução (em minutos)
tempo = (t1 - t0)/60
status = best['status']

# Caso ótimo
case = copy.deepcopy(case_source)
case['line']['status'] = status

rnp = bn.dp2rnp(case)

# Executa o PF e o CC
case = bn.run_pf(case, rnp)
for i in rnp:
    case = bn.icc_rnp(case, rnp=rnp[i])

#with open('rnp.p', 'r') as pfile:
    #rnp = pk.load(pfile)

#sys.exit()
#for i, j in zip(case['bus']['bus'], case['bus']['Icc']):
    #print i, j['3f'], j['1f'], j['1fm'], j['2f']
"""
# REMOVER #
print "|{:=^104}|".format('')
print "|{:^21}|{:^40}|{:^41}|".format('', 'Neutro', 'Fase')
print "|{:=^104}|".format('')
print "|{:^21}|{:^21}|{:^18}|{:^22}|{:^18}|".format(
    '', '',
    'TDS', '', 'TDS')
print "|{:^4}|{:^6}|{:^10}|{:^22}|{:^9}|{:^9}|{:^23}|{:^9}|{:^9}|".format(
    'De', 'Para', 'Fusível', 
    'Ajuste Instantâneo', 'Lento', 'Rápido',
    'Ajuste Instantâneo', 'Lento', 'Rápido')
print "|{:=^104}|".format('')
for i in sorted(top):
    # De - Para
    de, para = i
    disp = top[i]
    #print disp
    # Fusivel
    fus = disp['fus'] if disp.has_key('fus') else '-'
    # == Neutro == #
    # Ajuste instantâneo
    aiN = disp['ai_N'] if disp.has_key('ai_N') else '-'
    # Ipk
    ipkN = disp['Ipk_N'] if disp.has_key('Ipk_N') else '-'
    # TDS
    tdsN = disp['tds_N'] if disp.has_key('tds_N') else '-'
    # == Fase == #
    # Ajuste instantâneo
    aiF = disp['ai_F'] if disp.has_key('ai_F') else '-'
    # Ipk
    ipkF = disp['Ipk_F'] if disp.has_key('Ipk_F') else '-'
    # TDS
    tdsF = disp['tds_F'] if disp.has_key('tds_F') else '-'
    
    ""
    if aiN == '-':
        print '{:^4}{:^6}{:^9}{:^21}{:^9}{:^8}{:^22}{:^9}{:^8}'.format(
            de, para, fus, ipkN, tdsN, '-', ipkF, tdsF, '-'
        )
    else:
        print '{:^4}{:^6}{:^9}{:^9}{:^11}{:^9}{:^8}{:^10}{:^11}{:^9}{:^8}'.format(
            de, para, fus, ipkN, aiN, tdsN, '-', ipkF, aiF, tdsF, '-'
        )
    ""
    if aiN == '-':
        if type(ipkN) == float:
            print '|{:^4}|{:^6}|{:^9}|{:^21.4f}|{:^9.4f}|{:^8}|{:^22.4f}|{:^9.4f}|{:^8}|'.format(
                de, para, fus, ipkN, tdsN, '-', ipkF, tdsF, '-'
            )
        else:
            print '|{:^4}|{:^6}|{:^9}|{:^21}|{:^9}|{:^8}|{:^22}|{:^9}|{:^8}|'.format(
                de, para, fus, ipkN, tdsN, '-', ipkF, tdsF, '-'
            )
    else:
        print '|{:^4}|{:^6}|{:^9}|{:^9.4f}|{:^11.4f}|{:^9.4f}|{:^8}|{:^10.4f}|{:^11.4f}|{:^9.4f}|{:^8}|'.format(
            de, para, fus, ipkN, aiN, tdsN, '-', ipkF, aiF, tdsF, '-'
        )
    
#"""

def joinDP(dp, top):
    """
        Condiciona o dp aos trechos pertencentes da topologia
    """
    keys = top.keys()
    new_dp = []
    for i in dp:
        if i in keys:
            new_dp.append(i)
        elif i[::-1] in keys:
            new_dp.append(i)
    return new_dp
# REMOVER #
#sys.exit()
# Calculo do CTI
res = {}
res2 = {}
# Remove os trechos que não possuem dispositivos da topologia
tmp = dict(top)
for i in tmp:
    if tmp[i]['type'] == 1:
        del top[i]

tmp = dict()
tmp3 = dict()
#rnp = {9: rnp['9']}
orig_FT = [x for x, y in zip(case_source['line']['from_to'], case_source['line']['status']) if y == 1]
tie_keys = [x for x, y in zip(case_source['line']['from_to'], case_source['line']['status']) if y == 0]
orig_FT = case_source['line']['from_to']
zone_tmp = {}
for i in rnp:
    # Transforma a rnp num de-para
    dp = cti.rnp2dp(rnp[i])
    dpOnlyDisp = joinDP(dp, top)
    inver = cti.selInver(dp, orig_FT, tie_keys)
    # Gera as novas zonas do caso modificado
    zones_DISP, zones_ICC = cti.new_zones(top, dp, dpOnlyDisp, inver)
    zone_tmp[i] = copy.deepcopy(zones_DISP)
    tmp[i] = zones_ICC
    tmp3[i] = zones_DISP
    cti.expand_zone(top, zones_ICC, rnp[i], dpOnlyDisp)
    cti.expand_zone(top, zones_DISP, rnp[i], dpOnlyDisp)

    """
    tmp = dict(zones)
    for j in tmp:
        if not(j in top.keys()
               or j[::-1] in top.keys()):
            del zones[j]

    # De cada zona dos dispositivos, remove os trechos que não possuem dispositivos
    for j in zones:
        tmp = []
        for k in tmp:
            if (k in top.keys()
                or k[::-1] in top.keys()):
                tmp.append(k)
        zones[j] = tmp

    if i == '6':
        print zones[(105, 106)]
    ================================
    """
    # Iccs
    buses = [x[0] for x in rnp[i]]
    buses_id = [[x for x in case['bus']['bus']].index(y) for y in buses]
    Icc = {}
    for idx, b in zip(buses_id, buses):
        Icc[b] = case['bus']['Icc'][idx]

    # Dados gerais do cti
    cti_dict = {
        'fus-fus': 0.2,
        'relig-fus': 0.2,
        'rele-fus': 0.2,
        'fus-relig': 0.2,
        'relig-relig': 0.2,
        'rele-relig': 0.2
    }
    # Varre a topologia, utilizando o rnp, para calcular o cti
    out = cti.calc_cti(top, case, dpOnlyDisp, zones_ICC, zones_DISP, 
                               inver, Icc, tcr=1, ccr=1)
    res[int(i)] = out[:1]
    res2[int(i)] = out[2]
"""
# TODO: Comparar os resultados com os do Andrey
# TODO: Talvez o primeiro alimentador não esteja funcionando
tmp2 = tmp
tmp4 = dict()
tmp = dict()
for i in tmp2:
    tmp[int(i)] = tmp2[i]
    tmp4[int(i)] = tmp3[i]

for i in sorted(tmp):
    for j in sorted(tmp[i]):
        #if not tmp[i][j] == tmp4[i][j]:
        #print j, tmp[i][j], tmp4[i][j]
        pass
with open('zona.txt', 'w') as f:
    for i in sorted(zone_tmp):
        for j in sorted(zone_tmp[i]):
            w = list(j) + zone_tmp[i][j]
            for k in w:
                print >> f, k,
            print >> f, '\n'
            #f.write('\n'.join(w))
            #f.write("\n")
"""

def table1(res):
    disp = {
        2: 'Rele',
        3: 'Religador',
        5: 'Fusivel'
    }
    iterables = [
        ['Dispositivos']*2 + ['Localizacao']*4 + ['Lim. das ICCs']*4
      + ['Tempo do Protegido']*4 + ['Tempo do Protetor']*4 + ['CTI']*4,

        ['Protegido', 'Protetor'] + ['Protegido']*2 + ['Protetor']*2
      + ['Neutro']*2 + ['Fase']*2
      + ['Neutro']*2 + ['Fase']*2
      + ['Neutro']*2 + ['Fase']*2
      + ['Neutro']*2 + ['Fase']*2,

        ['', ''] + ['De', 'Para']*2
      + ['1f', '1fm', '3f', '2f']*2
      + ['1f', '1fm', '3f', '2f']*2
      + ['1f', '1fm', '3f', '2f']*2
      + ['1f', '1fm', '3f', '2f']*2
    ]
    iterables = zip(*iterables)
    index = pd.MultiIndex.from_tuples(iterables)
    table = pd.DataFrame(columns=index)

    for n in res:
        data = res[n][0]
        for i in data:
            protegido_tipo = disp[data[i]['tipo'][0]]
            protetor_tipo = disp[data[i]['tipo'][1]]
            # De-Para
            protegido_de = i[0][0]
            protegido_para = i[0][1]
            protetor_de = i[1][0]
            protetor_para = i[1][1]
            # Limite das correntes
            icc_1f = data[i]['Icc']['1f'][0]
            icc_1fm = data[i]['Icc']['1fm'][0]
            icc_3f = data[i]['Icc']['3f'][0]
            icc_2f = data[i]['Icc']['2f'][0]
            # Tempo do protegido
            time_d1_3f, time_d1_2f, time_d1_1f, time_d1_1fm = data[i]['tempo']['d1']
            # Tempo do protetor
            time_d2_3f, time_d2_2f, time_d2_1f, time_d2_1fm = data[i]['tempo']['d2']
            # CTI
            cti_3f, cti_2f, cti_1f, cti_1fm = data[i]['tempo']['cti']
            # Adiciona os dados na tabela
            table.loc[len(table)] = [
                protegido_tipo, protetor_tipo,
                protegido_de, protegido_para, protetor_de, protetor_para,
                icc_1f, icc_1fm, icc_3f, icc_2f,
                time_d1_1f, time_d1_1fm, time_d1_3f, time_d1_2f,
                time_d2_1f, time_d2_1fm, time_d2_3f, time_d2_2f,
                cti_1f, cti_1fm, cti_3f, cti_2f
            ]

    #table.to_excel(name)
    return table

def table2(res, top, case, case_source):
    # Junta todas as atuações numa só
    atuacao = {}
    for i in res:
        atuacao.update(res[i])
    # De-para
    dePara = atuacao.keys()
    dePara = sorted(dePara)
    de = [x[0] for x in dePara]
    para = [x[1] for x in dePara]
    # Dados com todas as informações
    data = list()

    # Criação dos dados
    for i in dePara:
        # Tipo
        if i in top:
            tmp = i
        else:
            tmp = i[::-1]
        tipo = top[i]['type']

        # Operação
        if tipo == 5:
            operacao = atuacao[i]['atuacao'] + ['-']
        else:
            operacao = atuacao[i]['atuacao'][::-1]

        # Ajuste
        if tipo == 5:
            ajuste = [str(top[i]['fus'])+'K', '-']
        else:
            ajuste = [top[i]['Ipk_N'],
                      top[i]['Ipk_F']
                     ]

        # Corrente Antes
        if i in case_source['line']['from_to']:
            ix = case_source['line']['from_to'].index(i)
        else:
            ix = case_source['line']['from_to'].index(i[::-1])
        I_antes = case_source['line']['I'][ix]
        
        # Corrente Após
        if i in case['line']['from_to']:
            ix = case['line']['from_to'].index(i)
        else:
            ix = case['line']['from_to'].index(i[::-1])
        I_apos = case['line']['I'][ix]

        data.append({
            'dePara': i,
            'operacao': operacao,
            'ajuste': ajuste,
            'I_antes': I_antes,
            'I_apos': I_apos,
            'tipo': tipo
        })
    # Cabeçalho
    head = ['DP_Alimentador', 
            'De',
            'Para',
            'I_Nom_Antes(A)',
            'I_Nom_Apos(A)',
            'Ajuste(A)_Fase',
            'Operacao_Fase',
            'Ajuste(A)_Neutro',
            'Operacao_Neutro']

    disp_name = {
        2: 'Rele',
        3: 'Religador',
        5: 'Fusivel'
    }

    # Ordena o data
    data = sorted(data, key=itemgetter('tipo', 'operacao'))

    columns = ['DP_Alimentador', 'De', 'Para', 
               'I Nom. Antes (A)', 'I Nom. Apos (A)', 
               'Ajuste de Neutro (A)', 'Operacao de Neutro',
               'Ajuste de Fase (A)', 'Operacao de Fase']

    table = pd.DataFrame(columns=columns)
    for disp in data:
        table.loc[len(table)] = [
            disp_name[disp['tipo']],
            disp['dePara'][0],
            disp['dePara'][1],
            disp['I_antes'],
            disp['I_apos'],
            disp['ajuste'][0],
            disp['operacao'][0],
            disp['ajuste'][1],
            disp['operacao'][1]
        ]
    return table

def table3(best, fault):
    # Outros dados para serem escritos
    # tempo
    nm = best['fit']['nm']
    bus_min_v, min_v = best['fit']['min_v']
    line_load = best['fit']['line_load'] * 100
    sub_load = best['fit']['sub_load'] * 100
    table = pd.DataFrame(
        columns=['Falta', 'N. de cahveamentos', 
                 'Tensao Minima (pu) - Barra', 
                 'Max. Carr. trechos (%)',
                 'Max. Carr. subestacao (%)',
                 'Tempo (minutos)'
                ]
    )
    
    table.loc[len(table)] = [fault, nm, 
                            '{:.4f} - {:}'.format(min_v, bus_min_v[0]),
                            line_load, sub_load, tempo]
    return table

def table4(best):
    pm = best['pm']
    table = pd.DataFrame(
        columns=['Isolamento', 'Restauracao']
    )
    isolamento = []
    restauracao = []
    for i in pm:
        if i.find(str(fault)) != -1:
            isolamento.append(i)
        else:
            restauracao.append(i)
    t1 = pd.DataFrame(isolamento)
    t2 = pd.DataFrame(restauracao)
    table = pd.concat([t1, t2], ignore_index=True, axis=1)
    table.columns = ['Isolamento', 'Restauracao']
    return table

df1 = table1(res)
df2 = table2(res2, top, case, case_source)
df3 = table3(best, fault)
df4 = table4(best)

#print df1
#print df2
#print df3
#print df4

#sys.exit()
with pd.ExcelWriter('execucoes/case'+str(case_num)+'_f'+str(fault)+'.xlsx') as writer:
    df1.to_excel(writer, sheet_name='CTI')
    df2.to_excel(writer, sheet_name='Dados dos Dispositivos')
    df3.to_excel(writer, sheet_name='Dados Restauracao')
    df4.to_excel(writer, sheet_name='Plano de Manobras')


with open('execucoes/dada_plot_'+str(case_num)+'_'+str(fault)+'.p', 'w') as pfile:
    pk.dump(save_data_plot, pfile)
