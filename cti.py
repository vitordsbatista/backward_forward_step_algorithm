    #!/usr/bin/python
# -*- coding: utf-8 -*-
# TODO:
#   Penalidade caso não seja possível calcular o valor co CTI

"""
PowerFlow with NDE
Vitor Batista
vitordsbatista@gmail.com
Python 2.7
"""

import csv
import sys
import matplotlib.pyplot as plt
import math

def read_fus_K(fus):
    """
        Carrega os dados de um fusível do tipo K
        É necessário que exista uma pasta com os dados do fusível disponível
    na raiz deste arquivo.

        Parâmetros:
            fus(int) -- tipo de fusível (6, 10, 15, 25, 40, 65, 100, 140, 200)
        Saída:
           [min_curve, max_curve](list) -- lista com as curvas mínimas e totais
        (máximas) do fusível
    """
    min_curve = []
    max_curve = []
    with open('fus/Fusivel_%i_K_Min.txt' % fus) as f:        # Curva mínima
        for i in f:
            tmp = i.split()
            tmp = map(float, tmp)
            min_curve.append(tmp)
    with open('fus/Fusivel_%i_K_Tot.txt' % fus) as f:        # Curva máxima
        for i in f:
            tmp = i.split()
            tmp = map(float, tmp)
            max_curve.append(tmp)
    return min_curve, max_curve

def frange_generator(s1, s2=None, step=1.0):
    # Use float number in range() function
    if s2:
        start = float(s1)
        stop = float(s2)
    else:
        start = 0.0
        stop = float(s1)
    while True:
        if step > 0.0 and start >= stop:
            break
        elif step < 0.0 and start <= stop:
            break
        yield ("%g" % start)  # return float number
        start = start + step

def frange(s1, s2, s3):
    tmp = [float(i) for i in frange_generator(s1, s2, s3)]
    return tmp

def i_in_curve(curve, I):
    a = [curve[0][0], curve[-1][0]]
    min_I = min(a)
    max_I = max(a)
    if I < min_I:
        return False, float('inf')
    elif I > max_I:
        return False, 0
    return True, 1

def time_fus(curve, i):
    Icc = float(i)
    # Curva no formato [corrente, tempo]
    current = []
    time = []
    for i, j in curve:
        current.append(i)
        time.append(j)

    if Icc in current:
        t = time[current.index(Icc)]
    else:
        for n, i in enumerate(current):
            if i > Icc:
                # Corrente anteriot (before) e posterior (after)
                Ib, Ia = current[n+1], current[n]
                Tb = time[current.index(Ib)]
                Ta = time[current.index(Ia)]
        # Tempo para a corrente I
        t = (Icc - Ia)*(Tb - Ta)/(Ib - Ia) + Ta
    return t

def time_reli(tds, ipk, i):
    tds = float(tds)
    ipk = float(ipk)
    Icc = float(i)
    A = 1.68546
    B = 0.158114
    C = 0.436523
    p = 1.78873
    out = tds * (A/(math.pow(Icc/ipk, p) - C) + B)
    return out

def time_rele(tds, ipk, tcr, ccr, I):
    tds = float(tds)
    ipk = float(ipk)
    Icc = float(I)
    if tcr == 1:  # IEC
        if ccr == 1:  # Inversa
            A = 0.14
            p = 0.02
            Q = 1
            B = 0
            K = 0
        elif ccr == 2:  # Muito Inversa
            A = 13.5
            p = 1
            Q = 1
            B = 0
            K = 0
        elif ccr == 3:  # Extremamente Inversa
            A = 80
            p = 2
            Q = 1
            B = 0
            K = 0
        out = (A*tds)/((Icc/ipk)**p-Q) + B * tds + K
    elif tcr == 2:  # ANSI
        if ccr == 1:  # Inversa
            A = 0.0274
            B = 2.2614
            C = 0.3
            D = -4.1899
            E = 9.1272
        elif ccr == 2:  # Muito Inversa
            A = 0.0615
            B = 0.7989
            C = 0.34
            D = -0.284
            E = 4.0505
        elif ccr == 3:  # Extremamentre Inversa
            A = 0.0399
            B = 0.2294
            C = 0.5
            D = 3.0094
            E = 0.7222
        out = tds*(A+B/((Icc/ipk)-C)+D/((Icc/ipk)-C)**2+E/((Icc/ipk)-C)**3)
    elif tcr == 3:  # IAC
        if ccr == 1:  # Inversa
            A = 0.2078
            B = 0.863
            C = 0.8
            D = -0.418
            E = 0.1947
        elif ccr == 2:  # Muito Inversa
            A = 0.09
            B = 0.7955
            C = 0.1
            D = -1.2885
            E = 7.9586
        elif ccr == 3:  # Extremtamente Inversa
            A = 0.004
            B = 0.6379
            C = 0.62
            D = 1.7872
            E = 0.2461
        out = tds*(A+B/((Icc/ipk)-C)+D/((Icc/ipk)-C)**2+E/((Icc/ipk)-C)**3)
    return out

def drange(start, stop, step=1):
    n = int(round((stop - start)/float(step)))
    if n > 1:
        return([start + step*i for i in range(n+1)])
    elif n == 1:
        return([start])
    else:
        return([])

def plot_disp(disp, parm, color='c'):
    I_end = 15000
    step = 0.01
    if disp == 5:
        parm = parm[0]
        curve_min, curve_tot = read_fus_K(parm)
        I_min = []
        t_min = []
        I_tot = []
        t_tot = []
        for i, j in curve_min:
            I_min.append(i)
            t_min.append(j)
        for i, j in curve_tot:
            I_tot.append(i)
            t_tot.append(j)
        plt.loglog(I_min, t_min, '--', color=color)
        plt.loglog(I_tot, t_tot, '-', color=color)
    if disp == 3:
        ipk = parm[0]
        tds = parm[1]
        I = drange(ipk, I_end, 0.01)
        t = [time_reli(tds, ipk, i) for i in I]
        plt.loglog(I, t, color=color)
    if disp == 2:
        ipk = parm[0]
        tds = parm[1]
        tcr = parm[2]
        ccr = parm[3]
        ai = parm[4]
        ipk_plot = ipk * 1.01
        I = frange(ipk_plot, ai, 0.5)
        t = [time_rele(tds, ipk, tcr, ccr, i) for i in I]
        plt.loglog(I, t, color=color)
        # Plot do AI
        plt.loglog([ai, ai],
                   [t[-1], 0.0001],
                   color=color)                             # Linha horizontal
        plt.loglog([ai, I_end],
                   [0.0001, 0.0001],
                   color=color)                             # Linha vertical
        # TODO: Definir os limites da figura

def leitura(case):
    top = {}
    with open('top{}/top.txt'.format(case), 'r') as csvfile:
        csvfile.next()
        for i in csvfile:
            tmp = i.split()
            #tmp = i.split(',')
            key = (
                int(tmp[0]),
                int(tmp[1])
            )
            ty = tmp[2]
            if ty == '1':                           # Vazio
                top[key] = {
                    'type': int(ty)
                }
            elif ty == '2':                         # Relé
                top[key] = {
                    'type':  int(ty),
                    'Ipk_N': float(tmp[4]),
                    'tds_N': float(tmp[5]),
                    'ai_N':  float(tmp[8]),
                    'Ipk_F': float(tmp[6]),
                    'tds_F': float(tmp[7]),
                    'ai_F':  float(tmp[9])
                }
            elif ty == '3':                         # Religador
                top[key] = {
                    'type':  int(ty),
                    'Ipk_N': float(tmp[4]),
                    'tds_N': float(tmp[5]),
                    'Ipk_F': float(tmp[6]),
                    'tds_F': float(tmp[7])
                }
            elif ty == '5':                         # Fusível
                top[key] = {
                    'type': int(ty),
                    'fus':  int(tmp[3])
                }

    with open('top{}/zona.txt'.format(case), 'r') as csvfile:
        for i in csvfile:
            tmp = i.split()
            key = (
                int(tmp[0]),
                int(tmp[1])
            )
            if tmp[2] != '1':
                zone = map(int, tmp[3:])
                zone = [x for x in zone if x != 0]      # Remove zeros
                top[key]['zone'] = zone

    Icc = {}
    with open('top{}/icc.txt'.format(case) % top, 'r') as csvfile:
        csvfile.next()
        for i in csvfile:
            #tmp = i.split(',')
            tmp = i.split()
            node = int(tmp[0])
            Icc[node] = {
                '3f': float(tmp[1]),
                '1f': float(tmp[2]),
                '1fm': float(tmp[3]),
                '2f': float(tmp[4])
            }
    return top, Icc

def dfs(graph, start):
    """
        Realiza uma busca em profundidade num grafo

        Parâmetros:
            graph   -- dicionário com o grafo
            start   -- nó de início da busca
        Saída:
            visited -- nós visitados pela busca
    """
    visited, stack = [], [[start, 1]]
    while stack:
        vertex = stack.pop()
        if vertex not in visited:
            visited.append(vertex)
            if vertex[0] in graph.keys():
                tmp = set([i[0] for i in visited]).intersection(graph[
                    vertex[0]]).symmetric_difference(graph[vertex[0]]),
                for i in tmp[0]:
                    stack.append([i, vertex[1]+1])
    visited = map(list, zip(*visited))              # Transposição da rnp
    return visited

def dp2rnp(dePara, roots):
    """
        Gera a floresta com todas as rnp's da rede

        Parâmetros:
            dePara   -- lista com o grafo
            roots    -- lista com os nós raízes
        Saída:
            floresta -- rnps num dicionário
    """
    paraDe = [(j, i) for i, j in dePara]
    # Transforma o De-Para num grafo
    dp = dict()
    for i in dePara:
        if dp.has_key(i[0]):
            dp[i[0]].append(i[1])
        else:
            dp[i[0]] = [i[1]]
    for i in paraDe:
        if dp.has_key(i[0]):
            dp[i[0]].append(i[1])
        else:
            dp[i[0]] = [i[1]]
    floresta = dict()
    for n, i in enumerate(roots):  # For com todas as raizes para gerar as rnps
        tmp = dfs(dp, i)
        floresta[str(n)] = tmp
    return floresta

def rnp2dp(rnp):
    """
        Transforma uma rnp num de-para

        Parâmetros:
            rnp     -- representação nó profundidade
        Saída:
            dePara  -- de-para
    """
    dePara = []
    # Inverte a RNP
    rnp = rnp[::-1]
    for n, (i, j) in enumerate(rnp):
        for m, (k, o) in enumerate(rnp[n+1:]):
            if o == j-1:
                dePara.append((k, i))
                break
    """
    rnp = [rnp[0][::-1], rnp[1][::-1]]          #Inverte a rnp
    for n, (i, j) in enumerate(zip(*rnp)):
        for m, (k, l) in enumerate(zip(*rnp)[n+1:]):
            if l == j-1:
                dePara.append((k, i))
                break
    """
    return dePara

def expand_zone(top, zones, rnp, dp):
    """
        Verifica quais são os nós e o de-para dentro de uma determinada zona
    """
    #for tree in forest:
        #rnp = forest[tree]
    rnp_zip = zip(*rnp)
    for i in dp:
        origin = [x[0] for x in rnp].index(i[0])
        path_nodes = []
        path_ft = []
        for j in zones[i]:
            target = [x[0] for x in rnp].index(j)
            depth = rnp[target][1]
            rnp_tmp = rnp[origin:target+1]      # Splita a rnp
            # Remove os nós que não pertencem ao caminho
            for k in rnp_tmp[-2::-1]:
                if k[1] >= depth:
                    rnp_tmp.remove(k)
                else:
                    depth = k[1]
            path_nodes += [x[0] for x in rnp_tmp]
            path_ft += rnp2dp(rnp_tmp)
        # Armazena o caminho e os nós presentes nele
        path_ft = list(set(path_ft))
        #new_zones[i]['zone_nodes'] = list(set(path_nodes))
        zones[i] = list(set(path_ft))

def cti(d1, d2, I, path_fus=None):
    """
    Calcula o CTI entre dois dispositivos
        Fusivel:
            [5, curve]
        Religador:
            [3, IPK, TDS]       #Fusivel
            [3, IPK, TDS]       #Rele
        Rele:
            [2, IPK, TDS, tcr, ccr]
    """
    # Calculo do cti
    if d2[0] == 5:                                              # Fusível
        curve_tot_d2 = read_fus_K(d2[1])[1]                     # Curva do d2
        # Verifica se a corrente I está no
        #  intervalo das curvas do fusível
        out, time_tot_d2 = i_in_curve(curve_tot_d2, I)
        if out:
            # Tempo de atuação dos dispositivos na corrente I
            time_tot_d2 = time_fus(curve_tot_d2, I)

        # Seleciona o dispositivo 2 e calcula o tempo de atuação para I
        if d1[0] == 5:
            # ================Fusível-Fusível================ #
            curve_min_d1 = read_fus_K(d1[1])[0]                 # Curva do d1
            # Verifica se a corrente I está no
            # intervalo das curvas do fusível
            out, time_min_d1 = i_in_curve(curve_min_d1, I)
            if out:
                # Tempo de atuação dos dispositivos na corrente I
                time_min_d1 = time_fus(curve_min_d1, I)
            # Diferença dos tempos entre os dispositivos
            # TODO: será incluido 75% do protegido (d1)
            diff_time = (time_min_d1 - time_tot_d2)
            time = [time_min_d1, time_tot_d2]
        elif d1[0] == 3:
            # ===============Religador-Fusível=============== #
            # Dados do religador
            ipk_reli = d1[1]                                    # IPk
            tds_lento_reli = d1[2]                              # TDS lento

            # Verifica se I está no intervalo das curvas do religador
            if I < ipk_reli:
                time_lento_reli = float('inf')
            else:
                time_lento_reli = time_reli(tds_lento_reli,
                                            ipk_reli, I)            # Tempo lento

            # Diferença dos tempos entre os dispositivos
            diff_time = (time_lento_reli - time_tot_d2)
            time = [time_lento_reli, time_tot_d2]

        elif d1[0] == 2:
            # =================Relé-Fusível================= #
            # Dados do relé
            ipk_rele = d1[1]                            # IPk
            tds_rele = d1[2]                            # TDS
            tcr = d1[3]                                 # TCR
            ccr = d1[4]                                 # CCR
            ai = d1[5]                                  # Ajuste instantâneo

            # Verifica se a corrente I está no intervalo das curvas do relé
            #print 'I', I
            #print 'AI', ai
            # Tempo do relé
            if I < ipk_rele:
                time_rele_d1 = float('inf')
            elif I-0.01 > ai:                            # Caso ultrapasse o ai
                time_rele_d1 = 0
            else:
                time_rele_d1 = time_rele(tds_rele, ipk_rele,
                                         tcr, ccr, I)           # Tempo lento
            # Diferença dos tempos entre os dispositivos
            diff_time = (time_rele_d1 - time_tot_d2)
            time = [time_rele_d1, time_tot_d2]
        else:
            # TODO: ERRO
            pass

    elif d2[0] == 3:                            # Religador
        # Dados do religador
        ipk_reli_d2 = d2[1]                     # IPk
        tds_lento_reli_d2 = d2[2]               # TDS lento

        # Verifica se a corrente I está no intervalo das curvas do religador
        if I < ipk_reli_d2:
            time_lento_reli_d2 = float('inf')
        else:
            time_lento_reli_d2 = time_reli(tds_lento_reli_d2,
                                           ipk_reli_d2, I)          # Tempo lento

        # Seleciona o dispositivo 1 e calcula o tempo de atuação para I
        if d1[0] == 3:
            # ===============Religador-Religador=============== #
            # Dados do religador
            ipk_reli_d1 = d1[1]                                 # IPk
            tds_lento_reli_d1 = d1[2]                           # TDS lento

            # Verifica se I está no intervalo das curvas do religador
            if I < ipk_reli_d1:
                time_lento_reli_d1 = float('inf')
            else:
                time_lento_reli_d1 = time_reli(tds_lento_reli_d1,
                                               ipk_reli_d1, I)      # Tempo lento

            # Diferença dos tempos entre os dispositivos
            diff_time = (time_lento_reli_d1 - time_lento_reli_d2)
            time = [time_lento_reli_d1, time_lento_reli_d2]

        elif d1[0] == 2:                                        # Relé-Fusível
            # ===============Relé-Religador=============== #
            # Dados do relé
            ipk_rele = d1[1]                                    # IPk
            tds_rele = d1[2]                                    # TDS
            tcr = d1[3]                                         # tcr
            ccr = d1[4]                                         # ccr
            ai = d1[5]                                 # Ajuste instantâneo
            # Verifica se a corrente I está no intervalo das curvas do relé
            if I < ipk_rele:
                time_rele_d1 = float('inf')
            # Tempo do relé
            if I-0.01 > ai:                          # Caso ultrapasse o ai
                time_rele_d1 = 0
            else:
                time_rele_d1 = time_rele(tds_rele, ipk_rele,
                                         tcr, ccr, I)           # Tempo lento
            # Diferença dos tempos entre os dispositivos
            diff_time = (time_rele_d1 - time_lento_reli_d2)
            time = [time_rele_d1, time_lento_reli_d2]

        elif d1[0] == 5:
            # ================Fusível-Fusível================ #
            curve_min_d1 = read_fus_K(d1[1])[0]                 # Curva do d1
            # Verifica se a corrente I está no
            #  intervalo das curvas do fusível
            out, time_min_d1 = i_in_curve(curve_min_d1, I)
            if out:
                # Tempo de atuação dos dispositivos na corrente I
                time_min_d1 = time_fus(curve_min_d1, I)
            # Diferença dos tempos entre os dispositivos
            # TODO: será incluido 75% do protegido (d1)
            diff_time = (time_min_d1 - time_lento_reli_d2)
            time = [time_min_d1, time_lento_reli_d2]
        else:
            # ERRO
            pass
    else:
        # ERRO
        pass
    return [diff_time, time]

def calc_cti(top, case, dp, zone, zone_disp, inver, Icc, tcr, ccr):
    cti_tot_F = 0
    cti_tot_N = 0

    # Seleciona os pares para a verificação da seletividade
    pares = []
    for protegido in dp:
        for protetor in zone_disp[protegido]:
            if protegido != protetor:
                if protetor in dp:
                    pares.append((protegido, protetor))

    # Variável de saída de dados
    output = dict()
    output2 = dict()

    for protegido, protetor in pares:
        # Menor ICC da interseção das zonas do protetor e protegido
        protegido_zona = set(zone[protegido])
        protetor_zona = set(zone[protetor])
        inter_zona = list(protetor_zona.intersection(protegido_zona))
        # TODO melhorar isso, pq a condição do fus-fus não vai utilizar essas correntes
        # as alterações foram realizadas posteriormente
        # Bifásico
        Icc_2f = [Icc[x[1]]['2f'] for x in inter_zona]
        # Fase-Tera mínimo
        Icc_1fm = [Icc[x[1]]['1fm'] for x in inter_zona]
        # Valores mínimos
        Icc_2f = min(Icc_2f)
        Icc_1fm = min(Icc_1fm)

        # Volta a forma original para pegar os dados da topologia
        if protetor in inver: protetor = protetor[::-1]
        if protegido in inver: protegido = protegido[::-1]
        
        # Dados de saída
        #print protegido, protetor
        tipo = [top[protegido]['type'], top[protetor]['type']]
        inter_zona_pontos = [x[1] for x in inter_zona]
        # Encontra as correntes de CC que servirão de base pro CTI # Em cima do protetor
        Icc_3f = Icc[protetor[0]]['3f']      # Trifásico
        Icc_1f = Icc[protetor[0]]['1f']      # Fase-Terra

        # Dados dos dispositivos de acordo com o tipo
        #  Protegido
        if top[protegido]['type'] == 5:             # Fusível
            d1 = [
                [5, top[protegido]['fus']],
                [5, top[protegido]['fus']]
            ]
        elif top[protegido]['type'] == 3:           # Religador
        
            fase = [
                3, top[protegido]['Ipk_F'],
                top[protegido]['tds_F']
            ]
            neutro = [
                3, top[protegido]['Ipk_N'],
                top[protegido]['tds_N']
            ]
            d1 = [fase, neutro]
        elif top[protegido]['type'] == 2:           # Relé
            fase = [
                2, top[protegido]['Ipk_F'], top[protegido]['tds_F'],
                tcr, ccr, top[protegido]['ai_F']
            ]
            neutro = [
                2, top[protegido]['Ipk_N'], top[protegido]['tds_N'],
                tcr, ccr, top[protegido]['ai_N']
            ]
            d1 = [fase, neutro]

        #  Protetor
        if top[protetor]['type'] == 5:              # Fusível
            d2 = [
                [5, top[protetor]['fus']],
                [5, top[protetor]['fus']]
            ]
        elif top[protetor]['type'] == 3:            # Religador
            fase = [
                3, top[protetor]['Ipk_F'],
                top[protetor]['tds_F']
            ]
            neutro = [
                3, top[protetor]['Ipk_N'],
                top[protetor]['tds_N']
            ]
            d2 = [fase, neutro]
        elif top[protetor]['type'] == 2:            # Relé
            fase = [
                2, top[protetor]['Ipk_F'], top[protetor]['tds_F'],
                tcr, ccr, top[protetor]['ai_F']
            ]
            neutro = [
                2, top[protetor]['Ipk_N'], top[protetor]['tds_N'],
                tcr, ccr, top[protetor]['ai_N']
            ]
            d2 = [fase, neutro]

        # POG Alterações do calculo para fus-fus
        if d1[0][0] == 5 and d2[0][0] == 5:
            Icc_2f = Icc[protetor[0]]['2f']         # Bifásico
            Icc_1fm = Icc[protetor[0]]['1fm']       # Fase-Terra-Mínimo

        # Calcula os CTIs Fase
        #print protegido, protetor
        cti_F_max, time_F_max = cti(d1[0], d2[0], Icc_3f)
        cti_F_min, time_F_min = cti(d1[0], d2[0], Icc_2f)
        cti_N_max, time_N_max = cti(d1[1], d2[1], Icc_1f)
        cti_N_min, time_N_min = cti(d1[1], d2[1], Icc_1fm)

        #print protegido, protetor
        #print time_F_max
        tempo = {
            'd1': [time_F_max[0], 
                   time_F_min[0], 
                   time_N_max[0], 
                   time_N_min[0]],
            'd2': [time_F_max[1], 
                   time_F_min[1], 
                   time_N_max[1], 
                   time_N_min[1]],
            'cti': [cti_F_max,
                    cti_F_min,
                    cti_N_max,
                    cti_N_min]
        }
        # CTI Fase
        cti_tot_F += cti_F_max               # Tempo total
        cti_tot_F += cti_F_min
        # CTI Neutro
        cti_tot_N += cti_N_max               # Tempo total
        cti_tot_N += cti_N_min

        # Verifica se o dispositivo atuou
        atuProtegido = atuacao(d1, case, protegido)
        atuProtetor  = atuacao(d2, case, protetor)
        #print atuProtegido
        #print atuProtetor

        # Armazena os dados
        output[(protegido, protetor)] = {
            'Icc': {
                '3f':  [Icc_3f, protetor[0]],
                '2f':  [Icc_2f, inter_zona_pontos],
                '1f':  [Icc_1f, protetor[0]],
                '1fm': [Icc_1fm, inter_zona_pontos]
            },
            'tipo': tipo,
            'tempo': tempo,
            'atuacao': [atuProtegido, atuProtetor]
        }

        # Dados secundários
        output2[protegido] = {'atuacao': atuProtegido}
        output2[protetor] = {'atuacao': atuProtetor}
        #print tipo[0], protegido, atuProtegido, case['line']['I'][case['line']['from_to'].index(protegido)]
        #print tipo[1], protetor, atuProtetor, case['line']['I'][case['line']['from_to'].index(protetor)]

        #print '================================'

        """
        plot_disp(d1[0][0], d1[0][1:], 'b')
        plot_disp(d2[0][0], d2[0][1:], 'r')
        plt.plot([Icc_3f, Icc_3f], [0, 1000], color='k')
        plt.plot([Icc_2f, Icc_2f], [0, 1000], color='k')
        plt.plot([Icc_1f, Icc_1f], [0, 1000], color='y')
        plt.plot([Icc_1fm, Icc_1fm], [0, 1000], color='y')
        plt.show()
        print protegido
        print 33
        sys.exit()
        """

    cti_tot = cti_tot_F + cti_tot_N
    return output, cti_tot, output2

def atuacao(disp, case, dp):
    idx = case['line']['from_to'].index(dp)
    I = case['line']['I'][idx]
    # Fusível
    if disp[0][0] == 5:
        fus_type = disp[0][1]
        if fus_type * 1.5 <= I:
            return ['Sim']
        else:
            return ['Nao']
    # Relé e religador
    else:
        Ipk_F = disp[0][1]
        Ipk_N = disp[1][1]
        """
        print dp
        print '=='
        print 'fase', Ipk_F
        print 'neutro', Ipk_N
        print '=='
        """
        if I >= Ipk_F:
            fase = 'Sim'
        else:
            fase = 'Nao'
        if I * 0.15 >= Ipk_N:
            neutro = 'Sim'
        else:
            neutro = 'Nao'
        return [fase, neutro]

def print_cti2(res):
    # Verbose
    if verbose:
        fase_out = []
        neutro_out = []
        disp_type = {
            2: 'Rele',
            3: 'Religador',
            5: 'Fusivel'
        }

        # Verbose
        # Lista com os valores de cada dispositivo
        # td1, pd1, td2, pd2, IccMax, IccMin,
        # timeMaxd1, timeMind1, timeMaxd2, timeMind2,
        # ctiMax, ctiMin
        """
            Icc_3f
            Icc_2f
            Icc_1f
            Icc_1fm
        """
        if verbose:
            # FASE
            time_d1_max_F, time_d2_max_F = tmp_F_max[2]
            time_d1_min_F, time_d2_min_F = tmp_F_min[2]
            cti_max_F = tmp_F_max[0]
            cti_min_F = tmp_F_min[0]
            type_d1 = top[protegido]['type']
            type_d2 = top[protetor]['type']
            fase_out.append([
                type_d1, protegido, type_d2, protetor,
                Icc_3f, Icc_2f, time_d1_max_F, time_d1_min_F,
                time_d2_max_F, time_d2_min_F, cti_max_F, cti_min_F
            ])
            # NEUTRO
            time_d1_max_N, time_d2_max_N = tmp_N_max[2]
            time_d1_min_N, time_d2_min_N = tmp_N_min[2]
            cti_max_N = tmp_N_max[0]
            cti_min_N = tmp_N_min[0]
            type_d1 = top[protegido]['type']
            type_d2 = top[protetor]['type']
            neutro_out.append([
                type_d1, protegido, type_d2, protetor,
                Icc_1f, Icc_1fm, time_d1_max_N, time_d1_min_N,
                time_d2_max_N, time_d2_min_N, cti_max_N, cti_min_N
            ])
    if verbose:
        #  FASE
        print '{:=^105}'.format('FASE')
        # Cabeçalho
        print '{:>52}{:>33}'.format('Icc', 'Tempo de Operação')
        print '{:^42}{:^10}{:^5}{:>14}{:>16}{:>13}'.format('Dispositivos', '3f', '2f', 'Protetor', 'Protegido', 'CTI')
        print '{:->105}'.format('')
        for i in fase_out:
            print '{:^10}-{:^9} {:^10}-{:^9} | {:<6.1f} {:6.1f} | {:5.4f} {:5.4f} | {:5.4f} {:5.4f} | {:5.4f} {:5.4f}'.format(
                disp_type[i[0]], i[1], disp_type[i[2]], i[3], 
                i[4], i[5], i[6], i[7], i[8], i[9],
                i[10], i[11]
            )
        print 
        #  NEUTRO
        print '{:=^105}'.format('NEUTRO')
        # Cabeçalho
        print '{:>52}{:>33}'.format('Icc', 'Tempo de Operação')
        print '{:^42}{:^10}{:^5}{:>14}{:>16}{:>13}'.format('Dispositivos', '1f', '1fm', 'Protetor', 'Protegido', 'CTI')
        print '{:->105}'.format('')
        for i in neutro_out:
            print '{:^10}-{:^9} {:^10}-{:^9} | {:<6.1f} {:6.1f} | {:5.4f} {:5.4f} | {:5.4f} {:5.4f} | {:5.4f} {:5.4f}'.format(
                disp_type[i[0]], i[1], disp_type[i[2]], i[3], 
                i[4], i[5], i[6], i[7], i[8], i[9],
                i[10], i[11]
            )

def selInver(dp, orig_dp, tie_dp):
    inver = []
    for i in dp:
        if i not in orig_dp:
            if (i not in tie_dp
                and i[::-1] not in tie_dp):
                inver.append(i)
    return inver

def testes():
    #TESTS
    #Fusível - Fusível
    d1 = [5, 25]
    d2 = [5, 15]
    I = 500
    #cti     - 0.0158127515808

    #Religador - Fusível
    d1 = [3, 50, 9.642]
    d2 = [5, 25]
    I = 800
    #time d1 - 1.63892105570565
    #time d2 - 0.0409334900279888
    #cti     - 1.59798756567766

    #Relé - Fusível
    d1 = [2, 70, 2.7, 1, 1, 1000]
    d2 = [5, 25]
    I = 150
    #time d1  - 24.6100718871433
    #time d2  - 0.812407367516758
    #cti      - 23.7976645196266

    #Religador - Religador
    d1 = [3, 50, 9.642]
    d2 = [3, 30, 5.642]
    I = 500
    #time d1  - 1.79076102486924
    #time d2  - 0.954285205085812
    #cti      - 0.836475819783427

    #Relé - Religador
    d1 = [2, 70, 2.7, 1, 1, 1000]
    d2 = [3, 30, 5.642]
    I = 100
    #time d1  - 52.800649168904
    #time d2  - 2.05471496972345
    #cti      - 50.7459341991805

    #Fusível - Religador
    d1 = [5, 65]
    d2 = [3, 50, 9.642]
    I = 500
    #time d1  - 0.353012692259952
    #time d2  - 1.79076102486924
    #cti      - 1.43774833260929

    #plot_disp(5, 25, color='g')
    #plot_disp(3, [50, 9.642], color='r')
    #plot_disp(2, [70, 2.7, 1, 1, 1500])
    #plt.grid()
    #plt.show()

def leitura2(case, idx):
    top = {}
    with open('{}res/res{}.txt'.format(case, idx), 'r') as csvfile:
        csvfile.next()
        for i in csvfile:
            #tmp = i.split()
            tmp = i.split(',')
            key = (
                int(tmp[0]),
                int(tmp[1])
            )
            ty = tmp[2]
            if ty == '1':                           # Vazio
                top[key] = {
                    'type': int(ty)
                }
            elif ty == '2':                         # Relé
                top[key] = {
                    'type':  int(ty),
                    'Ipk_N': float(tmp[4]),
                    'tds_N': float(tmp[5]),
                    'ai_N':  float(tmp[8]),
                    'Ipk_F': float(tmp[6]),
                    'tds_F': float(tmp[7]),
                    'ai_F':  float(tmp[9])
                }
            elif ty == '3':                         # Religador
                top[key] = {
                    'type':  int(ty),
                    'Ipk_N': float(tmp[4]),
                    'tds_N': float(tmp[5]),
                    'Ipk_F': float(tmp[6]),
                    'tds_F': float(tmp[7])
                }
            elif ty == '5':                         # Fusível
                top[key] = {
                    'type': int(ty),
                    'fus':  int(tmp[3])
                }

    with open('{}zona{}.txt'.format(case, idx), 'r') as csvfile:
        for i in csvfile:
            tmp = i.split()
            key = (
                int(tmp[0]),
                int(tmp[1])
            )
            if tmp[2] != '1':
                zone = map(int, tmp[3:])
                zone = [x for x in zone if x != 0]      # Remove zeros
                top[key]['zone'] = zone

    """
    Icc = {}
    with open('top{}/icc.txt'.format(case) % top, 'r') as csvfile:
        csvfile.next()
        for i in csvfile:
            #tmp = i.split(',')
            tmp = i.split()
            node = int(tmp[0])
            Icc[node] = {
                '3f': float(tmp[1]),
                '1f': float(tmp[2]),
                '1fm': float(tmp[3]),
                '2f': float(tmp[4])
            }
    """
    return top#, Icc

def adj_list(data, nodes=None):
    """
        Le um grafo e retorna uma
        lista de adjacência
    """
    if not nodes:
        nodes = list(set([x for sublist in data for x in sublist]))
    adj = {}
    for i in nodes: adj[i] = set()
    for i, j in data:
        adj[i].add(j)
        adj[j].add(i)
    for i in adj: adj[i] = list(adj[i])
    return adj

def criaZona(grafo, tmp, top, inver, zona=None, zona2=None, visitados=None, numDisp=0):
    """
        Cria a zona de um elemento
    """
    no = tmp[1]
    if zona is None:
        zona = []
        zona2 = []
    if visitados is None:
        visitados = []
    visitados.append(no)
    isEnd = True
    for i in grafo:
        if i[0] == no:
            isEnd = False
            if i[1] not in visitados:
                """
                if numDisp > 1:
                    zona.append(tmp[0])
                else:
                """
                if i in top.keys():
                    if top[i]['type'] != 1:
                        if numDisp + 1 > 1:
                            zona.append(tmp[1])
                            if i in inver:
                                zona2.append(i[1])
                            else:
                                zona2.append(tmp[1])
                        else:
                            visitados, zona, zona2 = criaZona(
                                grafo, i, top, inver, zona, 
                                zona2, visitados, numDisp+1)
                    else:
                        visitados, zona, zona2 = criaZona(
                            grafo, i, top, inver, zona, 
                            zona2, visitados, numDisp)
                elif i[::-1] in top.keys():
                    if top[i[::-1]]['type'] != 1:
                        if numDisp + 1 > 1:
                            zona.append(tmp[1])
                            if i in inver:
                                zona2.append(i[1])
                            else:
                                zona2.append(tmp[1])
                        else:
                            visitados, zona, zona2 = criaZona(
                                grafo, i, top, inver, zona, 
                                zona2, visitados, numDisp+1)
                    else:
                        visitados, zona, zona2 = criaZona(
                            grafo, i, top, inver, zona, 
                            zona2, visitados, numDisp)
                else:
                    visitados, zona, zona2 = criaZona(
                        grafo, i, top, inver, zona, 
                        zona2, visitados, numDisp)
    if isEnd:
        zona.append(no)
        zona2.append(no)
    return visitados, zona, zona2

def new_zones(top, dp, dpOnlyDisp, inver):
    # Nos
    zones = dict()
    zones2 = dict()
    for i in dpOnlyDisp:
        _, nova_zona, nova_zona2 = criaZona(dp, i, top, inver)
        zones[i] = list(set(nova_zona))
        zones2[i] = list(set(nova_zona2))
    return zones, zones2

def print_cti3(res):
    disp_type = {
        2: 'Rele',
        3: 'Religador',
        5: 'Fusivel'
    }
    #  FASE
    print '{:=^105}'.format('FASE')
    # Cabeçalho
    print '{:>52}{:>33}'.format('Icc', 'Tempo de Operação')
    print '{:^42}{:^10}{:^5}{:>14}{:>16}{:>13}'.format('Dispositivos', '3f', '2f', 'Protetor', 'Protegido', 'CTI')
    print '{:->105}'.format('')
    for j in res:
        fase_out = res[j][0]
        for i in fase_out:
            print '{:^10}-{:^9} {:^10}-{:^9} | {:<6.1f} {:6.1f} | {:5.4f} {:5.4f} | {:5.4f} {:5.4f} | {:5.4f} {:5.4f}'.format(
                disp_type[i[0]], i[1], disp_type[i[2]], i[3], 
                i[4], i[5], i[6], i[7], i[8], i[9],
                i[10], i[11]
            )
    print 
    #  NEUTRO
    print '{:=^105}'.format('NEUTRO')
    # Cabeçalho
    print '{:>52}{:>33}'.format('Icc', 'Tempo de Operação')
    print '{:^42}{:^10}{:^5}{:>14}{:>16}{:>13}'.format('Dispositivos', '1f', '1fm', 'Protetor', 'Protegido', 'CTI')
    print '{:->105}'.format('')
    for j in res:
        neutro_out = res[j][1]
        for i in neutro_out:
            print '{:^10}-{:^9} {:^10}-{:^9} | {:<6.1f} {:6.1f} | {:5.4f} {:5.4f} | {:5.4f} {:5.4f} | {:5.4f} {:5.4f}'.format(
                disp_type[i[0]], i[1], disp_type[i[2]], i[3], 
                i[4], i[5], i[6], i[7], i[8], i[9],
                i[10], i[11]
            )

def print_cti(data):
    disp = {
        2: 'Rele',
        3: 'Religador',
        5: 'Fusivel'
    }
    # Imprime o cabeçalho
    print '{:^11}|{:^11}|{:^29}|{:^27}|{:^27}|{:^27}|{:^31}|{:^37}|'.format(
        '', '', 'Disp. de Proteção', 'Lim. das Icc',
        'Tempo Protegido', 'Tempo Protetor',
        'CTI', 'Atuação')

    print '{:^11}|{:^11}|{:^13}|{:^13}|{:^13}|{:^13}|{:^13}|{:^13}|{:^13}|{:^13}|{:^15}|{:^15}|{:^17}|{:^17}|'.format(
        'Protegido', 'Protetor',
        'Protegido', 'Protetor', 
        'Neutro', 'Fase',
        'Neutro', 'Fase',
        'Neutro', 'Fase',
        'Neutro', 'Fase',
        'Protegido', 'Protetor')

    print '{:^11}|{:^11}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6}|{:^7}|{:^7}|{:^7}|{:^7}|{:^8}|{:^8}|{:^8}|{:^8}|'.format(
        '', '', 'De', 'Para', 'De', 'Para',
        '1f', '1fm', '3f', '2f',
        '1f', '1fm', '3f', '2f',
        '1f', '1fm', '3f', '2f',
        'LS', 'LI', 'LS', 'LI',
        'Fase', 'Neutro',
        'Fase', 'Neutro'
    )

    for i in data:
        # Seleção de dados
        # Tipo
        protegido_tipo = disp[data[i]['tipo'][0]]
        protetor_tipo = disp[data[i]['tipo'][1]]
        # De-Para
        protegido_de = i[0][0]
        protegido_para = i[0][1]
        protetor_de = i[1][0]
        protetor_para = i[1][1]
        # Limite das correntes
        icc_1f = data[i]['Icc']['1f'][0]
        icc_1fm = data[i]['Icc']['1fm'][0]
        icc_3f = data[i]['Icc']['3f'][0]
        icc_2f = data[i]['Icc']['2f'][0]
        # Tempo do protegido
        time_d1_3f, time_d1_2f, time_d1_1f, time_d1_1fm = data[i]['tempo']['d1']
        # Tempo do protetor
        time_d2_3f, time_d2_2f, time_d2_1f, time_d2_1fm = data[i]['tempo']['d2']
        # CTI
        cti_3f, cti_2f, cti_1f, cti_1fm = data[i]['tempo']['cti']
        # Atuação dos dispositivos
        atuacao_protegido, atuacao_protetor = data[i]['atuacao']

        if protegido_tipo == 'Fusivel' and protetor_tipo == 'Fusivel':
            atuacao_protegido = atuacao_protegido[0]
            atuacao_protetor = atuacao_protetor[0]
            print '{:^11}|{:^11}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6.1f}|{:^6.1f}|{:^6.1f}|{:^6.1f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^7.4f}|{:^7.4f}|{:^7.4f}|{:^7.4f}|{:^17}|{:^17}|'.format(
                protegido_tipo, protetor_tipo,
                protegido_de, protegido_para, protetor_de, protetor_para,
                icc_1f, icc_1fm, icc_3f, icc_2f,
                time_d1_1f, time_d1_1fm, time_d1_3f, time_d1_2f,
                time_d2_1f, time_d2_1fm, time_d2_3f, time_d2_2f,
                cti_1f, cti_1fm, cti_3f, cti_2f,
                atuacao_protegido, atuacao_protetor
            )
        elif protegido_tipo == 'Fusivel' and protetor_tipo == 'Religador':
            atuacao_protegido = atuacao_protegido[0]
            atuacao_protetor_fase, atuacao_protetor_neutro = atuacao_protetor
            print '{:^11}|{:^11}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6.1f}|{:^6.1f}|{:^6.1f}|{:^6.1f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^7.4f}|{:^7.4f}|{:^7.4f}|{:^7.4f}|{:^17}|{:^8}|{:^8}|'.format(
                protegido_tipo, protetor_tipo,
                protegido_de, protegido_para, protetor_de, protetor_para,
                icc_1f, icc_1fm, icc_3f, icc_2f,
                time_d1_1f, time_d1_1fm, time_d1_3f, time_d1_2f,
                time_d2_1f, time_d2_1fm, time_d2_3f, time_d2_2f,
                cti_1f, cti_1fm, cti_3f, cti_2f,
                atuacao_protegido, 
                atuacao_protetor_fase, atuacao_protetor_neutro
            )
        elif (protegido_tipo == 'Religador' or protegido_tipo == 'Rele') and protetor_tipo == 'Fusivel':
            atuacao_protegido_fase, atuacao_protegido_neutro = atuacao_protegido
            atuacao_protetor = atuacao_protetor[0]
            print '{:^11}|{:^11}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6.1f}|{:^6.1f}|{:^6.1f}|{:^6.1f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^7.4f}|{:^7.4f}|{:^7.4f}|{:^7.4f}|{:^8}|{:^8}|{:^17}|'.format(
                protegido_tipo, protetor_tipo,
                protegido_de, protegido_para, protetor_de, protetor_para,
                icc_1f, icc_1fm, icc_3f, icc_2f,
                time_d1_1f, time_d1_1fm, time_d1_3f, time_d1_2f,
                time_d2_1f, time_d2_1fm, time_d2_3f, time_d2_2f,
                cti_1f, cti_1fm, cti_3f, cti_2f,
                atuacao_protegido_fase, atuacao_protegido_neutro,
                atuacao_protetor
            )
        else:
            atuacao_protegido_fase, atuacao_protegido_neutro = atuacao_protegido
            atuacao_protetor_fase, atuacao_protetor_neutro = atuacao_protetor
            print '{:^11}|{:^11}|{:^6}|{:^6}|{:^6}|{:^6}|{:^6.1f}|{:^6.1f}|{:^6.1f}|{:^6.1f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^6.4f}|{:^7.4f}|{:^7.4f}|{:^7.4f}|{:^7.4f}|{:^8}|{:^8}|{:^8}|{:^8}|'.format(
                protegido_tipo, protetor_tipo,
                protegido_de, protegido_para, protetor_de, protetor_para,
                icc_1f, icc_1fm, icc_3f, icc_2f,
                time_d1_1f, time_d1_1fm, time_d1_3f, time_d1_2f,
                time_d2_1f, time_d2_1fm, time_d2_3f, time_d2_2f,
                cti_1f, cti_1fm, cti_3f, cti_2f,
                atuacao_protegido_fase, atuacao_protegido_neutro,
                atuacao_protetor_fase, atuacao_protetor_neutro
            )
            pass

# Lê os dados da topologia e das Iccs
# Esses dados virão direto do algoritmo


"""
top, Icc = leitura(7)

# Converte a topologia para um rnp
dp = top.keys()
rnp = dp2rnp(dp, [1])
print top
sys.exit()
# Varre a topologia, utilizando o rnp, para calcular o cti
expand_zone(top, rnp)
cti_dict = {
    'fus-fus': 0.2,
    'relig-fus': 0.2,
    'rele-fus': 0.2,
    'fus-relig': 0.2,
    'relig-relig': 0.2,
    'rele-relig': 0.2
}
out = calc_cti(top, rnp, Icc, tcr=1, ccr=1, cti_dict=cti_dict)
""
print 'Tempo total: {}'.format(out[0])
print 'Tempo fase: {}'.format(out[1][0])
print 'Tempo neutro: {}'.format(out[1][1])
print 'Violações fase: {}'.format(out[2][0])
print 'Violações neutro: {}'.format(out[2][1])

print Icc.keys()
""
# time = cti_rnp(rnp, top, Icc)

# TODO: colocar a zona_ft dos nós que não tem zona
#  que são os do tipo 1

for i in rnp:
    new_top = cti.new_zones(top, rnp[i])
zone = {}
for i in top:
    if new_top[i]['type'] != 1:
        zone[i] = new_top[i]['zone']
path = 'case135/'
keys = top.keys()
keys.sort()
with open(path+'zoneando.txt', 'w') as f:
    writer = csv.writer(f, delimiter='\t')
    for i in keys:
        if i in zone.keys():
            if len(zone[i]) < 6:
                z = zone[i] + [0] * (6-len(zone[i]))
                writer.writerow(list(i) + z)
            else:
                writer.writerow(list(i) + zone[i])
        else:
            writer.writerow(list(i) + [0] * 6)

"""
